// @flow
import React, {Component} from "react";
import {Dimensions, ActivityIndicator, Platform} from "react-native";
import {StyleProvider} from "native-base";
import {StackNavigator,DrawerNavigator} from "react-navigation";
// import {Font, AppLoading} from "expo";
import {useStrict} from "mobx";
// import { Client } from 'bugsnag-react-native';
// const bugsnag = new Client('2ef2f75a4bbb1f3c7d962fc8afccfa1a');

import {Images} from "./src/components";
import {Login} from "./src/login";
import {Home} from "./src/home";
import {Lead} from "./src/lead";
import {Drawer} from "./src/drawer";
import {ViewLead} from "./src/viewlead";
import {MapsView} from "./src/googlemap";
import {Create} from "./src/create";
import {MyProfile} from "./src/myprofile";
import {MyCounties} from "./src/mycounty";
import {Contact} from "./src/contact";
import {Search} from "./src/search";
import {Setter} from "./src/setter";
import {MyStatus} from "./src/mystatus";
import {AddLead} from "./src/add-lead";
import {EditLead} from "./src/edit-lead";
import {Filter} from "./src/filter";
import {ActivityLog} from "./src/activity-log";
import {Appointment} from "./src/appointment";
import {AddApointment} from "./src/add_appointment";
import {Calendars} from "./src/calendars";
import {Notes} from "./src/notes";
import {CustomNote} from "./src/custom-note";
import {Settings} from "./src/settings";
import getTheme from "./native-base-theme/components";
import variables from "./native-base-theme/variables/commonColor";

export default class App extends Component {

    state: { ready: boolean } = {
        ready: false,
    };

    componentWillMount() {
        const promises = [];
        // promises.push(
        //     Font.loadAsync({
        //         "Avenir-Book": require("./fonts/Avenir-Book.ttf"),
        //         "Avenir-Light": require("./fonts/Avenir-Light.ttf")
        //     })
        // );
        // Promise.all(promises.concat(Images.downloadAsync()))
        //     .then(() => this.setState({ ready: true }))
        //     // eslint-disable-next-line
        //     .catch(error => console.error(error))
        // ;
        this.setState({ ready: true });
    }

    render(): React$Element<*> {
        const {ready} = this.state;
        return <StyleProvider style={getTheme(variables)}>
            {
                ready ?
                    <AppNavigator onNavigationStateChange={() => undefined} />
                :
                    // <AppLoading />
                    <ActivityIndicator size="large" color="#00ff00" />
            }
        </StyleProvider>;
    }
}

useStrict(true);

const stackNavigationConfig = {
    // initialRouteName: 'Places',
    headerMode: 'none',
    navigationOptions: {
    //   title: 'App Name',
    //   header: ({ state, setParams }) => ({
    //     style: { backgroundColor: 'green' }
    //   })
    }
  }

const MainNavigator = DrawerNavigator({
    Home: { screen: Home },
    Lead: { screen: Lead },
    MapsView: { screen: MapsView },
    Filter: { screen: Filter },
    AddLead: { screen: AddLead },
    Calendars: { screen: Calendars },
    ActivityLog: { screen: ActivityLog }
}, {
    drawerWidth: Dimensions.get('window').width,
    contentComponent: Drawer,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle'
});

const AppNavigator = StackNavigator({
    
    Login: { screen: Login },
    Main: { screen: MainNavigator },
    Filter: { screen: Filter },
    Home: { screen: Home },
    MapsView: { screen: MapsView },
    Lead: { screen: Lead },
    ViewLead: { screen: ViewLead },
    MyProfile: { screen: MyProfile },
    MyCounties: { screen: MyCounties },
    MyStatus: { screen: MyStatus},
    Contact: { screen: Contact },
    Search: { screen: Search },
    ActivityLog: { screen: ActivityLog },
    AddApointment: { screen: AddApointment },
    Calendars: { screen: Calendars },
    Setter: { screen: Setter },
    EditLead: { screen: EditLead },
    Notes: { screen: Notes },
    Appointment: { screen: Appointment},
    AddLead: { screen: AddLead },
    CustomNote: { screen: CustomNote },
    Settings: { screen: Settings }
}, {
    headerMode: "none"
});

export {AppNavigator};