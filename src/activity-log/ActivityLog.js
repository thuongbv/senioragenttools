// @flow
import moment from "moment";
import React, {Component} from "react";
import {ListView,Text, Image, StyleSheet, View,ImageBackground,
    AsyncStorage,Dimensions,ActivityIndicator,TouchableWithoutFeedback,ScrollView} from "react-native";
import {H1,Container,Button} from "native-base";
import Toast from 'react-native-root-toast';


import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

export default class ActivityLog extends Component {

    go(key: string) {
              this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    constructor(props) {
       super(props);
       this.state = {
        //  isLoading: true,
            isLoading: true,
            loadMore:true,
         page:1,
         dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
       };
       this.group = [];
       this.toast_configuration = {
           duration: Toast.durations.SHORT,
           position: Toast.positions.BOTTOM,
           shadow: true,
           animation: true,
           hideOnPress: true,
           delay: 0,
       };
       this.item_count = 200;
       
    }
    async componentWillMount() {
       
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let output_list_url = Global.API_URL + "activity_logs/";
        output_list_url += user_dataa.uid + "/";

        output_list_url += user_dataa.token + "/";
        output_list_url +=  1+ "/";
        output_list_url += this.item_count;
        console.log(output_list_url);
        return fetch(output_list_url)
            .then((response) => response.json())
            .then((responseJson) => {
                if(responseJson.data.length==0){
                    this.setState({isLoading: false,loadMore:false})
                }
                if('1' == responseJson.status&&responseJson.data.length!=0) {
                    mang =responseJson.data;
                    console.log(mang);
                    this.setState({
                        isLoading: false,
                        dataSource: this.state.dataSource.cloneWithRows(mang)
                    

                });
                console.log(this.state.dataSource);
                
            }
            
            })
        .catch((error) => {
        console.error(error);
        });
        
    }
    async _onEndReached() {
        numpage=this.state.page+1;
        if(this.state.loadMore){
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let output_list_url = Global.API_URL + "activity_logs/";
        output_list_url += user_dataa.uid + "/";

        output_list_url += user_dataa.token + "/";
        output_list_url +=  numpage+ "/";
        output_list_url += this.item_count;
            return fetch(output_list_url)
                .then((response) => response.json())
                .then((responseJson) => {
                    if(responseJson.data.length==0){
                        this.setState({isLoading: false,loadMore:false})
                    }
                    if('1' == responseJson.status&&responseJson.data.length!=0) {
                    mang=mang.concat(responseJson.data);
                    this.group = responseJson.data;
                    this.setState({
                        dataSource: this.state.dataSource.cloneWithRows(mang),
                        page: this.state.page+1

                    });
                    
                   
                }
                
              })
            .catch((error) => {
            console.error(error);
            });
        }
    }

    render(): React$Element<*> {
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        const {navigation} = this.props;
        // console.log(this.state.dataSource);
        return <BaseContainer title="Activity Log" {...{ navigation }}>
            <Container >
                <View style={[Styles.listItemView, style.group,{ marginLeft:5,marginBottom: 15,padding:10}]} last>
                    <View style={style.text}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                            <View style={{width: '25%'}} >
                                <Text>Date time </Text>
                            </View>
                            <View style={{width: '20%'}} >
                                <Text>Lead Name</Text>
                            </View>
                            <View style={{width: '20%'}} >
                                <Text>UserName</Text>
                            </View>
                            <View style={{width: '35%'}} >
                                <Text>Action</Text>
                            </View>
                        </View>
                    </View>
                </View>
                
               
                <ListView
                    
                    onEndReached={this._onEndReached.bind(this)}
                    onEndReachedThreshold={this.item_count}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => 
                       
                        <TouchableWithoutFeedback >
                            <View style={[Styles.listItemView, style.group]} last>
                                <View style={style.text}>
                                    <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                                            <View style={{width: '25%'}}>
                                                <Text>{rowData.created_date}</Text>
                                            </View>
                                            <View style={{width: '20%'}} >
                                                <Text>{rowData.lead_name2}</Text>
                                            </View>
                                            <View style={{width: '20%'}} >
                                                <Text>{rowData.name}</Text>
                                            </View>
                                            <View style={{width: '35%'}} >
                                                <Text>{rowData.action+':'+rowData.content}</Text>
                                            </View>
                                            {/* <View style={{width: '100%'}} >
                                                <Text>{rowData.city}</Text>
                                            </View> */}
                                            {/* <View style={{width: '100%'}} >
                                                <Text>{rowData.county}</Text>
                                            </View>
                                            <View style={{width: '100%'}} >
                                                <Text>{rowData.state}</Text>
                                            </View>
                                            <View style={{width: '100%'}} >
                                                <Text>{rowData.zip}</Text>
                                            </View> */}
                                            
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>

                    }
                    />
                   
                
            </Container>
        </BaseContainer>;
    }
}
const {width, height} = Dimensions.get("window");
const style = StyleSheet.create({

    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    button: {
            marginBottom: 100,
            marginRight: 5,
            backgroundColor: "#291BE0",
    //        color: "white",
            alignSelf:'center',
            padding:0
        },
    group: {
            width:'100%',
            backgroundColor: variables.lightGray,
            justifyContent: "space-between",
            marginBottom: 1,
            
        },
    text: {
//        paddingLeft: variables.contentPadding * 2,
        paddingBottom:10
    },

});