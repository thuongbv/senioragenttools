// @flow
import autobind from "autobind-decorator";
import moment from "moment";
import React, {Component} from "react";
import {Text, TextInput, StyleSheet, View,Alert,ImageBackground,Dimensions,AsyncStorage,ScrollView,KeyboardAvoidingView,ActivityIndicator} from "react-native";
import {H1,Container,Button,Item,Input,Picker} from "native-base";

import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Circle, Styles, Images, WindowDimensions,NavigationHelpers,Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import Toast from 'react-native-root-toast';
import DatePicker from 'react-native-datepicker';
import RNFetchBlob from 'react-native-fetch-blob';

export default class AddLead extends Component {
   
    go(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    
constructor(props) {
    super(props);
    this.state = {
        isLoading: true,
        output_key: 0,
        contact:0,
        notes:'',
        lead_status:'',
        check_copy:'',
        uid:'',
        token:'',
        first_name:'',
        last_name:'',
        age:'',
        spouse_name:'',
        spouse_age:'',
        street_address:'',
        city:'',
        state_code:'',
        zip:'',
        country:'',
        phone1:'',
        phone2:'',
        quick_note:'',
        record_day:'',
        email_address:'',
        add_lead_type:'',
        decline_id:'',
        check_copy:'',
        file:'',
        url:'',
        lead_type:'Direct Mail',
        chosenDate: new Date()
      // dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
  };
 
    this.group = {};
    this.status_cuz=[];
    this.status_default =[];
    this.state_codes =[];
    this.counties=[];
    this.lead_types=["Direct Mail","Telemarket","Other"];
    this.totalLeads = 0;
}

async getDataOffline(data_name) {
    const json_file_path = await AsyncStorage.getItem('json_file_path');
    if(null !== json_file_path) {
        return RNFetchBlob.fs.readFile(json_file_path+data_name+'.json', 'utf8')
        .then((file_content) => {
        // handle the data ..
            return JSON.parse(file_content);
        })
    }
}

async componentWillMount() {
    // get is_offline data from async storage
    const is_offline = JSON.parse(await AsyncStorage.getItem('is_offline'));
    this.setState({is_offline: null === is_offline ? this.state.is_offline : is_offline}, async function() {
        console.log('is offline: ', this.state.is_offline);
        if(this.state.is_offline) {
            // offline
            // get status
            const statuses = await this.getDataOffline('statuses');
            this.status_cuz = statuses.custom;
            this.status_default = statuses.default;

            // get state
            this.state_codes = await this.getDataOffline('states');

            // get county
            this.counties = await this.getDataOffline('counties');
            
            // get total lead
            this.totalLeads = await this.getDataOffline('count_data');

            this.setState({
                isLoading: false,
                lead_status: this.status_default[0].default_name
            });
        } else {
            // online
            const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));

            let output_list_url = Global.API_URL + "statuses/"+user_dataa.uid + "/"+user_dataa.token + "/";
            let output_list_url_counties = Global.API_URL + "counties/"+user_dataa.uid + "/"+user_dataa.token + "/";
            let output_list_url_state = Global.API_URL + "states/"+user_dataa.uid + "/"+user_dataa.token + "/";
            fetch(output_list_url)
                .then((response) => response.json())
                .then((responseJson) => {
                    
                    if('1' == responseJson.status) {
                        // get top list successfully
                        this.status_cuz = responseJson.data.custom;
                        this.status_default = responseJson.data.default;
                        fetch(output_list_url_state)
                            .then((response) => response.json())
                            .then((responseJson) => {
                            if('1' == responseJson.status) {
                                // get top list successfully
                                this.state_codes=responseJson.data;
                                fetch(output_list_url_counties)
                                    .then((response) => response.json())
                                    .then((responseJson) => {
                                    
                                        if('1' == responseJson.status) {
                                            // get top list successfully
                                            this.counties = responseJson.data;
                                            // console.log(this.contact_results);
                                            this.setState({
                                                isLoading: false,
                                                uid:user_dataa.uid,
                                                token:user_dataa.token,
                                                lead_status: this.status_default[0].default_name
                                            //   dataSource: ds.cloneWithRows(responseJson.movies),
                                            }
                                            );
                                        }
                                    })
                                    .catch((error) => {
                                        console.error(error);
                                    });
                                    }
                                })
                            .catch((error) => {
                                console.error(error);
                            }); 
            
                        }
                    // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                        
                })
                .catch((error) => {
                    console.error(error);
            });
        }
    });  
}

getCurrentDatetime() {
    const currentdate = new Date();
    const current_hour = 12 > currentdate.getHours() ? currentdate.getHours() : currentdate.getHours()-12;
    return (10 > (currentdate.getMonth()+1) ? "0" : "") + (currentdate.getMonth()+1) + "/"
    + (10 > currentdate.getDate() ? "0" : "") + currentdate.getDate()  + "/" 
    + currentdate.getFullYear() + " "  
    + (10 > current_hour ? "0" : "") + current_hour + ":"  
    + (10 > currentdate.getMinutes() ? "0" : "")+ currentdate.getMinutes() + ":" 
    + (10 > currentdate.getSeconds() ? "0" : "")+ currentdate.getSeconds() + " "
    + (currentdate.getHours()>12 ? "PM" : "AM");
}

    UpdateLead=async () =>{
        if(this.state.first_name.trim()==='') {
            Toast.show('First name must NOT blank', this.toastConfiguration);
            return;
        }

        if(this.state.last_name.trim()==='') {
            Toast.show('Last name must NOT blank', this.toastConfiguration);
            return;
        }
        if(this.state.record_day.trim()==='') {
            Toast.show('date received must NOT blank', this.toastConfiguration);
            return;
        }
        if(this.state.street_address.trim()==='') {
            Toast.show('Stress address must NOT blank', this.toastConfiguration);
            return;
        }
        if(this.state.city.trim()==='') {
            Toast.show('City must NOT blank', this.toastConfiguration);
            return;
        }
        if(this.state.country.trim()==='') {
            Toast.show('County must NOT blank', this.toastConfiguration);
            return;
        }
        if(this.state.zip.trim()==='') {
            Toast.show('Zip must NOT blank', this.toastConfiguration);
            return;
        }
        if(this.state.state_code.trim()==='') {
            Toast.show('State must NOT blank', this.toastConfiguration);
            return;
        }

        if(this.state.is_offline) {
            // offline
            let offline_data_change = JSON.parse(await AsyncStorage.getItem('offline_data_change'));
            const new_lead_id = Math.random().toString(36).substr(2, 5);
            const lead = {
                id: new_lead_id,
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                age: this.state.age,
                spouse_name: this.state.spouse_name,
                spouse_age: this.state.spouse_age,
                street_address: this.state.street_address,
                city: this.state.city,
                country: this.state.country,
                phone1: this.state.phone1,
                phone2: this.state.phone2,
                quick_note: this.state.quick_note,
                record_day: this.state.record_day,
                lead_status: this.state.lead_status,
                state: this.state.state_code,
                zip: this.state.zip,
                add_lead_type: '',
                decline_id: '',
                check_copy: this.state.check_copy,
                lead_type: this.state.lead_type,
                url: this.state.url,
                current_datetime: this.getCurrentDatetime()
            };
            if(undefined === offline_data_change.leads) {
                offline_data_change.leads = [];
            }
            offline_data_change.leads.push(lead);
            const _this = this;
            await AsyncStorage.setItem('offline_data_change', JSON.stringify(offline_data_change), async function() {
                // update file
                const json_file_path = await AsyncStorage.getItem('json_file_path');
                const new_lead_index = offline_data_change.leads.length + _this.totalLeads.total_lead;
                console.log(new_lead_index);
                RNFetchBlob.fs.writeFile(json_file_path+'leads/'+new_lead_index+'-'+new_lead_id+'.json', JSON.stringify(lead), 'utf8')
                .then(()=>{
                    NavigationHelpers.reset(_this.props.navigation, "Lead");
                })
            });
        } else {
            // online
            // call api
            const edit_lead = Global.API_URL + "lead/";
            let form_data_edit_lead = new FormData();
            
            form_data_edit_lead.append("uid", this.state.uid);
            form_data_edit_lead.append("token", this.state.token);
            
            form_data_edit_lead.append("first_name", this.state.first_name);
            form_data_edit_lead.append("last_name", this.state.last_name);
            form_data_edit_lead.append("age", this.state.age);
            form_data_edit_lead.append("spouse_name", this.state.spouse_name);
            form_data_edit_lead.append("spouse_age", this.state.spouse_age);
            form_data_edit_lead.append("street_address", this.state.street_address);
            form_data_edit_lead.append("city", this.state.city);
            form_data_edit_lead.append("country", this.state.country);
            form_data_edit_lead.append("phone1", this.state.phone1);
            form_data_edit_lead.append("phone2", this.state.phone2);
            form_data_edit_lead.append("quick_note", this.state.quick_note);
            form_data_edit_lead.append("record_day", this.state.record_day);
            form_data_edit_lead.append("lead_status", this.state.lead_status);
            form_data_edit_lead.append("state", this.state.state_code);
            form_data_edit_lead.append("zip", this.state.zip);
            form_data_edit_lead.append("add_lead_type", "");
            form_data_edit_lead.append("decline_id", "");
            form_data_edit_lead.append("id", this.state.id);
            form_data_edit_lead.append("check_copy", this.state.check_copy);
            form_data_edit_lead.append("lead_type", this.state.lead_type);
            form_data_edit_lead.append("url", this.state.url);
            try {
                let response = await fetch(edit_lead, {
                    method: 'POST',
                    headers: {
                    },
                    body: form_data_edit_lead
                });
                let responseJson = await response.json();

                Toast.show(responseJson.msg, this.toastConfiguration);
                if(1 == responseJson.status) {
                    // saving user data
                    try {
                        NavigationHelpers.reset(this.props.navigation, "Lead");
                    } catch (error) {
                    // Error saving data
                        console.error(error);
                    }
                }else{
                    // Toast.show(responseJson.msg, this.toastConfiguration);
                }

            } catch(error) {
                console.error(error);
            }
        }
    }
    getChangeStatus(value:string){
        this.setState({lead_status: value});
    }
    render(): React$Element<*> {
        // console.log(this.status_default[0].default_name);
        const {navigation} = this.props;
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        const date = moment(this.state.record_day);
       
        
        return <BaseContainer title="Add Lead" {...{ navigation }}>
            <Container style={{backgroundColor: "#ECF0F3"}}>
            <ScrollView contentContainerStyle={style.content}>
                    <KeyboardAvoidingView behavior="padding">
                    {/* <Item style={style.tagInput}> */}
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="First Name"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(first_name) => this.setState({first_name: first_name})}
                                value={this.state.first_name}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Last Name"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(last_name) => this.setState({last_name: last_name})}
                                value={this.state.last_name}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Age"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(age) => this.setState({age: age})}
                                value={this.state.age}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Spouse Name"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({spouse_name: value})}
                                value={this.state.spouse_name}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Spouse Age"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({spouse_age: value})}
                                value={this.state.spouse_age}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Street Address"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({street_address: value})}
                                value={this.state.street_address}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="City"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({city: value})}
                                value={this.state.city}
                            />
                        </View>
                        <Picker
                                mode="dropdown"
                                selectedValue={this.state.state_code}
                                onValueChange={(state_code) => this.setState({state_code: state_code})}
                                >
                                    <Item label="select state" value="" />
                                    {this.state_codes.map((item, index) => (
                                        <Item label={item.state_name} value={item.state_code} />
                                    ))}
                        </Picker>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Zip"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({zip: value})}
                                value={this.state.zip}
                            />
                        </View>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.country}
                            onValueChange={(country) => this.setState({country: country})}
                            >
                                <Item label="select county" value="" />
                                {this.counties.map((item, index) => (
                                    <Item label={item.county_name} value={item.county_name} />
                                ))}
                        </Picker>
                        
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Phone 1"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({phone1: value})}
                                value={this.state.phone1}
                            />
                        </View>
                        
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Phone 2"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({phone2: value})}
                                value={this.state.phone2}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Quick Note - Shows in Book of Business View"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({quick_note: value})}
                                value={this.state.quick_note}
                            />
                        </View>
                        
                        <Text>Date Received</Text>
                        <DatePicker
                                    
                                date={this.state.record_day}
                                mode="date"
                                placeholder="Start Date"
                                format="MM/DD/YYYY"
                                
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                                // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {this.setState({record_day: date})}}
                            />
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Email Address"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({email_address: value})}
                                value={this.state.email_address}
                            />
                        </View>
                        <Text>Lead status</Text>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.lead_status}
                            onValueChange={(lead_status) => this.getChangeStatus(lead_status)}
                            >
                                {this.status_default.map((item, index) => (
                                    <Item label={item.default_name} value={item.default_name} />
                                ))}
                                {this.status_cuz.map((item, index) => (
                                    <Item label={item.name} value={item.name} />
                                ))}
                                 
                        </Picker>
                        <Text>Lead type</Text>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.lead_type}
                            onValueChange={(lead_type) => this.setState({lead_type: lead_type})}
                            >
                                
                                {this.lead_types.map((item, index) => (
                                    <Item label={item} value={item} />
                                ))}
                        </Picker>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Url"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({url: value})}
                                value={this.state.url}
                            />
                        </View>
                        <Button full onPress={() => this.UpdateLead()} style={style.button}>
                                <Text style={{color:"white"}}>Add</Text>
                        </Button>
                        {/* <View style={style.blur}>
                            <TextInput 
                                placeholder="File"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="go"
                                inverse
                                onChangeText={(value) => this.setState({file: value})}
                                value={this.state.file}
                            />
                        </View> */}
                    {/* </Item> */}
                    </KeyboardAvoidingView>
            </ScrollView>
            </Container>
        </BaseContainer>;
    }
}

const style = StyleSheet.create({
    content: {
        
        backgroundColor:"#ECF0F3",
        
    },
    button: {
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: "#6AA6D6",
//        color: "white",
        // alignSelf:'center'

    },
    blur: {
        backgroundColor: "rgba(255, 255, 255, .2)"
    },
    text: {
        fontFamily: variables.titleFontfamily,
        color: "white"
    },
    headline: {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18
      },
    sizeLogo:{
        width : 357,
        // justifyContent: 'center',
        height: 70
        
    },
    tagInput:{
        borderColor:"gray",
        height:40
    },
});