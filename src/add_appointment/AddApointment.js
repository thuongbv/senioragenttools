// @flow
import autobind from "autobind-decorator";
import moment from "moment";
import React, {Component} from "react";
import {Text, Image, StyleSheet, View,ImageBackground,Dimensions,AsyncStorage} from "react-native";
import {H1,Container,Button} from "native-base";

import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Circle, Field,Styles, Images, WindowDimensions,NavigationHelpers,Global } from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import DatePicker from 'react-native-datepicker';
import Toast from 'react-native-root-toast';
import { Calendar } from "react-native-calendars";
export default class AddApointment extends Component {
    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    go(key: string) {
        this.props.navigation.navigate(key);
    }
    constructor(props) {
        super(props);
       
        this.state = {
            isLoading: true,
            output_key: 0,
            date_time:'',
            end_date:'',
            title:'',
            description: '',
            data:{},
            lead_id:'',
            type_view:'',
            notSended:true,
            from_lead_view: false
            
          // dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
      };
     
      //   this.contact='';
    }
    async componentDidMount() {
        const {state} = this.props.navigation;
        if(state.params!=undefined){
            // console.log("ngahahaha");
            const data = state.params.data;
            console.log(data);
            const output_key = state.params.output_key;
            let lead_id_key = state.params.lead_id;
            const from_lead_view = undefined===state.params.from_lead_view ? false : state.params.from_lead_view;
            if(lead_id_key==undefined){
                lead_id_key=data.lead_id;
            }
           
            if(output_key!=undefined){
                this.setState({
                    isLoading: false,
                    output_key: output_key,
                    date_time:data.date_time,
                    end_date:data.end_date,
                    title:data.title,
                    description:data.description,
                    type_view:data.type_view,
                    lead_id:data.lead_id,
                    from_lead_view: from_lead_view
                });
            }else{
                const currentDate = moment(Date(new Date)).format('YYYY-MM-DD HH:mm');
                const currentDate_end = moment(Date(new Date)).add(1, 'hours').format('YYYY-MM-DD HH:mm');
                
                this.setState({
                    isLoading: false,
                    output_key: '',
                    date_time:currentDate,
                    end_date:currentDate_end,
                    title:'',
                    description:'',
                    type_view:'Calendar',
                    lead_id:lead_id_key,
                    from_lead_view: from_lead_view
                });
            }
            
            
        }
    }
    deleteCalendar=async () =>{
        const del_appointment = Global.API_URL + "appointment/";
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        const form_data_del_appointment="uid="+user_dataa.uid+"&token="+user_dataa.token+"&id="+this.state.output_key;
        console.log(form_data_del_appointment) ;
        try {
        
            let response = await fetch(del_appointment, {
                method: 'DELETE',
                headers: {
                  
                },
                body: form_data_del_appointment
            });
            let responseJson = await response.json();
            

            Toast.show(responseJson.msg, this.toastConfiguration);
    //                    NavigationHelpers.reset(this.props.navigation, "Home");
            if(1 == responseJson.status) {
                //// login successfully
                console.log(this.state.lead_id);
                
                try {
                    if(this.state.lead_id!=''&&this.state.type_view=="Calendar"){
                        this.go1("Calendars",{data:"Lead_View", output_key: this.state.from_lead_view ? this.state.lead_id : ''});
                    }else if(this.state.lead_id==''&&this.state.type_view=="Calendar"){
                        this.go1("Calendars",{data:"None", output_key:"Delete"});
                    }else{
                        this.go("Appointment");
                    }
                    // console.log(this.state.lead_id);

                } catch (error) {
                // Error saving data
                    console.error(error);
                    Toast.show(error, this.toastConfiguration);

                }

            }else{
                Toast.show(responseJson.msg, this.toastConfiguration);
            }

        } catch(error) {
    //                     console.log(1234);
            console.error(error);
            Toast.show(error, this.toastConfiguration);
        }
    }
    addApointment=async () =>{
        
            // if(this.state.title.trim()==='') {
            //     Toast.show('Title must NOT blank', this.toastConfiguration);
            //     return;
            // }
            
            
            // if(this.state.start_date.trim()==='') {
            //     Toast.show('Last name not bank', this.toastConfiguration);
            //     return;
            // }
            // if(this.state.end_date.trim()==='') {
            //     Toast.show('date received not bank', this.toastConfiguration);
            //     return;
            // }
                // call api
                console.log(this.state.notSended+"gjnl");
        if(this.state.notSended){
            this.setState({
                notSended : false
            },async ()=>{
                console.log(this.state.notSended+"gjnl");
                const add_appointment = Global.API_URL + "appointment/";

                let form_data_add_appointment = new FormData();
                const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
                form_data_add_appointment.append("uid",user_dataa.uid);
                form_data_add_appointment.append("token", user_dataa.token);
                form_data_add_appointment.append("id", this.state.output_key);
                form_data_add_appointment.append("title", this.state.title);
                form_data_add_appointment.append("description", this.state.description);
                form_data_add_appointment.append("date_time", this.state.date_time);
                form_data_add_appointment.append("end_date", this.state.end_date);
                form_data_add_appointment.append("lead_id", this.state.lead_id);
               
                
            try {
            
                let response = await fetch(add_appointment, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                    body: form_data_add_appointment
                });
                let responseJson = await response.json();
                

                Toast.show(responseJson.msg, this.toastConfiguration);
        //                    NavigationHelpers.reset(this.props.navigation, "Home");
                if(1 == responseJson.status) {
                    // saving user data
                    try {
                        //add
                        
                        // console.log(this.state.lead_id, this.state.type_view);
                        
                        if(this.state.output_key==0){
                            this.go1("Calendars",{data:"None", output_key: this.state.lead_id});
                        }
                        //edit
                        else if(this.state.lead_id!=''&&this.state.type_view=="Calendar"){
                            this.go1("Calendars",{data:"Lead_View", output_key: this.state.from_lead_view ? this.state.lead_id : ''});
                        }else if(this.state.lead_id==''&&this.state.type_view=="Calendar"){
                            this.go1("Calendars",{data:"None", output_key:"Delete"});
                        }else{
                            this.go("Appointment");
                        }
                    } catch (error) {
                    // Error saving data
                        console.error(error);
                        Toast.show(error, this.toastConfiguration);

                    }

                }else{
                    Toast.show(responseJson.msg, this.toastConfiguration);
                }

            } catch(error) {
        //                     console.log(1234);
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
            });
            
        }
    }
    render(): React$Element<*> {
        const {navigation} = this.props;
        
        const {height, width} = Dimensions.get("window");
        // console.log(width);
        return <BaseContainer title={this.state.output_key==0?"Add Appointment":"Edit Appointment"} {...{ navigation }}>
            <Container style={{backgroundColor: "#ECF0F3"}}>
            {/* <Text>Title</Text> */}
            <View style={style.blur}>
                <Field 
                    label="Title"
                    keyboardType="default"
                    autoCapitalize="none"
                    returnKeyType="next"
                    inverse
                    onChangeText={(value) => this.setState({title: value})}
                    value={this.state.title}
                />
                <Field 
                    label="Description"
                    keyboardType="default"
                    autoCapitalize="none"
                    returnKeyType="next"
                    inverse
                    onChangeText={(value) => this.setState({description: value})}
                    value={this.state.description}
                    multiline = {true}
                    style={{height:119}}
                />
            </View>
            <Text>Start date</Text>
            <DatePicker
                style={{width: 200}}
                date={this.state.date_time}
                mode="datetime"
                format="YYYY-MM-DD HH:mm"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                    },
                    dateInput: {
                    marginLeft: 36
                    }
                }}
                minuteInterval={10}
                onDateChange={(datetime) => {this.setState({date_time: datetime});}}
                />
            
            <Text>End date</Text>
            <DatePicker
                style={{width: 200}}
                date={this.state.end_date}
                mode="datetime"
                format="YYYY-MM-DD HH:mm"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                    },
                    dateInput: {
                    marginLeft: 36
                    }
                }}
                minuteInterval={10}
                onDateChange={(datetime) => {this.setState({end_date: datetime});}}
                />
                <View style={Styles.listItem}>
                <Button full onPress={() => this.addApointment()} style={style.button}>
                    <Text style={{color:"white"}}>{this.state.output_key==0?"Add":"Edit"}</Text>
                </Button>
                {this.state.output_key==0? <Text></Text>:<Button style={style.button} onPress={() =>this.deleteCalendar(this.state.lead_id)}>
                        <Text style={{color:"white"}}>Delete</Text>
                    </Button>}
                    </View>
                </Container>
               
        </BaseContainer>;
    }
}

const style = StyleSheet.create({
    button: {
        marginTop: 20,
        backgroundColor: "#6AA6D6",
//        color: "white",
        padding:10,
        alignSelf:'center',
        marginRight:10

    },
    text: {
        fontFamily: variables.titleFontfamily,
        color: "white"
    },
    headline: {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18
      },
    sizeLogo:{
        width : 238,
        alignSelf:'center',
        height: 50
        
    }
});