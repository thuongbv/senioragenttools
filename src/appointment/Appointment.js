// @flow
import moment from "moment";
import React, {Component} from "react";
import {ListView,Text, Image, StyleSheet, View,ImageBackground,
    AsyncStorage,Dimensions,ActivityIndicator,TouchableWithoutFeedback,ScrollView,TouchableOpacity} from "react-native";
import {H1,Container,Button,Item,Icon,Input} from "native-base";
import Toast from 'react-native-root-toast';


import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import { SearchBar } from 'react-native-elements';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class Appointment extends Component {

    go(key: string) {
        this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    constructor(props) {
       super(props);
       
       this.state = {
        //  isLoading: true,
            isLoading: true,
            page:1,
            output_key:'',
            search:'',
            keyword:'',
            uid:'',
            token:'',
            group:[],
            loadMore:true,
            dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
       };
       
    //    this.group = [];
       this.toast_configuration = {
           duration: Toast.durations.SHORT,
           position: Toast.positions.BOTTOM,
           shadow: true,
           animation: true,
           hideOnPress: true,
           delay: 0,
       };
       
    }
    
    async componentDidMount() {
        const {state} = this.props.navigation;
        if(state.params!=undefined){
            const data = state.params.data;
            const output_key = state.params.output_key;
            console.log(output_key);
            this.setState({
                isLoading: false,
                dataSource:  this.state.dataSource.cloneWithRows(data),
                output_key: output_key
            });
        }
    }
    async componentWillMount() {
        const {state} = this.props.navigation;
        
            // console.log(1243432);
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let output_list_url = Global.API_URL + "appointments/";
        output_list_url += user_dataa.uid + "/";

        output_list_url += user_dataa.token + "/";
        output_list_url += "" + "/";
        output_list_url +=  1+ "/";
        output_list_url += 100;
        console.log(output_list_url);
        return fetch(output_list_url)
            .then((response) => response.json())
            .then((responseJson) => {
                if(responseJson.data.length==0){
                    this.setState({isLoading: false,loadMore:false})
                }
                if('1' == responseJson.status&&responseJson.data.length!=0) {
                    mang =responseJson.data;
                    
                this.setState({
                    isLoading: false,
                    group : responseJson.data,
                    dataSource: this.state.dataSource.cloneWithRows(mang),
                    uid:user_dataa.uid ,
                    token:user_dataa.token,

                });
             }
            
            })
        .catch((error) => {
            console.error(error);
        });
    
        
    }
    async _onEndReached() {
        const {state} = this.props.navigation;
        console.log(this.state.search);
      if(this.state.loadMore) {
        numpage=this.state.page+1;
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let output_list_url = Global.API_URL + "appointments/";
        output_list_url += user_dataa.uid + "/";

        output_list_url += user_dataa.token + "/";
        output_list_url += ""+ "/";
        output_list_url +=  numpage+ "/";
        output_list_url += 100;
            return fetch(output_list_url)
                .then((response) => response.json())
                .then((responseJson) => {
                if(responseJson.data.length==0){
                    this.setState({isLoading: false,loadMore:false})
                }
                 if('1' == responseJson.status&&responseJson.data.length!=0) {
                    mang=mang.concat(responseJson.data);
                    
                    this.setState({
                        
                        isLoading: false,
                        group : responseJson.data,
                        dataSource: this.state.dataSource.cloneWithRows(mang),
                        page: this.state.page+1

                    }); 
                }
                
              })
            .catch((error) => {
            console.error(error);
            });
        }
          
    }
    
    render(): React$Element<*> {
        const {navigation} = this.props;
        console.log(this.state.isLoading);
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        console.log(this.state.dataSource);
        // console.log(this.group);
        if(this.state.group.length>0){
            return <BaseContainer title="Appointment" {...{ navigation }}>
            <Container >
            
                {/* <View style={Styles.listItem}> */}
                    {/* {this.state.output_key!=''? <Button onPress={() =>this.go1("AddApointment",{data:"None", output_key: this.state.output_key})}>
                        <Text>Add</Text>
                    </Button>:<Text></Text>} */}
                    <Button onPress={() => this.go("Calendars")} style={style.button}>
                        <Image source={Images.add_appointment} style={{width: 26, height: 26}}/>
                    </Button>
                    
                {/* </View> */}
                <View style={[Styles.listItemView, style.group,{ marginLeft:5,marginBottom: 15,padding:10}]} last>
                    <View style={style.text}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                            <View style={{width: '20%'}} >
                                <Text>Name Lead</Text>
                            </View>
                            <View style={{width: '20%'}} >
                                <Text>Start date</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>End date</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>Title</Text>
                            </View>
                           
                        </View>
                    </View>
                </View>
                
               
                <ListView
                    style={style.styleList}
                    onEndReached={this._onEndReached.bind(this)}
                    onEndReachedThreshold={100}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => 
                       
                       
                            <View style={[Styles.listItemView, style.group]} last>
                                <View style={style.text}>
                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                            <View style={{width: '25%'}}>
                                                <Text>{rowData.first_name+' '+rowData.last_name}</Text>
                                            </View>
                                            <View style={{width: '75%'}}>
                                                <View style={{flexDirection:'column', flex: 1}}>
                                                {/* {rowData.appointments.map((item, index) => ( */}
                                                    
                                                    <Group  info={rowData} onPress={() => this.go1("AddApointment",{data: rowData, output_key: rowData.id})}/>
                                                {/* ))} */}
                                            </View>
                                            </View>
                                    </View>
                                </View>
                            </View>

                    }
                    />
                   
                
            </Container>
        </BaseContainer>;
        }else{
            return(
            <BaseContainer title="Appointment" {...{ navigation }}>
            <Container >
            <View style={Styles.listItem}>

                    <Button onPress={() => this.go("Calendars")} style={style.button}>
                        <Image source={Images.add_appointment} style={{width: 26, height: 26}}/>
                    </Button>
                    
                </View>
                
                <View style={[Styles.listItemView, style.group,{ marginLeft:5,marginBottom: 15,padding:10}]} last>
                    <View style={style.text}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                            <View style={{width: '30%'}} >
                                <Text>Name Lead</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>Start date</Text>
                            </View>
                            <View style={{width: '40%'}} >
                                <Text>End date</Text>
                            </View>
                           
                            {/* <View style={{width: '30%'}} >
                                <Text>City</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>County</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>State</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>Zip</Text>
                            </View> */}
                        </View>
                    </View>
                </View>
                </Container>
                        </BaseContainer>);
        }
        
    }
}
const {width, height} = Dimensions.get("window");
class Group extends Component {
    
    props: {
        info: object,
        onPress: () => void
    }
    render(): React$Element<*> {
        const {info, onPress} = this.props;
        return ( <TouchableOpacity onPress={onPress}  >
                    <View style={{flex: 1, flexDirection: 'row',width:'100%',justifyContent: 'space-between'}}>
                    
                    <View style={{width: '30%'}}>

                        <Text>{info.date_time}</Text>
                    </View>
                    <View style={{width: '30%'}}>
                
                        <Text>{info.end_date}</Text>
                    </View>
                    <View style={{width: '30%'}}>
                        <Text>{info.title}</Text>
                    </View>
                
                </View>
                </TouchableOpacity>
                );
    }
}
const style = StyleSheet.create({
    styleList:{
        flex: 1,
        padding:0,
        margin:0
    },
    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    button: {
            marginLeft: 3,
            backgroundColor: "#6AA6D6",
            // alignSelf:'center',
            padding:0
        },
    buttons: {
        
        backgroundColor: "#6AA6D6",
        alignSelf:'center', 
        // padding:0
        
    },
    group: {
            width:width,
            backgroundColor: variables.lightGray,
            justifyContent: "space-between",
            marginBottom: 1,
            
        },
    text: {
//        paddingLeft: variables.contentPadding * 2,
        paddingBottom:10
    },

});