import moment from "moment";
import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,ListView,AsyncStorage,ActivityIndicator,TouchableHighlight,TouchableOpacity,Dimensions
} from 'react-native';
import {Agenda} from 'react-native-calendars';
import Toast from 'react-native-root-toast';
import {H1,Container,Button,Item,Icon,Input} from "native-base";
import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
const {height, width} = Dimensions.get("window"); 
export default class AgendaScreen extends Component {
 
  go(key: string) {
    this.props.navigation.navigate(key);
}
go1(key: string, detail_data: object) {
    this.props.navigation.navigate(key, detail_data);
}
constructor(props) {
   super(props);
   
   this.state = {
    //  isLoading: true,
        isLoading: true,
        items: {},
        page:1,
        output_key:'',
        search:'',
        keyword:'',
        uid:'',
        token:'',
        loadMore:true,
        start_dates:[],
        group:[],
        dateCalen:{},
        getCalen:{}
   };
   
  //  this.
   this.toast_configuration = {
       duration: Toast.durations.SHORT,
       position: Toast.positions.BOTTOM,
       shadow: true,
       animation: true,
       hideOnPress: true,
       delay: 0,
   };
   
}

async componentDidMount() {
    const {state} = this.props.navigation;
    console.log(state.params);
    if(state.params!=undefined){
        const data = state.params.data;
        let output_key = state.params.output_key;
        
        
        if(output_key==undefined){
          output_key='';
        }
       
        this.setState({
            isLoading: false,
            output_key: output_key
        });
        console.log(this.state.output_key+"ngango");
    }
}
async componentWillMount() {
    const {state} = this.props.navigation;
    const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
    let output_list_url = Global.API_URL + "appointments/";
    output_list_url += user_dataa.uid + "/";

    output_list_url += user_dataa.token + "/";
    output_list_url += this.state.output_key + "/";
    output_list_url +=  1+ "/";
    output_list_url += 100;

    console.log(output_list_url);
    return fetch(output_list_url)
        .then((response) => response.json())
        .then((responseJson) => {
          if(responseJson.data.length==0){
            this.setState({isLoading: false})
        }
            if('1' == responseJson.status&&responseJson.data.length!=0) {
                mang =responseJson.data;
                // this.group = responseJson.data;
                this.setState({
                    isLoading: false,
                    group : responseJson.data,
                    start_dates : responseJson.data.date_time,
                   
                    uid:user_dataa.uid ,
                    token:user_dataa.token,

            },function(){
              console.log(this.state.group);
              let lengh=this.state.group.length;
              if(lengh>0){
                for(let i=0;i<lengh;i++){
      
                  // console.log(this.state.group[i].appointments);
                  for(let j=0;j<this.state.group[i].appointments.length;j++){
                        let date = moment(this.state.group[i].appointments[j].date_time);
                        let setDate=date.format("YYYY-MM-DD");
                        let strTime = setDate;
                        this.state.items[strTime] = [];
                        console.log(strTime);
                        // console.log(12);
                        for(let k=0;k<this.state.group[i].appointments.length;k++){
                          
                          if(strTime==moment(this.state.group[i].appointments[k].date_time).format("YYYY-MM-DD")){
                               console.log(strTime);
                              this.state.items[strTime].push({
                              date_time:  this.state.group[i].appointments[k].date_time,
                              phone1: this.state.group[i].phone1,
                              phone2: this.state.group[i].phone2,
                              first_name: this.state.group[i].first_name,
                              last_name: this.state.group[i].last_name,
                              lead_id:this.state.group[i].id,
                              type_view:"Calender",
                              id:this.state.group[i].appointments[k].id,
                              title:this.state.group[i].appointments[k].title,
                              end_date:this.state.group[i].appointments[k].end_date,
                              height: height/5
                            });
                          }
                          
                        }
                        
                      }
                     
                    }
                    
              }
              
            });
        }
        
        
        })
    .catch((error) => {
        console.error(error);
    });

    
}
async _onEndReached() {
  const {state} = this.props.navigation;
  console.log(this.state.search);
  if(this.state.loadMore){
  numpage=this.state.page+1;
  const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
  let output_list_url = Global.API_URL + "appointments/";
  output_list_url += user_dataa.uid + "/";

  output_list_url += user_dataa.token + "/";
  output_list_url += this.state.output_key + "/";

  output_list_url +=  numpage+ "/";
  output_list_url += 100;
      return fetch(output_list_url)
          .then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.data.length==0){
              this.setState({isLoading: false,loadMore:false})
          }
           if('1' == responseJson.status&&responseJson.data.length!=0) {
              mang=mang.concat(responseJson.data);
              this.setState({
                  
                  isLoading: false,
                  group : responseJson.data,
                  
                  page: this.state.page+1

              },function(){
              
                let lengh=this.state.group.length;
                if(lengh>0){
                  for(let i=0;i<lengh;i++){
                    
                    
                    
                    // console.log(this.state.group[i].appointments);

                    for(let j=0;j<this.state.group[i].appointments.length;j++){
                          let date = moment(this.state.group[i].appointments[j].date_time);
                          let setDate=date.format("YYYY-MM-DD")
                          let strTime = setDate;
                          console.log(strTime);
                          this.state.items[strTime] = [];
                          // console.log(12);
                          for(let k=0;k<this.state.group[i].appointments.length;k++){
                            if(strTime==moment(this.state.group[i].appointments[k].date_time).format("YYYY-MM-DD")){
                            this.state.items[strTime].push({
                              date_time:  this.state.group[i].appointments[k].date_time,
                              phone1: this.state.group[i].phone1,
                              phone2: this.state.group[i].phone2,
                              first_name: this.state.group[i].first_name,
                              last_name: this.state.group[i].last_name,
                              lead_id:this.state.group[i].id,
                              type_view:"Calender",
                              id:this.state.group[i].appointments[k].id,
                              title:this.state.group[i].appointments[k].title,
                              end_date:this.state.group[i].appointments[k].end_date,
                              height: height/5
                            });
                          }
                          }
                          
                        }
                       
                        
                }
                }
                
              }); 
          }
        })
      .catch((error) => {
      console.error(error);
      });
  
    }
}
  go(key: string, detail_data: object) {
    
    this.props.navigation.navigate(key, detail_data);
  }
  loadItems(day) {
    console.log(this.state.items[day.dateString]);
    if(this.state.items[day.dateString]!=undefined){
        var name_day=day.dateString;
        
        var key_day = Object.keys(this.state.dateCalen);
        console.log(key_day.length+"asad");
        if(key_day.length==0){
          this.state.dateCalen[day.dateString]=this.state.items[day.dateString];
          var key_day = Object.keys(this.state.dateCalen);
          console.log(key_day.length+"ngango");
        }
        console.log(key_day);
        if(key_day[0] != name_day){
          this.state.dateCalen[name_day]=this.state.dateCalen[key_day[0]];
          delete this.state.dateCalen[key_day[0]];
          console.log(this.state.items[day.dateString]);
          this.state.dateCalen[name_day]=this.state.items[day.dateString];
          // console.log(this.state.dateCalen);
          key_day[0]=name_day;
        }
        // console.log(this.state.dateCalen);
        
    }
  }
  render() {  
    console.log(this.state.output_key+"ngango");
    if (this.state.isLoading) {
        return (
          <View style={{flex: 1, paddingTop: 60}}>
            <ActivityIndicator />
          </View>
        );
    }
    // this.state.dateCalen=[];
    
    // console.log("ngango hehe");
    const {navigation} = this.props;
    const currentDate = moment(Date(new Date)).format('YYYY-MM-DD');
    return (
      // <View>
      // {this.state.output_key!=''? <Button onPress={() =>this.go1("AddApointment",{data:"None", output_key: this.state.output_key})}>
      // <Text>Add</Text>
      // </Button>:<Text></Text>}
      // </View>
      
      // Console.log()
      
      <BaseContainer title="Calender" {...{ navigation }}>
            <Container >
            <View style={Styles.listItem}>
                    {this.state.output_key!=''? <Button style={styles.button} onPress={() =>this.go1("AddApointment",{data:"None", lead_id: this.state.output_key})}>
                        <Text style={{color:"white"}}>Add</Text>
                    </Button>:<Text></Text>}
                    {this.state.output_key==''?<Button onPress={() =>this.props.navigation.navigate("Appointment")} style={styles.button}>
                    <Text style={{color:"white"}}>List more</Text>
                    </Button>:<Text></Text>}
                    
                </View>
      <Agenda
        // items={
            
        //     {'2018-07-28': [{"name":"ngango",height: 113}],
        //     "2018-07-27":[{"name":"hello",height: 113}]
        //     }}
        items={this.state.dateCalen}
        
        selected={currentDate}
        loadItemsForMonth={this.loadItems.bind(this)}
        // onDayChange={(day)=>{this.loadItems.bind(day.dateString)}}
        // onDayPress={(day) => {this.selectedOK(day.dateString,this.state.items)}}
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={() => {return (<View />);}}
        renderEmptyData = {() => {return (<View />);}}
        rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}
        
      />
      </Container>
        </BaseContainer>
    );
  }
  
 
  
 
  renderItem(item) {
    console.log(this.state.output_key+"nga1234");
    if(this.state.output_key!=''){
    return (
      <TouchableOpacity onPress={() => this.go("AddApointment",{data: item, output_key: item.id})}>
        <View style={[styles.item, {height: item.height}]}>
          <Text>{item.date_time} - {item.end_date}</Text>
          <Text>{item.first_name+" "+ item.last_name}</Text>
          <Text>{item.title}</Text>
          <Text>{item.phone1}</Text>
          <Text>{item.phone2}</Text>
        </View>
      </TouchableOpacity>
    );
  }else{
    return (
      
      <View style={[styles.item, {height: item.height}]}>
        <Text>{item.date_time} - {item.end_date}</Text>
        <Text>{item.first_name+" "+ item.last_name}</Text>
        <Text>{item.title}</Text>
        <Text>{item.phone1}</Text>
        <Text>{item.phone2}</Text>
      </View>
    
    );
  }
    
  }
  
  renderEmptyDate() {
    // console.log("renderEmptyDate");
    return (
      <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
    );
  }

//   rowHasChanged(r1, r2) {
//     return r1.name !== r2.name;
//   }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  },
  styleList:{
    flex: 1,
    padding:0,
    margin:0
},
circle: {
    marginVertical: variables.contentPadding * 4
},
badge: {
    position: "absolute",
    right: 10,
    top: 10
},
button: {
        marginLeft: 3,
        backgroundColor: "#6AA6D6",
        alignSelf:'center',
        padding:0
    },
buttons: {
    
    backgroundColor: "#6AA6D6",
    alignSelf:'center', 
    // padding:0
    
},

text: {
//        paddingLeft: variables.contentPadding * 2,
    paddingBottom:10
},
});