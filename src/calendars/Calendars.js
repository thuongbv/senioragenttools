import moment from "moment";
import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,Platform,AsyncStorage,ActivityIndicator,Linking,TouchableOpacity,Dimensions
} from 'react-native';
import {Agenda} from 'react-native-calendars';
import Toast from 'react-native-root-toast';
import {H1,Container,Button,Item,Input} from "native-base";
import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import Icon from 'react-native-vector-icons/FontAwesome';
const {height, width} = Dimensions.get("window"); 
export default class AgendaScreen extends Component {
 
  go(key: string) {
    this.props.navigation.navigate(key);
}
go1(key: string, detail_data: object) {
    this.props.navigation.navigate(key, detail_data);
}
constructor(props) {
   super(props);
   
   this.state = {
    //  isLoading: true,
        isLoading: true,
        items: [],
        page:1,
        output_key:'',
        search:'',
        keyword:'',
        uid:'',
        token:'',
        loadMore:true,
        start_dates:[],
        group:[],
        dateCalen:{},
        getCalen:{},
        strTime:'',
   };
   
   this.arr=[];
   this.toast_configuration = {
       duration: Toast.durations.SHORT,
       position: Toast.positions.BOTTOM,
       shadow: true,
       animation: true,
       hideOnPress: true,
       delay: 0,
   };
   
}

async componentDidMount() {
    const {state} = this.props.navigation;
    console.log(state.params);
    if(state.params!=undefined){
        const data = state.params.data;
        let output_key = state.params.output_key;
        
        
        if(output_key==undefined){
          output_key='';
        }
       
        this.setState({
            // isLoading: false,
            output_key: output_key
        });
        console.log(this.state.output_key+"ngango");
    }
}
async componentWillMount() {
    const {state} = this.props.navigation;
    
    const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
    let output_list_url = Global.API_URL + "appointments/";
    output_list_url += user_dataa.uid + "/";

    output_list_url += user_dataa.token + "/";
    output_list_url += this.state.output_key + "/";
    output_list_url +=  1+ "/";
    output_list_url += 100;
    console.log(output_list_url);
    
    return fetch(output_list_url)
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if(responseJson.data.length==0){
            this.setState({isLoading: false})
        }
            if('1' == responseJson.status&&responseJson.data.length!=0) {
                mang =responseJson.data;
                // this.group = responseJson.data;
                this.setState({
                    // isLoading: false,
                    group : responseJson.data,
                    start_dates : responseJson.data.date_time,
                   
                    uid:user_dataa.uid,
                    token:user_dataa.token,

            },function(){
             
              let lengh=this.state.group.length;
              
              if(lengh>0){
                this.arr=new Array();
                let items = new Array();
                let strTime = '';
                for(let i=0;i<lengh;i++){
                  // for(let j=0;j<this.state.group[i].appointments.length;j++){
                    strTime=moment(this.state.group[i].date_time).format("YYYY-MM-DD")
                    items[strTime]=[];
                    const appointment_title_replace = this.state.group[i].first_name + ' ' + this.state.group[i].last_name + ' ' + '(Phone: ' + this.state.group[i].phone1 + ')';
                    const appointment_title = this.state.group[i].title.replace(appointment_title_replace, '');
                    this.arr.push({
                          start_date: this.state.group[i].date_time,
                          date_time:  this.state.group[i].date_time,
                          date_time_display:  moment(this.state.group[i].date_time).format('hh:mm a'),
                          phone1: this.state.group[i].phone1,
                          phone2: this.state.group[i].phone2,
                          first_name: this.state.group[i].first_name,
                          last_name: this.state.group[i].last_name,
                          lead_id:this.state.group[i].lead_id,
                          type_view:"Calendar",
                          id:this.state.group[i].id,
                          title_display:appointment_title,
                          title: this.state.group[i].title,
                          end_date:this.state.group[i].end_date,
                          height: height/5,
                          duration: this.state.group[i].duration,
                          note: this.state.group[i].quick_note,
                          description: this.state.group[i].description
                        });
                      
                     
                    }
                
               
                console.log(this.arr.length);
                
                for(let i=0;i<this.arr.length;i++){
                 const strs=moment(this.arr[i].start_date).format("YYYY-MM-DD");
                //  console.log(this.state.items.hasOwnProperty(strs), strs);
                  if(items.hasOwnProperty(strs)){
                    items[strs].push(this.arr[i]);
                  }
                
                }
                this.setState({items: items, isLoading: false})
              }
              console.log(this.arr);
              // console.log( this.state.items);
              
            });
        }        
        })
    .catch((error) => {
        console.error(error);
    });

    
}
async _onEndReached() {
  const {state} = this.props.navigation;
  if(this.state.loadMore){
  numpage=this.state.page+1;
  const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
  let output_list_url = Global.API_URL + "appointments/";
  output_list_url += user_dataa.uid + "/";

  output_list_url += user_dataa.token + "/";
  output_list_url += this.state.output_key + "/";

  output_list_url +=  numpage+ "/";
  output_list_url += 100;
      return fetch(output_list_url)
          .then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.data.length==0){
              this.setState({isLoading: false,loadMore:false})
          }
           if('1' == responseJson.status&&responseJson.data.length!=0) {
              mang=mang.concat(responseJson.data);
              this.setState({
                  
                  isLoading: false,
                  group : responseJson.data,
                  
                  page: this.state.page+1

              },function(){
              
                let lengh=this.state.group.length;
              
                if(lengh>0){
                this.arr=new Array();
                for(let i=0;i<lengh;i++){
                  // for(let j=0;j<this.state.group[i].appointments.length;j++){
                    this.state.strTime=moment(this.state.group[i].date_time).format("YYYY-MM-DD")
                    this.state.items[this.state.strTime]=[];
                    const appointment_title_replace = this.state.group[i].first_name + ' ' + this.state.group[i].last_name + ' ' + '(Phone: ' + this.state.group[i].phone1 + ')';
                    const appointment_title = this.state.group[i].title.replace(appointment_title_replace, '');
                    this.arr.push({
                          start_date: this.state.group[i].date_time,
                          date_time:  this.state.group[i].date_time,
                          date_time_display:  moment(this.state.group[i].date_time).format('hh:mm a'),
                          phone1: this.state.group[i].phone1,
                          phone2: this.state.group[i].phone2,
                          first_name: this.state.group[i].first_name,
                          last_name: this.state.group[i].last_name,
                          lead_id:this.state.group[i].lead_id,
                          type_view:"Calendar",
                          id:this.state.group[i].id,
                          title_display:appointment_title,
                          title: this.state.group[i].title,
                          end_date:this.state.group[i].end_date,
                          height: height/5,
                          description: this.state.group[i].description
                        });
                      // }
                     
                    }
                
                // for(var i=0;i<this.arr.length;i++){
                
                //   let strs=moment(this.arr[i].date_time).format("YYYY-MM-DD");
                //   this.state.items[strs]=[];
                console.log(this.arr.length);
                //  }
                for(let i=0;i<this.arr.length;i++){
                 let strs=moment(this.arr[i].start_date).format("YYYY-MM-DD");
                 console.log(this.state.items.hasOwnProperty(strs));
                  if(this.state.items.hasOwnProperty(strs)){
                    this.state.items[strs].push(this.arr[i]);
                  }
                
                }
              }
              console.log(this.arr);
              console.log( this.state.items);
                
              }); 
          }
        })
      .catch((error) => {
      console.error(error);
      });
  
    }
}
  
  loadItems(day) {
    console.log(this.state.items[day.dateString]);
    console.log(day);
    if(this.state.items[day.dateString]!=undefined){
        var name_day=day.dateString;
        
        var key_day = Object.keys(this.state.dateCalen);
        // console.log(key_day.length+"asad");
        if(key_day.length==0){
          this.state.dateCalen[day.dateString]=this.state.items[day.dateString];
          var key_day = Object.keys(this.state.dateCalen);
          console.log(key_day.length+"ngango");
        }
        // console.log(key_day);
        if(key_day[0] != name_day){
          this.state.dateCalen[name_day]=this.state.dateCalen[key_day[0]];
          delete this.state.dateCalen[key_day[0]];
          // console.log(this.state.items[day.dateString]);
          this.state.dateCalen[name_day]=this.state.items[day.dateString];
          // console.log(this.state.dateCalen);
          key_day[0]=name_day;
        }
        // console.log(this.state.dateCalen);
        
    }
  }
  render() {  
    // console.log(this.state.output_key+"ngango");
    if (this.state.isLoading) {
        return (
          <View style={{flex: 1, paddingTop: 60}}>
            <ActivityIndicator />
          </View>
        );
    }
    // this.state.dateCalen=[];
    
    // console.log("ngango hehe");
    const {navigation} = this.props;
    const currentDate = moment(Date(new Date)).format('YYYY-MM-DD');
    return (
      // <View>
      // {this.state.output_key!=''? <Button onPress={() =>this.go1("AddApointment",{data:"None", output_key: this.state.output_key})}>
      // <Text>Add</Text>
      // </Button>:<Text></Text>}
      // </View>
      
      // Console.log()
      
      <BaseContainer title="Calendar" {...{ navigation }}>
            <Container >
            <View style={Styles.listItem}>
                    {this.state.output_key!=''? <Button style={styles.button} onPress={() =>this.go1("AddApointment",{data:"None", lead_id: this.state.output_key, from_lead_view: this.state.output_key!=''})}>
                        <Text style={{color:"white"}}>Add</Text>
                    </Button>:<Text></Text>}
                    {this.state.output_key==''?<Button onPress={() =>this.props.navigation.navigate("Appointment")} style={styles.button}>
                    <Text style={{color:"white"}}>List more</Text>
                    </Button>:<Text></Text>}
                    
                </View>
      <Agenda
        // items={
            
        //     {'2018-07-28': [{"name":"ngango",height: 113}],
        //     "2018-07-27":[{"name":"hello",height: 113}]
        //     }}
        items={this.state.dateCalen}
        
        selected={currentDate}
        loadItemsForMonth={this.loadItems.bind(this)}

        // onDayChange={(day)=>{this.loadItems.bind(day.dateString)}}
        // onDayPress={(day) => {this.selectedOK(day.dateString,this.state.items)}}    

        renderItem={(item) => this.renderItem(item)}
        renderEmptyDate={() => {return (<View />);}}
        renderEmptyData = {() => {return (<View />);}}
        rowHasChanged={(r1, r2) => {return r1 !== r2}}
        
      />
      </Container>
        </BaseContainer>
    );
  }
  
 
  onPressCall(phoneNumber) {
    let url;
    let prompt=true;
if(Platform.OS !== 'android') {
  url = prompt ? 'telprompt:' : 'tel:';
}
else {
  url = 'tel:';
}

    url += phoneNumber;
    
    Linking.canOpenURL(url).then(supported => {
        if(!supported) {
            console.log('Can\'t handle url: ' + url);
        } else {
            Linking.openURL(url)
            .catch(err => {
                if(url.includes('telprompt')) {
                    // telprompt was cancelled and Linking openURL method sees this as an error
                    // it is not a true error so ignore it to prevent apps crashing
                    // see https://github.com/anarchicknight/react-native-communications/issues/39
                } else {
                    console.warn('openURL error', err)
                }
            });
        }
    }).catch(err => console.warn('An unexpected error happened', err));
}
 
  renderItem(item) {
    // console.log(this.state.output_key+"nga1234");
    // console.log(item);
  //   if(this.state.output_key!=''){
  //   return (
  //     <TouchableOpacity onPress={() => this.go("ViewLead",{data: "None", output_key: item.lead_id, return_page: "Calendars"})}>
  //       <View style={[styles.item, {height: item.height}]}>
  //         <Text>{item.date_time}</Text>
  //         <Text>{item.first_name+" "+ item.last_name}</Text>
  //         <Text>{item.title}</Text>
  //         <Text>{item.phone1}</Text>
  //         <Text>{item.phone2}</Text>
  //       </View>
  //     </TouchableOpacity>
  //   );
  // }else{
    return (
      
      <View style={[styles.item, {height: item.height}]}>
        <Text>{item.date_time_display}</Text>
        <View >
          <Icon size={16} name="search"  style={{flex: 1,  alignSelf: 'flex-start'}}/> 
          <Text>{0===item.duration ? 1 : item.duration}h</Text>
        </View>
        
        <TouchableOpacity onPress={() => this.go1("ViewLead",{data: "None", output_key: item.lead_id, return_page: "Calendars"})}><Text>{item.first_name+" "+ item.last_name}</Text></TouchableOpacity>
        <Text>{item.title_display}</Text>
        <TouchableOpacity onPress={() => this.go1("AddApointment", {data: item, output_key: item.id, from_lead_view: this.state.output_key!=''})}>
          <Text>Edit</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.onPressCall(item.phone1)}>
          <Text>{item.phone1}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.onPressCall(item.phone2)}>
          <Text>{item.phone2}</Text>
        </TouchableOpacity>
        <Text>{item.note}</Text>
      </View>
    
    );
  // }
    
  }
  
  renderEmptyDate() {
    // console.log("renderEmptyDate");
    return (
      <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
    );
  }

//   rowHasChanged(r1, r2) {
//     return r1.name !== r2.name;
//   }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  },
  styleList:{
    flex: 1,
    padding:0,
    margin:0
},
circle: {
    marginVertical: variables.contentPadding * 4
},
badge: {
    position: "absolute",
    right: 10,
    top: 10
},
button: {
        marginLeft: 3,
        backgroundColor: "#6AA6D6",
        alignSelf:'center',
        padding:0
    },
buttons: {
    
    backgroundColor: "#6AA6D6",
    alignSelf:'center', 
    // padding:0
    
},

text: {
//        paddingLeft: variables.contentPadding * 2,
    paddingBottom:10
  },
});