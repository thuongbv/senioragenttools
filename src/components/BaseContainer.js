// @flow
import React, {Component} from "react";
import {View, ScrollView,StyleSheet, PermissionsAndroid, Image,Text,AsyncStorage,ActivityIndicator} from "react-native";
import {Container, Button, Header as NBHeader, Left, Body, Title, Right,Footer, FooterTab,Drawer,H1} from "native-base";

import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import Avatar from "./avatar/Avatar";
import {Images,NavigationHelpers, WindowDimensions, Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import Icon from 'react-native-vector-icons/FontAwesome';
// import Contacts from 'react-native-contacts';
import * as Progress from 'react-native-progress';

export default class BaseContainer extends Component {
    constructor(props) {
        super(props);
        this.state= {
            open_drawer: false
        }
     }


   
    props: {
        title: string | React$Element<*>,
        navigation: NavigationScreenProp<*, *>,
        scrollable?: boolean,
        children?: React$Element<*>
    }
    go(key: string) {
        if(this.props.close_popup) {
            this.props.close_popup();
        }
        this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        // console.log(this.props);
        if(this.props.close_popup) {
            this.props.close_popup();
        }
        this.props.navigation.navigate(key, detail_data);
    }
    // closeControlPanel(){
    //     this.refs.controlPanel.closeDrawer()
    //   }
    //   openControlPanel(){
    //     this.refs.controlPanel.openDrawer()
    //   }

    render(): React$Element<*> {
        const {title, navigation, hide_footer, close_popup} = this.props;
                
        return <Container >
            <Drawer
        // ref={(ref) => this._drawer = ref}
        content={<DrawerSidebar {...{ navigation }}/>}
        open={this.state.open_drawer}
        type="displace"
        styles={drawerStyles}
        >
                <NBHeader style={{backgroundColor: "#436E90"}}>
                        <Left>
                            <Icon.Button name="bars" size={32} backgroundColor={"#436E90"} 
                            // onPress={() => this.props.navigation.navigate("DrawerOpen")}
                            onPress={() => this.setState({open_drawer: true})}
                             transparent/>
                        </Left>
                        <Body>
                        {
                        typeof(title) === "string" ? <Title style={style.cuzTil}>{title}</Title> : title
                        }
                        </Body>
                        <Right style={{ alignItems: "center" }}>
                            <Button transparent onPress={() => navigation.navigate("Profile")}>
                            </Button>
                        </Right>
                    </NBHeader>
                    {
                            this.props.children
                    }
                    <Footer style={{bottom:0, height: hide_footer ? 0 :'auto'}}>
                        <FooterTab style={{backgroundColor:"#436E90"}}>
                            <Button vertical onPress={() => this.go("Lead")} >
                                <Icon color="white" size={26} name="tasks" />
                                <Text style={style.btn_text}>List</Text>
                            </Button>
                            <Button vertical onPress={() => this.go("MapsView")} >
                                <Image source={Images.icon_map} />
                                <Text style={style.btn_text}>Map</Text>
                            </Button>
                            <Button vertical onPress={() => this.go("AddLead")} >
                                <Image source={Images.add_lead} />
                                <Text style={style.btn_text}>Add</Text>
                            </Button>
                            <Button vertical onPress={() => this.go("Calendars")} >
                                <Image source={Images.Calendar} />
                                <Text style={style.btn_text}>Calendar</Text>
                            </Button>
                            <Button vertical onPress={() => this.go("Filter")} >
                                <Icon color="white" size={26} name="filter" />
                                <Text style={style.btn_text}>Filter</Text>
                            </Button>
                            <Button vertical onPress={() => this.go1('Lead', {is_focus_search: true})}>
                                <Icon color="white" size={26} name="search" />
                                <Text style={style.btn_text}>Search</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
            </Drawer>
                
            </Container>;
    }
}


class DrawerSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showProgress: false,
            progress: 0,
            indeterminate: true,
            progressText: 'loading'
          };
     }

    go(key: string) {
        this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    // @autobind
    // login() {
    //     NavigationHelpers.reset(this.props.navigation, "Login");
    // }
    logout() {
            AsyncStorage.multiRemove(['user_data'], () => {
                     NavigationHelpers.reset(this.props.navigation, "Login");
                 });

    }

    // async askPermissionContacts() {
    //     // get permission
    //     const read_permision_allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_CONTACTS);
    //     const write_permision_allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS);
        
    //     if(read_permision_allowed && write_permision_allowed) {
    //         this.addContacts();
    //     } else {
    //         const granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_CONTACTS, PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS]);
    //           if (granted['android.permission.READ_CONTACTS'] === PermissionsAndroid.RESULTS.GRANTED && granted['android.permission.WRITE_CONTACTS'] === PermissionsAndroid.RESULTS.GRANTED) {
    //             console.log("You can read/write contacts");
    //             this.addContacts();
    //           } else {
    //             console.log("Read/write permission denied");
    //           }
    //     }
    // }

    // addContacts() {
    //     this.setState({showProgress: true,indeterminate: false}, async function() {
    //         // this.animateProgress();
    //         // get list lead
    //         const user_data = JSON.parse(await AsyncStorage.getItem('user_data'));
    //         let lead_url = Global.API_URL + "contacts/";
    //         lead_url += user_data.uid + "/";
    //         lead_url += user_data.token;
    //         console.log(lead_url);
            
    //         return fetch(lead_url)
    //             .then((response) => response.json())
    //             .then((responseJson) => {
            
    //                 if('1' == responseJson.status) {
                        
    //                     for(const contact of responseJson.data) {
                            
    //                         const newPerson = {
    //                             emailAddresses: [{
    //                             label: "work",
    //                             email: contact.email_address,
    //                             }],
    //                             familyName: contact.last_name,
    //                             givenName: contact.first_name,
    //                             phoneNumbers: [{
    //                                 label: "phone1",
    //                                 number: contact.phone1,
    //                             },
    //                             {
    //                                 label: "phone2",
    //                                 number: contact.phone2,
    //                             }
    //                             ],
    //                         }
    //                         console.log(newPerson);
    //                         Contacts.addContact(newPerson, async (err) => {
    //                             if (err) throw err;
    //                             // save successful                                
    //                         })
    //                     }
    //                 }
    //             }).then(() => {
    //                 this.setState({showProgress: false});
    //             })
    //         .catch((error) => {
    //             console.error(error);
    //         });
    //     })
        
    // }

    render(): React$Element<*> {
        if (this.state.showProgress) {
            
            return (
                <View style={drawerStyles.circles}>

                {/* <Progress.Circle
                  style={drawerStyles.progress}
                  progress={this.state.progress}
                  indeterminate={this.state.indeterminate}
                  direction="counter-clockwise"
                  size={140}
                  showsText={true}
                  formatText={() => this.getProgressText()}
                /> */}
                <ActivityIndicator />
                
              </View>
            );
        }
        const navState = {
            routes:["Lead", "MapsView", "Filter", "AddLead", "Calendars", "ActivityLog"] //, "Settings"
        };
        
        const items = navState.routes
            .map((route, i) =>{
                return (
                    <DrawerItem key={i} onPress={() => this.go(route)} label={route} active={true} />
                );
            });
            

        return <Container style={style.container}>
                <ScrollView>
                <View style={style.drawerItemsContainer}>
                    <View style={style.drawerItems}>
                        {items}
                        <DrawerItem key={-2} onPress={() => this.go1('Lead', {is_focus_search: true})} icon="search" label="Search Leads"  active={true}/>
                        {/* <DrawerItem key={-3} onPress={() => this.askPermissionContacts()} icon="address-card" label="Add Contacts"  active={true}/> */}
                        <DrawerItem key={-1} onPress={() => this.logout()} icon="power-off" label="Logout" active={true} />
                    </View>
                    
                </View>
                </ScrollView> 
            </Container>;
    }
}

class DrawerItem extends Component {

    props: {
        label: string,
        onPress: () => void,
        active?: boolean,
        icon: string
    }

    render(): React$Element<Button> {
        const {label, onPress, active, icon} = this.props;
        let icon_name = '';
        
        switch (label) {
            case 'Home':
                icon_name = 'home';
                this.label='Home';
                break; 
            case 'Lead':
                icon_name = 'tasks';
                this.label='List View';
                break;
            case 'Logout':
                icon_name = 'power-off';
                this.label='Sign Out';
                break;
            case 'MapsView':
                 icon_name = 'map-marker';
                 this.label='Map View';
                 break; 
            case 'MyProfile':
                icon_name = 'user';
                break;
            case 'MyCounties':
                icon_name = 'road';
                break;

            case 'MyStatus':
                icon_name = 'codiepie';
                break;
            case 'Calendars':
                icon_name = Images.add_appointment;
                this.label='Calendar';
                break;
            case 'Contact':
                icon_name = 'info-circle';
                break;
            case 'Settings':
                icon_name = 'wrench';
                this.label='Settings';
                break;
            case 'ActivityLog':
                icon_name = Images.emailed;
                this.label='Activity Log';
                break;
            case 'Filter':
                icon_name = 'filter';
                this.label='Filter';
                break;
            case 'Setter':
                icon_name = 'flickr';
                break;
            case 'AddLead':
                icon_name = Images.add_lead;
                this.label = 'Add Lead';
                break;
           default:
                icon_name = icon;
                this.label = label;
                break;
        }
        if(this.label=='Activity Log'){
            return (<Icon.Button iconLeft onPress={onPress} transparent backgroundColor="#22262E" >
                        <Image source={icon_name} style={{marginLeft:-10}}/>
                        <H1 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)", fontSize: 20, paddingLeft: 5 }}>{' '+this.label}</H1>
                    </Icon.Button>)
        }else if(this.label=='Calendar'){
            return (<Icon.Button iconLeft onPress={onPress} transparent backgroundColor="#22262E" >
                <Image source={icon_name} style={{marginLeft:-10, width: 26, height: 26}}/>
                <H1 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)", fontSize: 20, paddingLeft: 5 }}>{' '+this.label}</H1>
            </Icon.Button>)
        }else if(this.label=='Add Lead'){
            return (<Icon.Button iconLeft onPress={onPress} transparent backgroundColor="#22262E" >
                <Image source={icon_name} style={{marginLeft:-10}}/>
                <H1 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)", fontSize: 20, paddingLeft: 5 }}>{' '+this.label}</H1>
            </Icon.Button>)
        }
        
        else{
            return (<Icon.Button name={icon_name} onPress={onPress} full transparent backgroundColor="#22262E"  color={ color= active ? "white" : "rgba(255, 255, 255, .5)" }>
            <H1 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)", fontSize: 20, paddingLeft: 5 }}>{this.label}</H1>
        </Icon.Button>);
        }
        
    }
}

const style = StyleSheet.create({
    cuzTil:{
        color:"white",
        textAlign: 'center', // <-- the magic
        
    },
    btn_text: {fontSize: 6.5},
    btn: {padding:0,margin:0},
//drawer
    img: {
        ...WindowDimensions
    },
    container: {
        backgroundColor: "#22262E",
        paddingLeft: variables.contentPadding, //1.5
        paddingVertical: variables.contentPadding * 2.5
        
    },
    mask: {
        color: "rgba(255, 255, 255, .5)"
    },
    closeIcon: {
        fontSize: 30,
        color: "rgba(255, 255, 255, .5)"
    },
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "stretch"
    },
    drawerItemsContainer: {
        flex: 1,
        justifyContent: "center",
        // paddingVertical: variables.contentPadding * 6
    },
    drawerItems: {
        flex: 1,
        
        padding:0, 
        justifyContent: "space-between"
    },
    drawerIcon: {
        justifyContent: "center",
        alignItems: "stretch"
    },
    drawerIcons: {
        width:30,
        flex: 1,
        
        justifyContent: "space-between"
    }
});

const drawerStyles = {
    drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
    circles: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      },
      progress: {
        margin: 10,
      },
  }