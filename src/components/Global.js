// @flow
const Global = {
    API_URL: "https://senioragenttools.com/services/navigator_test/api/index.php/",
    MAX_PREFIX_COUNT: 200
};

export default Global;