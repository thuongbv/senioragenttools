// @flow
import { NavigationActions } from "react-navigation"
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

export default class NavigationHelpers {
    static reset(navigation: NavigationScreenProp<*, *>, route: string) {
        const action = NavigationActions.reset({
            index: 0,
            // key: null,
            actions: [
                NavigationActions.navigate({ routeName: route })
            ]
        });
        navigation.dispatch(action);
        // setTimeout(navigation.dispatch(action), 500);
    }

    static resetWithParam(navigation: NavigationScreenProp<*, *>, route: string, param_object: object) {
        const action = NavigationActions.reset({
            index: 0,
            // key: null,
            actions: [
                NavigationActions.navigate({ routeName: route, params: param_object  })
            ]
        });
        navigation.dispatch(action);
        // setTimeout(navigation.dispatch(action), 500);
    }

    static moveBack(navigation: NavigationScreenProp<*, *>) {
        navigation.dispatch(NavigationActions.back({ key: null}));
    }
}