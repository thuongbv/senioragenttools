// @flow
import {StyleSheet, Dimensions} from "react-native";

import variables from "../../native-base-theme/variables/commonColor";

const {width, height} = Dimensions.get("window");
const Styles = StyleSheet.create({
    imgMask: {
        backgroundColor: "#000033" //backgroundColor: "rgba(0, 0, 51, 0.8)", //backgroundColor: "rgba(80, 210, 194, .8)",
    },
    bgContainer: {
        // fontFamily: 'Inter-UI-Regular',
        // backgroundColor: "#ECF0F3",
       
        // color:"black"
    },
    header: {
        width,
        height: width * 440 / 750
    },
    flexGrow: {
        flex: 1
    },
    center: {
        justifyContent: "center",
        alignItems: "center"
    },
    textCentered: {
        textAlign: "center"
    },
    bg: {
        backgroundColor: "white"
    },
    row: {
        flexDirection: "row"
    },
    whiteBg: {
        backgroundColor: "white"
    },
    whiteText: {
        color: "white"
    },
    grayText: {
        color: variables.gray
    },
    listItem: {
        flexDirection: "row",
        padding:10,
        marginRight:20
    },
    listItemView: {
            flexDirection: "row",
            padding:3,
        },
    btnAdd: {
        borderWidth: 2,
        borderStyle: "solid",
        borderColor: "black",
        borderRadius: 3,
        width: 48,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10
    },
    imgAdd: {
        width: 30,
        height: 30,
        // resizeMode: "cover",
    },
    insertView: {
        flex: 1,
        flexDirection: 'row',
        borderWidth: 4,
        borderStyle: "solid",
        borderColor: "black",
        borderRadius: 3,
        marginTop: 30,
        padding: 10
    },
    insert_footer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "flex-end",
        position: 'absolute', left: 0, right: 0, bottom: 60,
        
    },
    inset_footer_text: {
        color: "#BB9C02",
        width: 70,
        fontWeight: "600",
    },
    insert_footer_image: {
        width: 45,
        height: 75
    }
});

export default Styles;