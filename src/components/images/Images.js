// @flow
// import {Asset} from "expo";

export default class Images {


//    logo
    static navLogo = require("./logo/navigator_logo.png");
    //icon_lead
    static icon_map = require("./icon_map.png");
    static add_lead = require("./add.png");
    static Calendar = require("./Calendar.png");
    static Upload_CSV = require("./Upload_CSV.png");
    static emailed = require("./emailed.png");
    static list_lead = require("./List.png");
    static Edit = require("../img/edit.png");
    static Filter = require("../img/Filter.png");
    static clear_filter = require("../img/clear_filter.png");
    static add_appointment = require("../img/add_appointment.png");
	
    static search = require("./Search.png");
	 static login = require("./login.jpg");
    static signUp = require("./signUp.jpg");
    static drawer = require("./drawer.jpg");
    static home = require("./home.jpg");
    static lists = require("./lists.jpg");
    static timeline = require("./timeline.jpg");

    static defaultAvatar = require("./avatars/default-avatar.jpg");
    static avatar1 = require("./avatars/avatar-1.jpg");
    static avatar2 = require("./avatars/avatar-2.jpg");
    static avatar3 = require("./avatars/avatar-3.jpg");

    static foodGroup = require("./groups/food.jpg");
    static workGroup = require("./groups/work.jpg");
    static vacationGroup = require("./groups/vacation.jpg");
    static citiesGroup = require("./groups/cities.jpg");

    static top10 = require("./menu/top_10.png");
    static duel = require("./menu/duel.png");
    static quiz = require("./menu/quiz.png");
    static activity = require("./menu/activity.png");
    static rankList = require("./menu/text.png");

    static coin100 = require("./tokens/coin_100.png");
    static coin500 = require("./tokens/coin_500.png");
    static coin1000 = require("./tokens/coin_1000.png");
    static coin1500 = require("./tokens/coin_1500.png");
    static coin2000 = require("./tokens/coin_2000.png");
    static coin2500 = require("./tokens/coin_2500.png");

    static add = require("./add.png");
    static camera = require("./camera.png");
    static token_store = require("./token_store.png");
    static token = require("./token.png");
    static profile = require("./profile.png");

    static star = require("./star.png");
    static like = require("./like.png");
    static unlike = require("./unlike.png");
    static rate = require("./rate.png");

    static left_arrow = require("./left_arrow.png");
    static right_arrow = require("./right_arrow.png");

    static icon_up = require("./icon-up.png");
    static icon_down = require("./icon-down.png");

    // static downloadAsync(): Promise<*>[] {
    //     return [
    //         Asset.fromModule(Images.login).downloadAsync(),
    //         Asset.fromModule(Images.signUp).downloadAsync(),
    //         Asset.fromModule(Images.drawer).downloadAsync(),
    //         Asset.fromModule(Images.home).downloadAsync(),
    //         Asset.fromModule(Images.lists).downloadAsync(),
    //         Asset.fromModule(Images.timeline).downloadAsync(),

    //         Asset.fromModule(Images.defaultAvatar).downloadAsync(),
    //         Asset.fromModule(Images.avatar1).downloadAsync(),
    //         Asset.fromModule(Images.avatar2).downloadAsync(),
    //         Asset.fromModule(Images.avatar3).downloadAsync(),

    //         Asset.fromModule(Images.foodGroup).downloadAsync(),
    //         Asset.fromModule(Images.workGroup).downloadAsync(),
    //         Asset.fromModule(Images.vacationGroup).downloadAsync(),
    //         Asset.fromModule(Images.citiesGroup).downloadAsync(),

    //         Asset.fromModule(Images.top10).downloadAsync(),
    //         Asset.fromModule(Images.duel).downloadAsync(),
    //         Asset.fromModule(Images.quiz).downloadAsync(),
    //         Asset.fromModule(Images.activity).downloadAsync(),
    //         Asset.fromModule(Images.rankList).downloadAsync(),

    //         Asset.fromModule(Images.coin100).downloadAsync(),
    //         Asset.fromModule(Images.coin500).downloadAsync(),
    //         Asset.fromModule(Images.coin1000).downloadAsync(),
    //         Asset.fromModule(Images.coin1500).downloadAsync(),
    //         Asset.fromModule(Images.coin2000).downloadAsync(),
    //         Asset.fromModule(Images.coin2500).downloadAsync(),

    //         Asset.fromModule(Images.add).downloadAsync(),
    //         Asset.fromModule(Images.camera).downloadAsync(),
    //         Asset.fromModule(Images.token_store).downloadAsync(),
    //         Asset.fromModule(Images.token).downloadAsync(),
    //         Asset.fromModule(Images.profile).downloadAsync(),

    //         Asset.fromModule(Images.star).downloadAsync(),
    //         Asset.fromModule(Images.like).downloadAsync(),
    //         Asset.fromModule(Images.unlike).downloadAsync(),
    //         Asset.fromModule(Images.rate).downloadAsync(),

    //         Asset.fromModule(Images.left_arrow).downloadAsync(),
    //         Asset.fromModule(Images.right_arrow).downloadAsync()
    //     ];
    // }
}
