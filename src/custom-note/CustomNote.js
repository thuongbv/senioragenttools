// @flow 
import React, {Component} from "react";
import {Text, Image, StyleSheet, View,ImageBackground,Platform,AsyncStorage,TouchableWithoutFeedback,
    ActivityIndicator,ListView,ScrollView,Dimensions,Alert,Modal,TouchableHighlight,Linking,TouchableOpacity,Keyboard} from "react-native";
import {H3,Container,Button,Picker,Item,Input,Icon} from "native-base";
 import { Col, Row, Grid } from 'react-native-easy-grid';
import Toast from 'react-native-root-toast';
import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global,NavigationHelpers } from "../components";
import Communications from 'react-native-communications';
import variables from "../../native-base-theme/variables/commonColor";
import RNFetchBlob from 'react-native-fetch-blob';
const {width, height} = Dimensions.get("window");

export default class CustomNote extends Component {
      go(key: string, detail_data: object) {
              this.props.navigation.navigate(key, detail_data);
          }
          go1(key) {
            this.props.navigation.navigate(key);
        }  
      constructor(props) {
          super(props);
          this.state = {
            isLoading: true,
            data: {},
            contact:0,
            notes:'',
            status:'',
            uid:'',
            token:'',
            modalVisible: false,
            show_custom_note: false,
            show_contact_result: false,
            hide_footer: false
        };
       
          this.lead_id = 0;
          this.notess =[];
          this.contact_results=[];
      }

      
    async componentWillMount() {
        const {state} = this.props.navigation;
        this.lead_id = state.params.lead_id;
        const show_contact_result = state.params.show_contact_result;

        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));

        let output_list_url_contact = Global.API_URL + "contact_results/"+user_dataa.uid + "/"+user_dataa.token + "/";
        let output_list_url_note = Global.API_URL + "notes/"+user_dataa.uid + "/"+user_dataa.token + "/"+this.lead_id + "/";
        
        fetch(output_list_url_note)
            .then((response) => response.json())
            .then((responseJson) => {
            if('1' == responseJson.status) {
                
                this.notess=responseJson.data;
                fetch(output_list_url_contact)
                    .then((response) => response.json())
                    .then((responseJson) => {
                    
                        if('1' == responseJson.status) {
                            this.contact_results = responseJson.data;
                            // console.log(this.contact_results);
                            this.setState({
                                uid:user_dataa.uid,
                                token:user_dataa.token,
                                show_contact_result: show_contact_result
                            }, function() {
                                // always get lead data from db
                                let output_list_url_lead = Global.API_URL + "lead/";
                                output_list_url_lead += user_dataa.uid + "/";
                                output_list_url_lead += user_dataa.token + "/";
                                output_list_url_lead += this.lead_id + "/";
                                console.log(output_list_url_lead);
                                fetch(output_list_url_lead)
                                    .then((response) => response.json())
                                    .then((responseJson) => {
                                        
                                        if('1' == responseJson.status) {
                                            this.setState({
                                                isLoading: false,
                                                data: responseJson.data,
                                            });
                                        }
                                    }).catch((error) => {
                                        console.error(error);
                                    });
                            });
                        }
                        })
                    .catch((error) => {
                        console.error(error);
                    });
                }
            })
        .catch((error) => {
            console.error(error);
        });
          
    }
   
    addNote=async () =>{
        // call api
        if(this.state.notes.trim()==='') {
            Toast.show('Content note not bank', this.toastConfiguration);
            return;
        }
        const add_note = Global.API_URL + "note/";

        let form_data_add_note = new FormData();

        form_data_add_note.append("uid", this.state.uid);
        form_data_add_note.append("token", this.state.token);
        
        form_data_add_note.append("id", '');
        form_data_add_note.append("lead_id", this.state.data.id);
        form_data_add_note.append("content", this.state.notes);
        
        console.log(form_data_add_note) ;
        try {
           
            let response = await fetch(add_note, {
                method: 'POST',
                headers: {
                },
                body: form_data_add_note
            });
            let responseJson = await response.json();
            

            Toast.show(responseJson.msg, this.toastConfiguration);
            if(1 == responseJson.status) {
                // saving user data
                try {
                    // update note
                    let output_list_url_note = Global.API_URL + "notes/"+this.state.uid + "/"+this.state.token + "/"+this.lead_id + "/";
                    fetch(output_list_url_note)
                    .then((response) => response.json())
                    .then((responseJson) => {
                        if('1' == responseJson.status) {
                            // get top list successfully
                            this.notess=responseJson.data;
                            this.forceUpdate();
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    });
                    
                    this.setState({notes: '',hide_footer:false});
                    Keyboard.dismiss();
                    this.go("ViewLead", {data: this.state.data, output_key: this.lead_id});

                } catch (error) {
                // Error saving data
                    console.error(error);
                    Toast.show(error, this.toastConfiguration);

                }

            }else{
                Toast.show(responseJson.msg, this.toastConfiguration);
            }

         } catch(error) {
              console.error(error);
              Toast.show(error, this.toastConfiguration);
         }
    }
    setStateContact(value:string) {
        this.setState(
            {
                contact: value,
                notes:value,
                show_custom_note: false,
                show_contact_result: true
            }
        
        );
    }

    
   
    closeCustomNotePopup() {
        this.setState({hide_footer: false});
        Keyboard.dismiss();
        this.go("ViewLead", {data: this.state.data, output_key: this.lead_id});
    }
     
    render(): React$Element<*> {
        
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
                </View>
            );
        }

            const {navigation} = this.props;
            
            return <BaseContainer title="Custom Note" {...{ navigation }} hide_footer={this.state.hide_footer} >
                     <Container style={Styles.bgContainer}>
                     <View style={{height:600}} >
                        
                        <View style={style.bubble}>
                            <View >
                                <TouchableHighlight  onPress={() => this.closeCustomNotePopup()}
                                style={{
                                    position: 'absolute',
                                    right:     20,
                                    top:      0,
                                    // bottom:30
                                    }}
                                >
                                <Icon name="close" style={{fontSize: 25}}/>
                                </TouchableHighlight >
                            </View>
                            <Grid style={{paddingTop:30}}>
                                <Row style={this.state.show_contact_result ? style.show : style.hide} size={1}>
                                    <Col style={{height: 100}}>
                                    <Picker
                                        mode="dropdown"
                                        selectedValue={this.state.contact}
                                        onValueChange={(contact) => this.setStateContact(contact)}
                                        >
                                        <Item label="select contact result" value="" />
                                        {this.contact_results.map((item_contact, index) => (
                                            <Item label={item_contact.content} value={item_contact.content} />
                                        ))}
                                    </Picker>
                                    </Col>
                                </Row>
                                <Row size={9}>
                                <Col>
                                    <Item style={style.tagInput}>
                                    <Input
                                        autoCapitalize="none"
                                        returnKeyType="go"
                                        last
                                        inverse
                                        onChangeText={(notes) => this.setState({notes: notes})}
                                        multiline = {true}
                                        value={this.state.notes}
                                        placeholder='Custom Note'
                                        style={{height:119}}
                                        onFocus={() => this.setState({hide_footer: true})} // hide footer
                                    />
                                    </Item>
                                    <View style={style.listItem}>
                                        <Button onPress={() => this.addNote()} style={style.button}>
                                            <Text style={{color:"white"}}>Save</Text>
                                        </Button>
                                    </View>
                                </Col>
                                </Row>
                            </Grid>
                        </View>
                    </View>

                    
                     </Container>
            </BaseContainer>;
        }
}

const style = StyleSheet.create({
    img: {
        ...WindowDimensions
    },
    tagInput:{
        borderColor:"gray",
        marginLeft:10,
        marginRight:10,
    },
    title:{
        color:"#0099FF",
        marginLeft:10,
        fontWeight:"bold"
    },
    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    sizeTitile:{
        flexDirection: 'row',
        marginLeft:10,
    },
    items:{
        marginRight:10,
        fontWeight:"bold",
        color:"black"
    },
    button: {
        // padding:0,
        marginTop:20,
        marginRight: 10,
        marginBottom: 10,
        backgroundColor: "#6AA6D6",
        marginLeft:10,

        
    },
        group: {
            width:width,
            
            justifyContent: "space-between",
            marginBottom: 10,
            
        },
    text: {
        // fontFamily: variables.titleFontfamily,
        color: "white"
    },
    label: {
        justifyContent: 'center', 
        alignItems: 'flex-start',
        paddingLeft: 10
    },
    button_text: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    hide: {height: 0, opacity: 0, width: 0},
    bubble: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.7)',
        // paddingHorizontal: 18,
        // paddingVertical: 12,
        // borderRadius: 20,
        position: 'relative'
      },
      customView: {
        
        // alignSelf:'center',
        marginLeft:5,
        marginBottom:20
      },

});