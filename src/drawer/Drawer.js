// @flow
import autobind from "autobind-decorator";
import React, {Component} from "react";
import {View, StyleSheet, ImageBackground, Image,TouchableOpacity, Text,AsyncStorage,ScrollView, PermissionsAndroid} from "react-native";
import {Button, Container, H1} from "native-base";

import {Global, Images, NavigationHelpers, WindowDimensions} from "../components";
import { Col, Row, Grid } from 'react-native-easy-grid';
import variables from "../../native-base-theme/variables/commonColor";
import Icon from 'react-native-vector-icons/FontAwesome';
import { Appointment } from "../appointment";
// import Contacts from 'react-native-contacts';
// const myIcon = ()

export default class Drawer extends Component {
    constructor(props) {
        super(props);
        
     }

    go(key: string) {
        this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    // @autobind
    // login() {
    //     NavigationHelpers.reset(this.props.navigation, "Login");
    // }
    logout() {
            AsyncStorage.multiRemove(['user_data'], () => {
                     NavigationHelpers.reset(this.props.navigation, "Login");
                 });

    }
    async askPermissionContacts() {
        // get permission
        const read_permision_allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_CONTACTS);
        const write_permision_allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS);
        
        if(read_permision_allowed && write_permision_allowed) {
            this.addContacts();
        } else {
            const granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_CONTACTS, PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS]);
            if (granted['android.permission.READ_CONTACTS'] === PermissionsAndroid.RESULTS.GRANTED && granted['android.permission.WRITE_CONTACTS'] === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can read/write contacts");
                this.addContacts();
              } else {
                console.log("Read/write permission denied");
              }
        }
    }
    async addContacts() {
        // get list lead
        const user_data = JSON.parse(await AsyncStorage.getItem('user_data'));
        let lead_url = Global.API_URL + "contacts/";
        lead_url += user_data.uid + "/";
        lead_url += user_data.token;
        console.log(lead_url);
        return fetch(lead_url)
            .then((response) => response.json())
            .then((responseJson) => {
                if('1' == responseJson.status) {
                    for(const contact of responseJson.data) {
                        const newPerson = {
                            emailAddresses: [{
                              label: "work",
                              email: contact.email_address,
                            }],
                            familyName: contact.last_name,
                            givenName: contact.first_name,
                        }
                        
                        Contacts.addContact(newPerson, (err) => {
                        if (err) throw err;
                        // save successful
                        })
                    }
                }
            })
        .catch((error) => {
            console.error(error);
        });
    }
    render(): React$Element<*> {
        console.log(this.props.navigation);
        const navState = this.props.navigation.state;
        const currentIndex = navState.index;
        const items = navState.routes
            .filter(route => ["Home", "Settings", "Create"].indexOf(route.key) === -1)
            .map((route, i) =>{
                return (
                    <DrawerItem key={i} onPress={() => this.go(route.key)} label={route.key} active={true} />
                );
            });
            

        return <ImageBackground source={Images.drawer} style={style.img}>
            <Container style={StyleSheet.flatten(style.container)}>
                {/* <View style={style.row}>
                    <Button transparent onPress={() => this.go("DrawerClose")}>
                        <Icon name="times" style={StyleSheet.flatten(style.closeIcon)} />
                    </Button>
                </View> */}
                <ScrollView>
                <View style={style.drawerItemsContainer}>
                    <View style={style.drawerItems}>
                        {items}
                        <DrawerItem key={-2} onPress={() => this.go1('Lead', {is_focus_search: true})} icon="search" label="Search Leads"  active={true}/>
                        <DrawerItem key={-3} onPress={() => this.askPermissionContacts()} icon="address-card" label="Add Contacts"  active={true}/>
                        <DrawerItem key={-1} onPress={() => this.logout()} icon="power-off" label="Logout" active={true} />
                    </View>
                    
                </View>
                </ScrollView> 
            </Container>
        </ImageBackground>;
    }
}

class DrawerItem extends Component {

    props: {
        label: string,
        onPress: () => void,
        active?: boolean,
        icon: string
    }

    render(): React$Element<Button> {
        const {label, onPress, active, icon} = this.props;
        let icon_name = '';
        
        switch (label) {
            case 'Home':
                icon_name = 'home';
                this.label='Home';
                break; 
            case 'Lead':
                icon_name = 'tasks';
                this.label='List View';
                break;
            case 'Logout':
                icon_name = 'power-off';
                this.label='Sign Out';
                break;
            case 'MapsView':
                 icon_name = 'map-marker';
                 this.label='Map View';
                 break; 
            case 'MyProfile':
                icon_name = 'user';
                break;
            case 'MyCounties':
                icon_name = 'road';
                break;

            case 'MyStatus':
                icon_name = 'codiepie';
                break;
            case 'Calendars':
                icon_name = Images.add_appointment;
                this.label='Calendar';
                break;
            case 'Contact':
                icon_name = 'info-circle';
                break;
            case 'ActivityLog':
                icon_name = Images.emailed;
                this.label='Activity Log';
                break;
            case 'Filter':
                icon_name = 'filter';
                this.label='Filter';
                break;
            case 'Setter':
                icon_name = 'flickr';
                break;
            case 'AddLead':
                icon_name = Images.add_lead;
                this.label = 'Add Lead';
                break;
           default:
                icon_name = icon;
                this.label = label;
                break;
        }
        if(this.label=='Activity Log'){
            return (<Button iconLeft onPress={onPress} transparent backgroundColor="#22262E" >
                        <Image source={icon_name} style={{marginLeft:-10}}/>
                        <H1 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)", fontSize: 20, paddingLeft: 5 }}>{' '+this.label}</H1>
                    </Button>)
        }else if(this.label=='Calendar'){
            return (<Button iconLeft onPress={onPress} transparent backgroundColor="#22262E" >
                <Image source={icon_name} style={{marginLeft:-10}}/>
                <H1 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)", fontSize: 20, paddingLeft: 5 }}>{' '+this.label}</H1>
            </Button>)
        }else if(this.label=='Add Lead'){
            return (<Button iconLeft onPress={onPress} transparent backgroundColor="#22262E" >
                <Image source={icon_name} style={{marginLeft:-10}}/>
                <H1 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)", fontSize: 20, paddingLeft: 5 }}>{' '+this.label}</H1>
            </Button>)
        }
        
        else{
            return (<Icon.Button name={icon_name} onPress={onPress} full transparent backgroundColor="#22262E"  color={ color= active ? "white" : "rgba(255, 255, 255, .5)" }>
            <H1 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)", fontSize: 20, paddingLeft: 5 }}>{this.label}</H1>
        </Icon.Button>);
        }
        
    }
}

class DrawerIcon extends Component {

    props: {
        label: string,
        icon: string,
        onPress: () => void
    }

    render(): React$Element<Button> {
        const {label, icon, onPress} = this.props;
        return <TouchableOpacity style={style.drawerIcon} onPress={onPress}>
            <Icon name={icon} style={{ color: "rgba(255, 255, 255, .5)", padding: variables.contentPadding }} />
            <Text style={{ color: "white", fontSize: 12 }}>{label.toUpperCase()}</Text>
        </TouchableOpacity>;
    }
}

const style = StyleSheet.create({
    img: {
        
        ...WindowDimensions
    },
    container: {
        backgroundColor: "#22262E",
        paddingHorizontal: variables.contentPadding * 1.5,
        paddingVertical: variables.contentPadding * 2.5
    },
    mask: {
        color: "rgba(255, 255, 255, .5)"
    },
    closeIcon: {
        fontSize: 30,
        color: "rgba(255, 255, 255, .5)"
    },
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "stretch"
    },
    drawerItemsContainer: {
        flex: 1,
        justifyContent: "center",
        paddingVertical: variables.contentPadding * 6
    },
    drawerItems: {
        flex: 1,
        
        padding:0, 
        justifyContent: "space-between"
    },
    drawerIcon: {
        justifyContent: "center",
        alignItems: "stretch"
    },
    drawerIcons: {
        width:30,
        flex: 1,
        
        justifyContent: "space-between"
    }
});
