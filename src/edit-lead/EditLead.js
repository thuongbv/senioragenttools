// @flow
import autobind from "autobind-decorator";
import moment from "moment";
import React, {Component} from "react";
import {Text, TextInput, StyleSheet, View,Alert,Platform,ImageBackground,Dimensions,AsyncStorage,ScrollView,KeyboardAvoidingView,ActivityIndicator} from "react-native";
import {H1,Container,Button,Item,Input,Picker} from "native-base";

import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Circle, Styles, Images, WindowDimensions,NavigationHelpers,Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import Toast from 'react-native-root-toast';
import DatePicker from 'react-native-datepicker';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import RNFetchBlob from 'react-native-fetch-blob';

export default class EditLead extends Component {
    
    go(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    
constructor(props) {
    super(props);
    this.state = {
        isLoading: true,
        data: {},
        output_key: 0,
        contact:0,
        notes:'',
        lead_status:'',
        check_copy:'',
        uid:'',
        token:'',
        first_name:'',
        last_name:'',
        age:'',
        spouse_name:'',
        spouse_age:'',
        street_address:'',
        city:'',
        state_code:'',
        zip:'',
        country:'',
        phone1:'',
        phone2:'',
        quick_note:'',
        record_day:'',
        email_address:'',
        add_lead_type:'',
        decline_id:'',
        check_copy:'',
        file:{},
        url:'',
        lead_type:'Direct Mail',
        importfile:'',
        fileName:'',
        fileOld:'',
        chosenDate: new Date(),
        is_offline: false,
        leadIndex: null
  };
 
    this.group = {};
    this.status_cuz=[];
    this.status_default =[];
    this.state_codes =[];
    this.counties=[];
    this.lead_types=["Direct Mail","Telemarket","Other"];
  //   this.contact='';
}

async getDataOffline(data_name) {
    const json_file_path = await AsyncStorage.getItem('json_file_path');
    if(null !== json_file_path) {
        return RNFetchBlob.fs.readFile(json_file_path+data_name+'.json', 'utf8')
        .then((file_content) => {
        // handle the data ..
            return JSON.parse(file_content);
        })
    }
}

async componentWillMount() {
  const {state} = this.props.navigation;
  const data = state.params.data;
  const output_key = state.params.output_key;
  const leadIndex = state.params.leadIndex;
          
  // get is_offline data from async storage
  const is_offline = JSON.parse(await AsyncStorage.getItem('is_offline'));
  this.setState({is_offline: null === is_offline ? this.state.is_offline : is_offline}, async function() {
    console.log('is offline: ', this.state.is_offline);
    if(this.state.is_offline) {
        // offline
        // get status
        const statuses = await this.getDataOffline('statuses');
        console.log(statuses);
        this.status_cuz = statuses.custom;
        this.status_default = statuses.default;

        // get state
        this.state_codes = await this.getDataOffline('states');

        // get county
        this.counties = await this.getDataOffline('counties');

        // get lead data
        this.setState({
            isLoading: false,
            data: data,
            first_name:data.first_name,
            last_name:data.last_name,
            age:data.age,
            spouse_name:data.spouse_name,
            spouse_age:data.spouse_age,
            street_address:data.street_address,
            city:data.city,
            state_code:data.state,
            zip:data.zip,
            country:data.country,
            phone1:data.phone1,
            phone2:data.phone2,
            quick_note:data.quick_note,
            record_day:data.record_day,
            email_address:data.email_address,
            lead_type:data.lead_type,
            add_lead_type:'',
            decline_id:'',
            id:data.id,
            check_copy:'',
            fileOld:data.file,
            url:data.url,
            lead_status:data.lead_status,
            output_key: output_key,
            leadIndex: leadIndex
        });
    } else {
        // online
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));

        let output_list_url = Global.API_URL + "statuses/"+user_dataa.uid + "/"+user_dataa.token + "/";
        let output_list_url_counties = Global.API_URL + "counties/"+user_dataa.uid + "/"+user_dataa.token + "/";
        let output_list_url_state = Global.API_URL + "states/"+user_dataa.uid + "/"+user_dataa.token + "/";
        fetch(output_list_url)
            .then((response) => response.json())
            .then((responseJson) => {
                
                if('1' == responseJson.status) {
                    // get top list successfully
                    this.status_cuz = responseJson.data.custom;
                    this.status_default = responseJson.data.default;
                    fetch(output_list_url_state)
                        .then((response) => response.json())
                        .then((responseJson) => {
                        if('1' == responseJson.status) {
                            // get top list successfully
                            this.state_codes=responseJson.data;
                            fetch(output_list_url_counties)
                                .then((response) => response.json())
                                .then((responseJson) => {
                                
                                    if('1' == responseJson.status) {
                                        // get top list successfully
                                        this.counties = responseJson.data;
                                        
                                        // console.log(this.contact_results);
                                        this.setState({
                                            isLoading: false,
                                            data: data,
                                            first_name:data.first_name,
                                            last_name:data.last_name,
                                            age:data.age,
                                            spouse_name:data.spouse_name,
                                            spouse_age:data.spouse_age,
                                            street_address:data.street_address,
                                            city:data.city,
                                            state_code:data.state,
                                            zip:data.zip,
                                            country:data.country,
                                            phone1:data.phone1,
                                            phone2:data.phone2,
                                            quick_note:data.quick_note,
                                            record_day:data.record_day,
                                            email_address:data.email_address,
                                            lead_type:data.lead_type,
                                            add_lead_type:'',
                                            decline_id:'',
                                            id:data.id,
                                            check_copy:'',
                                            fileOld:data.file,
                                            url:data.url,
                                            lead_status:data.lead_status,
                                            output_key: output_key,
                                            uid:user_dataa.uid,
                                            token:user_dataa.token
                                        //   dataSource: ds.cloneWithRows(responseJson.movies),
                                        }, function() {
                                            if(data=="None"){
                                                let output_list_url_lead = Global.API_URL + "lead/";
                                                output_list_url_lead += user_dataa.uid + "/";
                                                output_list_url_lead += user_dataa.token + "/";
                                                output_list_url_lead += output_key + "/";
                                                console.log(output_list_url_lead);
                                                fetch(output_list_url_lead)
                                                    .then((response) => response.json())
                                                    .then((responseJson) => {
                                                        
                                                        if('1' == responseJson.status) {
                                                            this.setState({
                                                                    isLoading: false,
                                                                    status:responseJson.data.lead_status,
                                                                    data: responseJson.data,
                                                                    first_name:data.first_name,
                                                                    last_name:data.last_name,
                                                                    age:data.age,
                                                                    spouse_name:data.spouse_name,
                                                                    spouse_age:data.spouse_age,
                                                                    street_address:data.street_address,
                                                                    city:data.city,
                                                                    state_code:data.state,
                                                                    zip:data.zip,
                                                                    country:data.country,
                                                                    phone1:data.phone1,
                                                                    phone2:data.phone2,
                                                                    quick_note:data.quick_note,
                                                                    record_day:data.record_day,
                                                                    email_address:data.email_address,
                                                                    lead_type:data.lead_type,
                                                                    add_lead_type:'',
                                                                    decline_id:'',
                                                                    id:data.id,
                                                                    check_copy:'',
                                                                    fileOld:data.file,
                                                                    url:'',
                                                                    lead_status:data.lead_status,
                                                                    output_key: output_key,
                                                                    uid:user_dataa.uid,
                                                                    token:user_dataa.token
                                                            //   dataSource: ds.cloneWithRows(responseJson.movies),
                                                            }, function() {
                                                            
                                                            });
                                                        }
                                                        // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                                                        
                                                    })
                                                    .catch((error) => {
                                                        console.error(error);
                                                });
                                            }
                                        });
                                    }
                                })
                                .catch((error) => {
                                    console.error(error);
                                });
                                }
                            })
                        .catch((error) => {
                            console.error(error);
                        }); 
        
                    }
                // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                    
            })
            .catch((error) => {
                console.error(error);
        });
    }
  });
  
}
    UpdateLead=async () =>{
        if(this.state.first_name.trim()==='') {
            Toast.show('First name not bank', this.toastConfiguration);
            return;
        }

        if(this.state.last_name.trim()==='') {
            Toast.show('Last name not bank', this.toastConfiguration);
            return;
        }
        if(this.state.record_day.trim()==='') {
            Toast.show('date received not bank', this.toastConfiguration);
            return;
        }
        if(this.state.street_address.trim()==='') {
            Toast.show('Stress address not bank', this.toastConfiguration);
            return;
        }
        if(this.state.city.trim()==='') {
            Toast.show('City not bank', this.toastConfiguration);
            return;
        }
        if(this.state.country.trim()==='') {
            Toast.show('County not bank', this.toastConfiguration);
            return;
        }
        if(this.state.zip.trim()==='') {
            Toast.show('Zip not bank', this.toastConfiguration);
            return;
        }
        if(this.state.state_code.trim()==='') {
            Toast.show('State not bank', this.toastConfiguration);
            return;
        }
        if(this.state.lead_status.trim()==='') {
            Toast.show('Lead status not bank', this.toastConfiguration);
            return;
        }

        if(this.state.is_offline) {
            // offline
            let offline_data_change = JSON.parse(await AsyncStorage.getItem('offline_data_change'));
            const lead = {
                id: this.state.id,
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                age: this.state.age,
                spouse_name: this.state.spouse_name,
                spouse_age: this.state.spouse_age,
                street_address: this.state.street_address,
                city: this.state.city,
                country: this.state.country,
                phone1: this.state.phone1,
                phone2: this.state.phone2,
                quick_note: this.state.quick_note,
                record_day: this.state.record_day,
                lead_status: this.state.lead_status,
                state: this.state.state_code,
                zip: this.state.zip,
                add_lead_type: '',
                decline_id: '',
                check_copy: this.state.check_copy,
                lead_type: this.state.lead_type,
                url: this.state.url
            };
            if(undefined === offline_data_change.leads) {
                offline_data_change.leads = [];
            }
            // check if lead is create/update offline => update
            const lead_check_index = offline_data_change.leads.findIndex(n => n.id == this.state.id);
            if(-1 !== lead_check_index) {
                // get created date from old data
                lead.current_datetime = offline_data_change.leads[lead_check_index].current_datetime;
                offline_data_change.leads[lead_check_index] = lead;
            } else {
                offline_data_change.leads.push(lead);
            }
            console.log(offline_data_change);
            const _this = this;
            await AsyncStorage.setItem('offline_data_change', JSON.stringify(offline_data_change), async function() {
                // update file
                const json_file_path = await AsyncStorage.getItem('json_file_path');
                RNFetchBlob.fs.writeFile(json_file_path+'leads/'+_this.state.leadIndex+'-'+_this.state.id+'.json', JSON.stringify(lead), 'utf8')
                .then(()=>{
                    NavigationHelpers.reset(_this.props.navigation, "Lead");
                })
            });
        } else {
            // call api
            const edit_lead = Global.API_URL + "lead/";
            let form_data_edit_lead = new FormData();
            if (Object.keys(this.state.file).length != 0) {
                form_data_edit_lead.append("file",this.state.file);
            }
            form_data_edit_lead.append("uid", this.state.uid);
            form_data_edit_lead.append("token", this.state.token);
            
            form_data_edit_lead.append("first_name", this.state.first_name);
            form_data_edit_lead.append("last_name", this.state.last_name);
            form_data_edit_lead.append("age", this.state.age);
            form_data_edit_lead.append("spouse_name", this.state.spouse_name);
            form_data_edit_lead.append("spouse_age", this.state.spouse_age);
            form_data_edit_lead.append("street_address", this.state.street_address);
            form_data_edit_lead.append("city", this.state.city);
            form_data_edit_lead.append("country", this.state.country);
            form_data_edit_lead.append("phone1", this.state.phone1);
            form_data_edit_lead.append("phone2", this.state.phone2);
            form_data_edit_lead.append("quick_note", this.state.quick_note);
            form_data_edit_lead.append("record_day", this.state.record_day);
            form_data_edit_lead.append("lead_status", this.state.lead_status);
            form_data_edit_lead.append("state", this.state.state_code);
            form_data_edit_lead.append("zip", this.state.zip);
            form_data_edit_lead.append("add_lead_type", "");
            form_data_edit_lead.append("decline_id", "");
            form_data_edit_lead.append("id", this.state.id);
            form_data_edit_lead.append("check_copy", this.state.check_copy);
            form_data_edit_lead.append("lead_type", this.state.lead_type);

            form_data_edit_lead.append("url", this.state.url);       
            try {
                let response = await fetch(edit_lead, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                    body: form_data_edit_lead
                });
                let responseJson = await response.json();
                Toast.show(responseJson.msg, this.toastConfiguration);
                if(1 == responseJson.status) {
                    try {
                        NavigationHelpers.reset(this.props.navigation, "Lead");
                    } catch (error) {
                    // Error saving data
                        console.error(error);
                        Toast.show(error, this.toastConfiguration);
                    }
                }else{
                    Toast.show(responseJson.msg, this.toastConfiguration);
                }

            } catch(error) {
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
        }
    }
  
    onAttachFile(){
        try{
            DocumentPicker.show({
                filetype: [DocumentPickerUtil.allFiles()],
                
            },(error,res) => {
                // Android
                if(res!=null){
                const { uri, type: mimeType, fileName } = res;
                // importfile=JSON.stringify({["name"]:res.fileName,["type"]:res.type,["tmp_name"]:res.uri,["size"]:res.fileSize});
                let importfile={ uri, type: mimeType, name: fileName };
                this.setState({file:importfile,fileName:fileName});
                console.log(importfile);
                console.log(this.state.file);
                console.log(
                   res.uri,
                   res.type, // mime type
                   res.fileName,
                   res.fileSize
                );
            }
                if (Object.keys(this.state.file).length == 0) {
                    // this.setState({file:""});
                    // this.props.navigation.goBack();
                  }
            });
        }catch(err){
            console.log(err);
        }
    }
    
    getChangeStatus(value:string){
        this.setState({lead_status: value});
        if(value=="Customer"){
            Alert.alert(
                'Confirmation',
                'Do you want to copy this lead to Tracker?',
                [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => this.setState({check_copy: 'check_copy'})},
                ],
                { cancelable: false }
            )
        }
    }
    
    render(): React$Element<*> {
        console.log(this.state.fileName);
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        const date = moment(this.state.record_day);
        const {navigation} = this.props;
        
        return <BaseContainer title="Edit Lead" {...{ navigation }}>
            <Container style={{backgroundColor: "#ECF0F3"}}>
            <ScrollView contentContainerStyle={style.content}>
                    <KeyboardAvoidingView behavior= {(Platform.OS === 'ios')? "padding" : null}
                        style={{flex: 1}}>
                    {/* <Item style={style.tagInput}> */}
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="First Name"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(first_name) => this.setState({first_name: first_name})}
                                value={this.state.first_name}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Last Name"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(last_name) => this.setState({last_name: last_name})}
                                value={this.state.last_name}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Age"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(age) => this.setState({age: age})}
                                value={this.state.age}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Spouse Name"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({spouse_name: value})}
                                value={this.state.spouse_name}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Spouse Age"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({spouse_age: value})}
                                value={this.state.spouse_age}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Street Address"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({street_address: value})}
                                value={this.state.street_address}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="City"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({city: value})}
                                value={this.state.city}
                            />
                        </View>
                        <Picker
                                mode="dropdown"
                                selectedValue={this.state.state_code}
                                onValueChange={(state_code) => this.setState({state_code: state_code})}
                                >
                                    <Item label="select state" value="" />
                                    {this.state_codes.map((item) => (
                                        <Item label={item.state_name} value={item.state_code} />
                                    ))}
                        </Picker>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Zip"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({zip: value})}
                                value={this.state.zip}
                            />
                        </View>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.country}
                            onValueChange={(value) => this.setState({country: value})}
                            >
                                <Item label="select county" value="" />
                                {this.counties.map((item, index) => (
                                    <Item label={item.county_name} value={item.county_name} />
                                ))}
                        </Picker>
                        
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Phone 1"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({phone1: value})}
                                value={this.state.phone1}
                            />
                        </View>
                        
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Phone 2"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({phone2: value})}
                                value={this.state.phone2}
                            />
                        </View>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Quick Note - Shows in Book of Business View"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({quick_note: value})}
                                value={this.state.quick_note}
                            />
                        </View>
                        
                        <Text>Date Received</Text>
                        <DatePicker
                                    
                                date={this.state.record_day}
                                mode="date"
                                placeholder="Start Date"
                                format="MM/DD/YYYY"
                                
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                                // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {this.setState({record_day: date})}}
                            />
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Email Address"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({email_address: value})}
                                value={this.state.email_address}
                            />
                        </View>
                        <Text>Lead status</Text>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.lead_status}
                            onValueChange={(lead_status) => this.getChangeStatus(lead_status)}
                            >
                                {this.status_default.map((item, index) => (
                                    <Item label={item.default_name} value={item.default_name} />
                                ))}
                                {this.status_cuz.map((item, index) => (
                                    <Item label={item.name} value={item.name} />
                                ))}
                                 
                        </Picker>
                        <Text>Lead type</Text>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.lead_type}
                            onValueChange={(value) => this.setState({lead_type: value})}
                            >
                                
                                {this.lead_types.map((item, index) => (
                                    <Item label={item} value={item} />
                                ))}
                        </Picker>
                        <View style={style.blur}>
                            <TextInput 
                                placeholder="Url"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({url: value})}
                                value={this.state.url}
                            />
                        </View>
                        {Platform.OS === 'ios'
                        ?
                        <Button full onPress={() => this.onAttachFile()} style={style.button}>
                                <Text style={{color:"white"}}>Upload file</Text>
                        </Button>
                        :<View/>}
                        <Text>{this.state.fileName!='' ? 'File name: '+this.state.fileName :''}</Text>
                        <Button full onPress={() => this.UpdateLead()} style={style.button}>
                                <Text style={{color:"white"}}>Update</Text>
                        </Button>
                        {/* <View style={style.blur}>
                            <TextInput 
                                placeholder="File"
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="go"
                                inverse
                                onChangeText={(value) => this.setState({file: value})}
                                value={this.state.file}
                            />
                        </View> */}
                    {/* </Item> */}
                    
                    </KeyboardAvoidingView>
                    
                </ScrollView>
            </Container>
        </BaseContainer>;
    }
}

const style = StyleSheet.create({
    content: {
        
        backgroundColor:"#ECF0F3",
        
    },
    button: {
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: "#6AA6D6",
//        color: "white",
        // alignSelf:'center'

    },
    blur: {
        backgroundColor: "rgba(255, 255, 255, .2)"
    },
    text: {
        // fontFamily: variables.titleFontfamily,
        color: "white"
    },
    headline: {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18
      },
    sizeLogo:{
        width : 357,
        // justifyContent: 'center',
        height: 70
        
    },
    tagInput:{
        borderColor:"gray",
        height:40
    },
});