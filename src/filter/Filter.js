// @flow
import moment from "moment";
import React, {Component} from "react";
import {Text, TouchableHighlight,Image,Animated, StyleSheet, View,TouchableOpacity,ListView,Dimensions,AsyncStorage,ScrollView,KeyboardAvoidingView,ActivityIndicator} from "react-native";
import {H1,Container,Button,Item,H3,Picker,ListItem,Body} from "native-base";
import { CheckBox } from 'react-native-elements';
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";
import SelectMultiple from 'react-native-select-multiple';
import {BaseContainer, Circle, Styles, Images, WindowDimensions,NavigationHelpers,Global,Field} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import Toast from 'react-native-root-toast';
import MultiSelect from 'react-native-multiple-select';
import DatePicker from 'react-native-datepicker';
import { Col, Row, Grid } from 'react-native-easy-grid';
import {Collapse,CollapseHeader, CollapseBody, AccordionList} from 'accordion-collapse-react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Filter extends Component {
    go(key: string) {
        this.props.navigation.navigate(key);
    }
    constructor() {
        super();
        this.state = {
            isLoading: true,
            keyword:'',
            checked:true,
            selectedItems:[],
            type_view:"view_map",
            status_cuz:[],
            selectedFriendId:[],
            selectedCounty:[],
            status_default:[],
            counties:[],
            selectedLeadType:[],
            startDate:'',
            endDate:'',
            chosenDate: new Date(),
            ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
            collapsed_status:false,
            collapsed_county:false,
            collapsed_date:false,
            collapsed_city:false,
            collapsed_view:false,
            collapsed_lead_type:false,
            show_datepicker: false,
            date_view: 'All Date'
      };
        this.group = {};
        this.cities =[];
        this.status_cuzss=[];
        this.lead_types = [];
        this.lead_views=[{"value":"Map View","key":"view_map"},{"value":"List View","key":"view_lead"}];
      //   this.contact='';
    }
   
    async componentWillMount() {
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
      
        let output_list_url = Global.API_URL + "statuses/"+user_dataa.uid + "/"+user_dataa.token + "/";
        let output_list_url_counties = Global.API_URL + "counties/"+user_dataa.uid + "/"+user_dataa.token + "/";
        let output_list_url_cities = Global.API_URL + "cities/"+user_dataa.uid + "/"+user_dataa.token + "/";
        let output_list_url_lead_type = Global.API_URL + "lead_types/"+user_dataa.uid + "/"+user_dataa.token + "/";

        return fetch(output_list_url_lead_type)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                if('1' == responseJson.status) {
                    this.lead_types = responseJson.data;
                    
                    fetch(output_list_url)
                    .then((response) => response.json())
                    .then((responseJson) => {
                        
                        if('1' == responseJson.status) {
                            // get top list successfully
                            this.setState({
                                status_cuz:responseJson.data.custom,
                                status_default:responseJson.data.default
                            });
                        
                            //
                            // this.status_cuzss=this.status_cuz.concat(this.status_default)
                            fetch(output_list_url_cities)
                                .then((response) => response.json())
                                .then((responseJson) => {
                                if('1' == responseJson.status) {
                                    // get top list successfully
                                    // this.cities=[];
                                
                                    this.cities=responseJson.data;
                                    this.cities.unshift({"city":"All cities"});
                                    console.log(this.cities);
                                    fetch(output_list_url_counties)
                                        .then((response) => response.json())
                                        .then((responseJson) => {
                                        
                                            if('1' == responseJson.status) {                                        
                                                // console.log(this.contact_results);
                                                this.setState({
                                                    isLoading: false,
                                                    uid:user_dataa.uid,
                                                    token:user_dataa.token,
                                                    counties:responseJson.data,
                                                }, function() {
                                                    // get current filters value
                                                    const {state} = this.props.navigation;
                                                    
                                                    if(undefined !== state.params && 'yes'!==state.params.clear_filter) {
                                                    // do nothing
                                                    } else {
                                                        const filter_list_url = Global.API_URL + "filters/"+user_dataa.uid + "/"+user_dataa.token + "/";
                                                        fetch(filter_list_url)
                                                        .then((response) => response.json())
                                                        .then((responseJson) => {
                                                            if('1' == responseJson.status) {                                        
                                                                console.log(responseJson.data);
                                                                this.setState({
                                                                    selectedFriendId: undefined===responseJson.data.lead_status||''===responseJson.data.lead_status ? [] : responseJson.data.lead_status.split("-"),
                                                                    selectedCounty: undefined===responseJson.data.country||''===responseJson.data.country ? [] : responseJson.data.country.split("-"),
                                                                    date_view: undefined===responseJson.data.date_view||''===responseJson.data.date_view ? this.state.date_view : responseJson.data.date_view,
                                                                    startDate: undefined===responseJson.data.record_day||''===responseJson.data.record_day ? '' : responseJson.data.record_day.split(" - ")[0],
                                                                    endDate: undefined===responseJson.data.record_day||''===responseJson.data.record_day ? '' : responseJson.data.record_day.split(" - ")[1],
                                                                    selectedItems: undefined===responseJson.data.city||''===responseJson.data.city ? [] : responseJson.data.city.split("-"),
                                                                    type_view: undefined===responseJson.data.type_view ? this.state.type_view : responseJson.data.type_view,
                                                                    show_datepicker: undefined!==responseJson.data.date_view&&'Select A Range'===responseJson.data.date_view,
                                                                    selectedLeadType: undefined===responseJson.data.lead_type||''===responseJson.data.lead_type ? [] : responseJson.data.lead_type.split("-"),
                                                                });
                                                            }
                                                        })
                                                        .catch((error) => {
                                                            console.error(error);
                                                        });
                                                    }
                                                });
                                            }
                                        })
                                        .catch((error) => {
                                            console.error(error);
                                        });
                                        }
                                    })
                                .catch((error) => {
                                    console.error(error);
                                }); 
            
                            }
                        
                            
                    })
                    .catch((error) => {
                        console.error(error);
                });
                }
            })
            .catch((error) => {
                console.error(error);
            });

        
        
        
      }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
        console.log(selectedItems);
        
    };

    changeLeadType(id) {
        let tmp = this.state.selectedLeadType;
    
        if("All lead type"===id) {
            tmp = [id];
        } else if ( tmp.includes( id ) ) {
          tmp.splice( tmp.indexOf(id), 1 );
        } else {
          tmp.push( id );
        }
    
        this.setState({
            selectedLeadType: tmp
        });
      }
    
    onCheckBoxPress(id) {
        let tmp = this.state.selectedFriendId;
    
        if(""===id) {
            tmp = [id];
        } else if ( tmp.includes( id ) ) {
          tmp.splice( tmp.indexOf(id), 1 );
        } else {
          tmp.push( id );
        }
    
        this.setState({
          selectedFriendId: tmp
        });
      }
    async onCheckBoxCounty(id) {
        let tmp = this.state.selectedCounty;

        if(""===id) {
            tmp = [id];
        } else if ( tmp.includes( id ) ) {
            tmp.splice( tmp.indexOf(id), 1 );
        } else {
            tmp.push( id );
        }

        const _this = this;
        this.setState({
            selectedCounty: tmp
        },async function() {
            // change city
            const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
            let counties = '';
            for (const key in tmp) {
                counties += '\''+tmp[key]+'\''+',';
            }
            const cities_url = Global.API_URL + "cities/";
            let form_data_cities = new FormData();
            form_data_cities.append("uid", user_dataa.uid);
            form_data_cities.append("token", user_dataa.token);
            form_data_cities.append("counties", counties);
            console.log(form_data_cities);

            try {
                
                let response = await fetch(cities_url, {
                    method: 'POST',
                    headers: {
                    },
                    body: form_data_cities
                });
                let responseJson = await response.json();

                if(1 == responseJson.status) {
                    try {
                        // change city data
                        _this.cities=responseJson.data;
                        _this.cities.unshift({"city":"All cities"});

                        // change city selected state
                        
                        // let city_selected = this.state.selectedItems;
                        // let cities = [];
                        // for(const key in responseJson.data) {
                        //     cities.push(responseJson.data[key]);
                        // }
                        // for(const state_city_key in this.state.selectedItems) {
                        //     console.log(this.state.selectedItems[state_city_key]);
                        //     if(!cities.includes(this.state.selectedItems[state_city_key])) {
                        //         city_selected.splice( this.state.selectedItems.indexOf(this.state.selectedItems[state_city_key]), 1 );
                        //         console.log('remove: ',this.state.selectedItems[state_city_key]);
                        //     }
                        // }
                        // console.log(city_selected);
                        // this.setState({selectedItems: city_selected});
                        

                    } catch (error) {
                    // Error saving data
                        console.error(error);
                    }

                }

             } catch(error) {

                  console.error(error);
                  
             }

            
        });
    }
    Filter=async (type_view) =>{
        const isConnected=true
        let index = this.state.selectedItems.indexOf("All cities");
        console.log(index);
        // console.log(12321);
        if (index !== -1) {
            console.log(12321);
            let dem=0;
            if(dem==0){
                this.state.selectedItems[index] = "";
                selectedItems=Object.assign(this.state.selectedItems);
                selectedItems[index]="";
                // selectedItems=selectedItems.push("All cities");
                this.setState({selectedItems});
                dem++;
            }
            
            // console.log(this.state.selectedItems[index]);
        }
        const search = Global.API_URL + "search_leads/";
        let form_data_search = new FormData();
        const lead_statuss=this.state.selectedFriendId;
        let lead_status=lead_statuss.join('|');
        let counties=this.state.selectedCounty.join('|');
        let city=this.state.selectedItems.join('|');
        let lead_types=this.state.selectedLeadType.join('|');
        let record_day='';
        if('All Date'===this.state.date_view) {
            record_day = '01/01/2000|12/31/2050';
        } else if(this.state.startDate !=''||this.state.endDate !=''){
            record_day=this.state.startDate+'|'+this.state.endDate;
        }
        form_data_search.append("uid", this.state.uid);
        form_data_search.append("token", this.state.token);
        form_data_search.append("keyword", this.state.keyword);
        form_data_search.append("lead_status", lead_status);
        form_data_search.append("record_day",record_day);
        form_data_search.append("date_view",this.state.date_view);
        form_data_search.append("country", counties);
        form_data_search.append("city", city);
        form_data_search.append("lead_type", lead_types);
        // form_data_search.append("type_view", this.state.type_view);
        form_data_search.append("type_view", type_view);
        form_data_search.append("page_number",1);
        form_data_search.append("page_count", 100);
        console.log(form_data_search);
        try {
        
            let response = await fetch(search, {
                method: 'POST',
                headers: {
                },
                body: form_data_search
            });
            let responseJson = await response.json();
            

            Toast.show(responseJson.msg, this.toastConfiguration);
            console.log(responseJson);
            if(1 == responseJson.status) {
                //// login successfully
                // saving user data
                try {
                    if(type_view=="view_lead"){
                        this.props.navigation.navigate("Lead",{data: responseJson.data.leads,search:"Search"});
                    }else{
                        this.props.navigation.navigate("MapsView",{data: responseJson.data.addresses,output_key:"Search"});
                    }
                    
                    
                } catch (error) {
                // Error saving data
                    console.error(error);
                    Toast.show(error, this.toastConfiguration);

                }

            }else{
                Toast.show(responseJson.msg, this.toastConfiguration);
            }

        } catch(error) {
    //                     console.log(1234);
            console.error(error);
            Toast.show(error, this.toastConfiguration);
        }
        
    }
    clearFilter(){
        this.props.navigation.navigate("Filter", {'clear_filter': true});
    }
    changeDateOptions(value) {
        // console.log(value);
        let startDate = '';
        let endDate = '';
        switch(value) {
            case 'All Date':
                startDate = '01/01/2000';
                endDate = '12/31/2050';
                // set state
                this.setState({startDate: startDate, endDate: endDate, show_datepicker: false});
                break;
            case 'Today':
                startDate = moment().format('MM/DD/YYYY');
                endDate = moment().format('MM/DD/YYYY');
                // set state
                this.setState({startDate: startDate, endDate: endDate, show_datepicker: false});
                break;
            case 'This Month':
                startDate = moment().startOf('month').format('MM/DD/YYYY');
                endDate = moment().format('MM/DD/YYYY');
                // set state
                this.setState({startDate: startDate, endDate: endDate, show_datepicker: false});
                break;
            case 'This Year':
                startDate = moment().startOf('year').format('MM/DD/YYYY');
                endDate = moment().format('MM/DD/YYYY');
                // set state
                this.setState({startDate: startDate, endDate: endDate, show_datepicker: false});
                break;
            case 'Last Year':
                const last_year = moment().year() - 1;
                startDate = '01/01/'+last_year;
                endDate = '12/31/'+last_year;
                // set state
                this.setState({startDate: startDate, endDate: endDate, show_datepicker: false});
                break;
            default:
                this.setState({show_datepicker: true});
                break;
        }

        this.setState({date_view: value});
        // console.log(this.state.startDate, this.state.endDate);
    }

    getCurrentSelection(state_name, all_selection) {
        const current_selection = this.state[state_name];
        if('date_view' === state_name) {
            return 'Select A Range' === current_selection ? this.state.startDate+' - '+this.state.endDate : current_selection
        }

        if(current_selection.length === 1 && ("" === current_selection[0] || "All lead type" === current_selection[0])) {
            return all_selection;
        }

        // remove '' ('all' selection from current selections)        
        return current_selection.filter(function(item) { 
            return (item !== "") && (item !== "All lead type");
        }).join();

    }

    render(): React$Element<*> {
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
         
        const {navigation} = this.props;
        const { selectedItems } = this.state;
        
        return <BaseContainer title="Filter" {...{ navigation }}>
                <Container style={{backgroundColor: "#fbfbf0"}}>
                
                <ScrollView contentContainerStyle={style.content}>
                        {/* <KeyboardAvoidingView behavior="padding">
                            
                            <View style={style.blur}>
                                <Field 
                                    label="Keyword"
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    inverse
                                    onChangeText={(keyword) => this.setState({keyword: keyword})}
                                    value={this.state.keyword}
                                />
                            </View>
                        </KeyboardAvoidingView > */}

                        {/* <ScrollView style={style_panel.container}> */}
                            <Grid style={{marginTop: 20}}>
                                <Col size={7}><H3 style={style.header_title}>FILTER SELECTIONS</H3></Col>
                                <Col size={3}>
                                    <Button onPress={() => this.clearFilter()} style={style.button}>
                                        <Image source={Images.clear_filter} style={{width: 34}}/>
                                    </Button>
                                </Col>
                            
                            </Grid>

                            <Collapse style={style.collapse_views}
                                isCollapsed={this.state.collapsed_lead_type} 
                                onToggle={(isCollapsed)=>this.setState({collapsed_lead_type:isCollapsed})}
                            >
                                <CollapseHeader style={style.collapse_header}>
                                    <TouchableOpacity onPress={()=>this.setState({collapsed_lead_type:!this.state.collapsed_lead_type})} style={style.collapse_header_link}>
                                        <Text style={style.collapse_header_text}>Lead Type</Text>
                                        {/* <Image source={this.state.collapsed_lead_type ? Images.icon_up : Images.icon_down} style={style.dropdown_button}/> */}
                                        <Text style={style.collapse_header_selection_text}>{this.getCurrentSelection('selectedLeadType', "All lead type")}</Text>
                                    </TouchableOpacity>
                                </CollapseHeader>
                                <CollapseBody>
                                    <CheckBox
                                        title="All lead type"
                                        checked={this.state.selectedLeadType.includes("All lead type") && 1===this.state.selectedLeadType.length ? true : false}
                                        onPress={()=>this.changeLeadType("All lead type")}
                                    />
                                    <ListView
                                        enableEmptySections={true} 
                                        dataSource={this.state.ds.cloneWithRows(this.lead_types)}
                                        renderRow={(lead_type) =>
                                            
                                            <CheckBox
                                                containerStyle={style.styleCheckBox}
                                                title={lead_type.lead_type}
                                                checked={this.state.selectedLeadType.includes(lead_type.lead_type) ? true : false}
                                                onPress={()=>this.changeLeadType(lead_type.lead_type)}
                                            />
                                            
                                            
                                        }
                                    />
                                </CollapseBody>
                            </Collapse>

                            <Collapse style={style.collapse_views}
                                isCollapsed={this.state.collapsed_status} 
                                onToggle={(isCollapsed)=>this.setState({collapsed_status:isCollapsed})}
                                >
                                <CollapseHeader style={style.collapse_header}>
                                    <TouchableOpacity onPress={()=>this.setState({collapsed_status:!this.state.collapsed_status})} style={style.collapse_header_link}>
                                        <Text style={style.collapse_header_text}>Lead Status</Text>
                                        {/* <Image source={this.state.collapsed_status ? Images.icon_up : Images.icon_down} style={style.dropdown_button}/> */}
                                        <Text style={style.collapse_header_selection_text}>{this.getCurrentSelection('selectedFriendId', "All status")}</Text>
                                    </TouchableOpacity>
                                </CollapseHeader>
                                <CollapseBody>
                                    <CheckBox
                                        containerStyle={style.styleCheckBox}
                                        title="All status"
                                        checked={this.state.selectedFriendId.includes("") && 1===this.state.selectedFriendId.length ? true : false}
                                        onPress={()=>this.onCheckBoxPress("")}
                                    />
                                    <ListView
                                        enableEmptySections={true} 
                                        dataSource={this.state.ds.cloneWithRows(this.state.status_default)}
                                        renderRow={(status) =>
                                            
                                            <CheckBox
                                                containerStyle={style.styleCheckBox}
                                                title={status.default_name}
                                                checked={this.state.selectedFriendId.includes(status.default_name) ? true : false}
                                                onPress={()=>this.onCheckBoxPress(status.default_name)}
                                            />
                                            
                                        
                                        }
                                    />
                                    <ListView
                                        enableEmptySections={true} 
                                        dataSource={this.state.ds.cloneWithRows(this.state.status_cuz)}
                                        renderRow={(status) =>
                                            
                                            <CheckBox
                                                containerStyle={style.styleCheckBox}
                                                title={status.name}
                                                checked={this.state.selectedFriendId.includes(status.name) ? true : false}
                                                onPress={()=>this.onCheckBoxPress(status.name)}
                                            />
                                            
                                            
                                        }
                                    />
                                </CollapseBody>
                            </Collapse>

                            <Collapse style={style.collapse_views}
                                isCollapsed={this.state.collapsed_county} 
                                onToggle={(isCollapsed)=>this.setState({collapsed_county:isCollapsed})}
                            >
                                <CollapseHeader style={style.collapse_header}>
                                    <TouchableOpacity onPress={()=>this.setState({collapsed_county:!this.state.collapsed_county})} style={style.collapse_header_link}>
                                        <Text style={style.collapse_header_text}>County</Text>
                                        {/* <Image source={this.state.collapsed_county ? Images.icon_up : Images.icon_down} style={style.dropdown_button}/> */}
                                        <Text style={style.collapse_header_selection_text}>{this.getCurrentSelection('selectedCounty', "All Counties")}</Text>
                                    </TouchableOpacity>
                                </CollapseHeader>
                                <CollapseBody>
                                    <CheckBox
                                        title="All Counties"
                                        checked={this.state.selectedCounty.includes("") && 1===this.state.selectedCounty.length ? true : false}
                                        onPress={()=>this.onCheckBoxCounty("")}
                                    />
                                    <ListView
                                        enableEmptySections={true} 
                                        dataSource={this.state.ds.cloneWithRows(this.state.counties)}
                                        renderRow={(county) =>
                                            
                                            <CheckBox
                                                containerStyle={style.styleCheckBox}
                                                title={county.county_name}
                                                checked={this.state.selectedCounty.includes(county.county_name) ? true : false}
                                                onPress={()=>this.onCheckBoxCounty(county.county_name)}
                                            />
                                            
                                            
                                        }
                                    />
                                </CollapseBody>
                            </Collapse>

                            <Collapse style={style.collapse_views}
                                isCollapsed={this.state.collapsed_date} 
                                onToggle={(isCollapsed)=>this.setState({collapsed_date:isCollapsed})}
                            >
                                <CollapseHeader style={style.collapse_header}>
                                    <TouchableOpacity onPress={()=>this.setState({collapsed_date:!this.state.collapsed_date})} style={style.collapse_header_link}>
                                        <Text style={style.collapse_header_text}>Lead Date</Text>
                                        {/* <Image source={this.state.collapsed_date ? Images.icon_up : Images.icon_down} style={style.dropdown_button}/> */}
                                        <Text style={style.collapse_header_selection_text}>{this.getCurrentSelection('date_view', "")}</Text>
                                    </TouchableOpacity>
                                </CollapseHeader>
                                <CollapseBody>
                                    <Picker
                                        mode="dropdown"
                                        selectedValue={this.state.date_view}
                                        onValueChange={(value) => this.changeDateOptions(value)}
                                        style={{ width: undefined }}
                                        >
                                            <Picker.Item label='All Date' value='All Date' />
                                            <Picker.Item label='Today' value='Today' />
                                            <Picker.Item label='This Month' value='This Month' />
                                            <Picker.Item label='This Year' value='This Year' />
                                            <Picker.Item label='Last Year' value='Last Year' />
                                            <Picker.Item label='Select A Range' value='Select A Range' />
                                        </Picker>
                                    <Grid style={this.state.show_datepicker ? styles.show : styles.hide}>
                                        
                                        <Col>
                                            <DatePicker
                                                
                                                date={this.state.startDate}
                                                mode="date"
                                                placeholder="Start Date"
                                                format="MM/DD/YYYY"
                                                
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 4,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 36,
                                                    
                                                }
                                                // ... You can check the source to find the other keys.
                                                }}
                                                onDateChange={(date) => {this.setState({startDate: date})}}
                                            />
                                        </Col>
                                        <Col>
                                        <DatePicker
                                                date={this.state.endDate}
                                                mode="date"
                                                placeholder="End Date"
                                                format="MM/DD/YYYY"
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                customStyles={{
                                                    
                                                    dateInput: {
                                                        
                                                        marginLeft: 36,
                                                    }
                                                    // ... You can check the source to find the other keys.
                                                }}
                                                showIcon={false}
                                                onDateChange={(date) => {this.setState({endDate: date})}}
                                            />    
                                        </Col>
                                        
                                    </Grid>
                                </CollapseBody>
                            </Collapse>

                            <Collapse style={style.collapse_views}
                                isCollapsed={this.state.collapsed_city} 
                                onToggle={(isCollapsed)=>this.setState({collapsed_city:isCollapsed})}
                            >
                                <CollapseHeader style={style.collapse_header}>
                                    <TouchableOpacity onPress={()=>this.setState({collapsed_city:!this.state.collapsed_city})} style={style.collapse_header_link}>
                                        <Text style={style.collapse_header_text}>City</Text>
                                        {/* <Image source={this.state.collapsed_city ? Images.icon_up : Images.icon_down} style={style.dropdown_button}/> */}
                                        <Text style={style.collapse_header_selection_text}>{this.getCurrentSelection('selectedItems', "")}</Text>
                                    </TouchableOpacity>
                                </CollapseHeader>
                                <CollapseBody>
                                    <ScrollView style={{ flex: 1,marginTop:15 }}>
                                        <MultiSelect
                                            items={this.cities}
                                            uniqueKey="city"
                                            ref={(component) => { this.multiSelect = component }}
                                            onSelectedItemsChange={this.onSelectedItemsChange}
                                            selectedItems={selectedItems}
                                            selectText="Select city"
                                            onChangeInput={ (text)=> console.log(text)}
                                            
                                            tagRemoveIconColor="#CCC"
                                            tagBorderColor="#CCC"
                                            tagTextColor="#CCC"
                                            selectedItemTextColor="#CCC"
                                            selectedItemIconColor="#CCC"
                                            itemTextColor="#000"
                                            displayKey="city"
                                            searchInputStyle={{ color: '#CCC' }}
                                            submitButtonColor="#CCC"
                                            submitButtonText="Submit"
                                        />
                                    </ScrollView>
                                </CollapseBody>
                            </Collapse>

                            {/* <Collapse style={style.collapse_views}
                                isCollapsed={this.state.collapsed_view} 
                                onToggle={(isCollapsed)=>this.setState({collapsed_view:isCollapsed})}
                            >
                                <CollapseHeader style={style.collapse_header}>
                                    <TouchableOpacity onPress={()=>this.setState({collapsed_view:!this.state.collapsed_view})}>
                                        <Text style={style.collapse_header_text}>Choose to see filter results</Text>
                                    </TouchableOpacity>
                                </CollapseHeader>
                                <CollapseBody>
                                    <Picker
                                        mode="dropdown"
                                        selectedValue={this.state.type_view}
                                        onValueChange={(value) => this.setState({type_view: value})}
                                        >
                                            
                                            {this.lead_views.map((item, index) => (
                                                <Item label={item.value} value={item.key} />
                                            ))}
                                    </Picker>
                                </CollapseBody>
                            </Collapse> */}

                            {/* <View style={Styles.listItem}>
                                <Button full onPress={() => this.Filter()} style={style.button}>
                                    <Text style={{color:"white"}}>Search</Text>
                                </Button>
                                <Button onPress={() => this.clearFilter()} style={style.button}>
                                    <Image source={Images.clear_filter}/>
                                </Button>
                            </View> */}
                            <Row style={{paddingLeft:20,paddingRight:30,marginTop:20,marginBottom: 5}}>
                                {/* <Col>
                                    <Button onPress={() => this.clearFilter()} style={style.button}>
                                        <Image source={Images.clear_filter} style={{width: 30}}/>
                                    </Button>
                                </Col> */}
                                <Col>
                                    <Button full onPress={() => this.Filter('view_lead')} style={style.button}>
                                        {/* <Icon name='list' color="white" size={26}/> */}
                                        <Image source={Images.Filter} style={{width: 34}}/>
                                        <Text style={style.button_text}>LIST</Text>
                                    </Button>
                                </Col>
                                <Col>
                                    <Button full onPress={() => this.Filter('view_map')} style={style.button}>
                                        {/* <Image source={Images.icon_map} /> */}
                                        <Image source={Images.Filter} style={{width: 30}}/>
                                        <Text style={style.button_text}>MAP</Text>
                                    </Button>
                                </Col>
                            </Row>
                            {/* <Row>
                                <Col size={1}/>
                                <Col size={2}><Text style={{textAlign: 'center',color:'#060606'}}>VIEW FILTER RESULTS</Text></Col>
                            </Row> */}
                        {/* </ScrollView> */}
                    </ScrollView>
                </Container>
        </BaseContainer>;
    }
}

const style_panel = StyleSheet.create({
    container: {
        flex            : 1,
        // backgroundColor : '#f4f7f9',
        paddingTop      : 30
      },
  });

const style = StyleSheet.create({
    button: {
        
        backgroundColor: "#12324b", // #6AA6D6
        marginLeft:10,
        borderRadius: 8
    },
    text: {
        fontFamily: variables.titleFontfamily,
        color: "white"
    },
    headline: {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18
      },
      styleCheckBox:{
        padding:3,
        marginBottom:1
      },
    sizeLogo:{
        width : 357,
        // justifyContent: 'center',
        height: 70
        
    },
    collapse_views: {marginTop: 5, padding: 10},
    header_title: {
        color: '#363c43', // '#14574b',
        textAlign: 'center', fontWeight: 'bold', paddingLeft: 30},
    collapse_header_text: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#ebf5f7'
    },
    collapse_header_selection_text: {
        fontSize: 12,
        color: '#ebf5f7'
    },
    collapse_header: {
        padding: 8,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#749ebe',
        borderRadius: 8
    },
    collapse_header_link: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    button_text: {
        fontWeight: "bold",
        marginLeft: 5,
        color: "white",
        fontSize: 18
    },
    dropdown_button: {
        width: 20,
        height: 20
    }
});

class Panel extends Component{
    constructor(props){
        super(props);

        this.icons = {
            'up'    : Images.icon_up,
            'down'  : Images.icon_down
        };

        this.state = {
            title       : props.title,
            expanded    : true,
            animation   : new Animated.Value()
        };
        console.log(this.state.animation);
    }

    toggle(){
        
        const _this = this;
        
        let initialValue    = _this.state.expanded? _this.state.maxHeight + _this.state.minHeight : _this.state.minHeight,
            finalValue      = _this.state.expanded? _this.state.minHeight : _this.state.maxHeight + _this.state.minHeight;

        _this.setState({
            expanded : !_this.state.expanded
        });

        console.log(_this.state);

        _this.state.animation.setValue(initialValue);
        Animated.spring(
            _this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
        
       
    }

    _setMaxHeight(event){
        if(!this.state.maxHeight || (this.state.maxHeight && event.nativeEvent.layout.height > this.state.maxHeight)) {
            this.setState({
                maxHeight   : event.nativeEvent.layout.height,
            });
        }
        console.log('max height',event.nativeEvent.layout.height);
    }

    _setMinHeight(event){
        this.setState({
            minHeight   : event.nativeEvent.layout.height
        });
        console.log('min height',event.nativeEvent.layout.height);
    }

    render(){
        let icon = this.icons['down'];

        if(this.state.expanded){
            icon = this.icons['up'];
        }

        return (
            <Animated.View 
                style={[styles.container,{height: this.state.animation}]}>
                <View style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>
                    <Text style={styles.title}>{this.state.title}</Text>
                    <TouchableHighlight 
                        style={styles.button} 
                        onPress={this.toggle.bind(this)}
                        underlayColor="#f1f1f1">
                        <Image
                            style={styles.buttonImage}
                            source={icon}
                        ></Image>
                    </TouchableHighlight>
                </View>
                
                <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                    {this.props.children}
                </View>

            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container   : {
        backgroundColor: '#fff',
        margin:10,
        overflow:'hidden'
    },
    titleContainer : {
        flexDirection: 'row'
    },
    title       : {
        flex    : 1,
        padding : 10,
        color   :'#2a2f43',
        fontWeight:'bold'
    },
    button      : {

    },
    buttonImage : {
        width   : 30,
        height  : 25
    },
    body        : {
        padding     : 10,
        paddingTop  : 0
    },
    hide: {height: 0, opacity: 0}
});
