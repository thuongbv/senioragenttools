import moment from "moment";
import React, {Component} from "react";
import {Text, Image, StyleSheet,Platform,View,Linking,PermissionsAndroid,AsyncStorage,ActivityIndicator,Dimensions,TouchableOpacity,
    TouchableHighlight, Keyboard, Alert} from "react-native";
import {H1,Container,Button,Item,Input,Icon,Right} from "native-base";
import Toast from 'react-native-root-toast';
import { ProviderPropType, Marker, AnimatedRegion } from 'react-native-maps';
import MapView from 'react-native-map-clustering';
import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global } from "../components";
import Permissions from 'react-native-permissions';
//import img from "img";

import variables from "../../native-base-theme/variables/commonColor";
import { Grid, Col } from "react-native-easy-grid";
const {height, width} = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
export default class MapsView extends Component {

    constructor(props) {
        super(props);
        this.state = {
             isLoading: true,
             keyword:'',
             search:'',
             region: {
                latitude: 1,
                longitude: 1,
                latitudeDelta: LATITUDE_DELTA, // hardcode zoom levels just for example
                longitudeDelta: LONGITUDE_DELTA,
              },
              markers:[],
              statusBarHeight:1,
              cuzname:'',
            cuzstatus:'',
            cuzid:0,
            cuzheight:0,
            cuzlat:0,
            cuzlng:0,
            current_original_lat:0,
            current_original_lng:0,
            cuzcolor:'',
            cuzphone1:0,
            cuzphone2:0,
            current_address_map: '',
            flex: 0,
            map_margin_top: 1,
            isMapReady: false,
            markers: []
        };

        // this.markers = [];
        // this.color ='';
        this.toast_configuration = {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
        };
        this.init_region = {
            latitude: 1,
            longitude: 1,
            latitudeDelta: LATITUDE_DELTA, // hardcode zoom levels just for example
            longitudeDelta: LONGITUDE_DELTA,
          };
          this.method = 'post';
     }

     onMapLayout = () => {
       setTimeout(() => this.setState({ isMapReady: true, map_margin_top: 1===this.state.map_margin_top ? 0 : 1 }, function() {console.log(this.state.map_margin_top)}));
       console.log('map ready');

      }

    go(key: string) {
        this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        // hide popup before change view
        this.setState({cuzheight: 0});
        this.props.navigation.navigate(key, detail_data);

    }
    async componentDidMount() {
        // get parameter from Filter component
        const {state} = this.props.navigation;

        if(state.params!==undefined&&state.params.output_key === "Search"){
            console.log(state.params.data);
            // this.markers  = state.params.data;

            this.setState({
                markers: state.params.data,
                isLoading: this.state.region.latitude == this.init_region.latitude && this.state.region.longitude == this.init_region.longitude,
                markers :state.params.data,
                flex: 1,
                // region: {
                //     // latitude: this.markers[0].address.latitude,
                //     // longitude: this.markers[0].address.longitude,
                //     latitude: state.params.data.length > 0 ? this.markers[0].address.latitude : this.markers[0].start_lat,
                //     longitude: state.params.data.length > 0 ? this.markers[0].address.longitude : this.markers[0].start_long,
                //     latitudeDelta: LATITUDE_DELTA, // hardcode zoom levels just for example
                //     longitudeDelta: LONGITUDE_DELTA,
                // }

            },function(){
                if(!this.state.isLoading) {
                    setTimeout(()=>this.setState({statusBarHeight: 0}),500);
                }

                console.log(this.state.isLoading);
            });
        }

        // get current coordinate
        // const geolocation_permision_allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
        // console.log(geolocation_permision_allowed);
        // if(geolocation_permision_allowed) {
        //     this._findMe();
        // } else {
        //     const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
        //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //         console.log("You can use user location");
        //         this._findMe();
        //       } else {
        //         console.log("Location permission denied")
        //       }
        // }

        Permissions.check('location').then(response => {
            console.log(response);
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            if('authorized' === response) {
                console.log("You can use user location");
                this._findMe();
            } else {
                Alert.alert(
                    'Can we access your location?',
                    'We need access location while using app',
                    [
                      {
                        text: 'No',
                        onPress: () => console.log('Permission denied'),
                        style: 'cancel',
                      },
                    //   response == 'undetermined'
                    //     ? { text: 'OK', onPress: this._requestPermission }
                    //     : { text: 'Open Settings', onPress: Permissions.openSettings },
                    { text: 'OK', onPress: this._requestPermission }
                    ]
                  )
            }
        });

    }

    _requestPermission = () => {
        Permissions.request('location').then(response => {
          // Returns once the user has chosen to 'allow' or to 'not allow' access
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          if ('authorized' === response) {
            console.log("You can use user location");
            this._findMe();
            } else {
            console.log("Location permission "+response)
            }
        })
      }

    searchKey=async () =>{
        const isConnected=true
        const search = Global.API_URL + "search_leads/";
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let form_data_search = new FormData();

        form_data_search.append("uid", user_dataa.uid);
        form_data_search.append("token", user_dataa.token);
        form_data_search.append("keyword", this.state.keyword);
        form_data_search.append("lead_status","");
        form_data_search.append("record_day","");
        form_data_search.append("country", "");
        // form_data_search.append("city", this.state.city);
        form_data_search.append("type_view", "view_map");
        form_data_search.append("page_number",1);
        form_data_search.append("page_count", 100);

        try {

            let response = await fetch(search, {
                method: 'POST',
                headers: {
                },
                body: form_data_search
            });
            let responseJson = await response.json();


            Toast.show(responseJson.msg, this.toastConfiguration);
    //                    NavigationHelpers.reset(this.props.navigation, "Home");
            if(1 == responseJson.status) {
                //// login successfully
                // saving user data
                try {
                    // this.markers =responseJson.data.addresses;

                    this.setState({
                        markers: responseJson.data.addresses,
                        isLoading: false,
                        flex: 1,
                        markers :responseJson.data.addresses,
                        region: {
                            latitude: responseJson.data.addresses.length > 0 ? this.state.markers[0].address.latitude : responseJson.data.start_lat,
                            longitude: responseJson.data.addresses.length > 0 ? this.state.markers[0].address.longitude : responseJson.data.start_long,
                            latitudeDelta: LATITUDE_DELTA, // hardcode zoom levels just for example
                            longitudeDelta: LONGITUDE_DELTA,
                          }
                    }, function() {
                        Keyboard.dismiss();
                        // setTimeout(()=>this.setState({map_margin_top: 1===this.state.map_margin_top ? 0 : 1}),500);
                    });

                } catch (error) {
                // Error saving data
                    console.error(error);
                    Toast.show(error, this.toastConfiguration);

                }

            }else{
                Toast.show(responseJson.msg, this.toastConfiguration);
            }

        } catch(error) {
    //                     console.log(1234);
            console.error(error);
            Toast.show(error, this.toastConfiguration);
        }
    }

    async componentWillMount() {
        const {state} = this.props.navigation;
        if(state.params==undefined){
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let output_list_url = Global.API_URL + "map_leads/";
        if('post' === this.method) {
            let form_data = new FormData();
            form_data.append("uid", user_dataa.uid);
            form_data.append("token", user_dataa.token);

            try {
                let response = await fetch(output_list_url, {
                    method: 'POST',
                    headers: {
                    },
                    body: form_data
                });
                let responseJson = await response.json();
                if('1' == responseJson.status) {
                    console.log(responseJson.data.addresses);

                    // this.markers =responseJson.data.addresses;
                    this.setState({
                        markers :responseJson.data.addresses,
                        //// map not center marker
                        isLoading: this.state.region.latitude == this.init_region.latitude && this.state.region.longitude == this.init_region.longitude, // this.state.region === this.init_region,
                        flex: 1,
                        // region: {
                        //     latitude: responseJson.data.addresses.length > 0 ? this.markers[0].address.latitude : responseJson.data.start_lat,
                        //     longitude: responseJson.data.addresses.length > 0 ? this.markers[0].address.longitude : responseJson.data.start_long,
                        //     latitudeDelta: LATITUDE_DELTA, // hardcode zoom levels just for example
                        //     longitudeDelta: LONGITUDE_DELTA,
                        //   }
                    }, function() {
                        if(!this.state.isLoading) {
                            // setTimeout(()=>this.setState({map_margin_top: 1===this.state.map_margin_top ? 0 : 1}),500);
                        }

                        console.log('will mount');
                    });

                }

             } catch(error) {

                  Toast.show(error, this.toastConfiguration);
             }
        } else {
            output_list_url += user_dataa.uid + "/";
            output_list_url += user_dataa.token + "/";
    
            return fetch(output_list_url)
                .then((response) => response.json())
                .then((responseJson) => {
                    //navigator.geolocation.clearWatch(this.watchID);
    
                    if('1' == responseJson.status) {
                        console.log(responseJson.data.addresses);
    
                        // this.markers =responseJson.data.addresses;
                        this.setState({
                            markers :responseJson.data.addresses,
                            //// map not center marker
                            isLoading: this.state.region.latitude == this.init_region.latitude && this.state.region.longitude == this.init_region.longitude, // this.state.region === this.init_region,
                            flex: 1,
                            // region: {
                            //     latitude: responseJson.data.addresses.length > 0 ? this.markers[0].address.latitude : responseJson.data.start_lat,
                            //     longitude: responseJson.data.addresses.length > 0 ? this.markers[0].address.longitude : responseJson.data.start_long,
                            //     latitudeDelta: LATITUDE_DELTA, // hardcode zoom levels just for example
                            //     longitudeDelta: LONGITUDE_DELTA,
                            //   }
                        }, function() {
                            if(!this.state.isLoading) {
                                // setTimeout(()=>this.setState({map_margin_top: 1===this.state.map_margin_top ? 0 : 1}),500);
                            }
    
                            console.log('will mount');
                        });
    
                    }
    
                })
                .catch((error) => {
                console.error(error);
                });
        }
        
        }
    }
     changeColor(color) {
        switch (color) {
            case 'Blue.png':
                return  require('../components/img/Blue.png');
                break;
            case 'Black.png':
                return  require('../components/img/Black.png');
                break;
            case 'BurlyWood.png':
                return  require('../components/img/BurlyWood.png');
                break;
            case 'DeepSkyBlue.png':
                return  require('../components/img/DeepSkyBlue.png');
                break;
            case 'FireBrick.png':
                return require('../components/img/FireBrick.png');
                break;
            case 'Fuschia.png':
                return  require('../components/img/Fuschia.png');
                break;
            case 'Gold.png':
                return  require('../components/img/Gold.png');
                break;
            case 'Green.png':
                return require('../components/img/Green.png');
                break;
            case 'Grey.png':
                return require('../components/img/Grey.png');
                break;
            case 'Lime.png':
                return require('../components/img/Lime.png');
                break;
            case 'Purple.png':
                return require('../components/img/Purple.png');
                break;
            case 'Red.png':
                return  require('../components/img/Red.png');
                break;
            case 'Salmon.png':
                return  require('../components/img/Salmon.png');
                break;
            case 'Sienna.png':
                return  require('../components/img/Sienna.png');
                break;
            case 'SlateBlue.png':
                return require('../components/img/SlateBlue.png');
                break;
            case 'Teal.png':
                return require('../components/img/SlateBlue.png');
                break;
            default :
                return "";
                break;
        }
    }

    cuzCallout(name,status,id,lat,lng,color,phone1,phone2,original_lat, original_lng, address_map){
        Keyboard.dismiss();
        this.setState({
            cuzheight:width/1.5,
            cuzname:name,
            cuzstatus:status,
            cuzid:id,
            cuzlat:lat,
            cuzlng:lng,
            current_original_lat:original_lat,
            current_original_lng:original_lng,
            cuzcolor:color,
            cuzphone1:phone1,
            cuzphone2:phone2,
            current_address_map: address_map
        })
    }
    onPressCall(phoneNumber,value) {
        let url;
        let prompt=true;
		if(Platform.OS !== 'android') {
			url = prompt ? 'telprompt:' : 'tel:';
		}
		else {
			url = 'tel:';
		}

        url += phoneNumber;

        Linking.canOpenURL(url).then(supported => {
            if(!supported) {
                console.log('Can\'t handle url: ' + url);
            } else {
                Linking.openURL(url)
                .catch(err => {
                    if(url.includes('telprompt')) {
                        // telprompt was cancelled and Linking openURL method sees this as an error
                        // it is not a true error so ignore it to prevent apps crashing
                        // see https://github.com/anarchicknight/react-native-communications/issues/39
                    } else {
                        console.warn('openURL error', err)
                    }
                });
            }
        }).catch(err => console.warn('An unexpected error happened', err));
    }
    directionMapOld(){
        url="http://maps.google.com/maps?q="+this.state.current_original_lat+','+this.state.current_original_lng;

            Linking.canOpenURL(url).then(supported => {
                if(!supported) {
                    console.log('Can\'t handle url: ' + url);
                } else {
                    Linking.openURL(url)
                    .catch(err => {
                        if(url.includes('telprompt')) {
                            // telprompt was cancelled and Linking openURL method sees this as an error
                            // it is not a true error so ignore it to prevent apps crashing
                            // see https://github.com/anarchicknight/react-native-communications/issues/39
                        } else {
                            console.warn('openURL error', err)
                        }
                    });
                }
            }).catch(err => console.warn('An unexpected error happened', err));
    }

    directionMap() {
        const lat = this.state.current_original_lat;
        const lng = this.state.current_original_lng;
        const address = this.state.current_address_map;

        const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
        const latLng = `${lat},${lng}`;
        const label = 'Google Map';
        const url = Platform.select({
            // ios: `${scheme}${label}@${latLng}`,
            ios: "http://maps.google.com/maps?q="+address,
            android: `${scheme}${address}(${label})`
        });

        Linking.canOpenURL(url).then(supported => {
            if(!supported) {
                console.log('Can\'t handle url: ' + url);
                const browser_url="http://maps.google.com/maps?q="+lat+","+lng;
                Linking.openURL(browser_url);
            } else {
                Linking.openURL(url)
                .catch(err => {
                    if(url.includes('telprompt')) {
                        // telprompt was cancelled and Linking openURL method sees this as an error
                        // it is not a true error so ignore it to prevent apps crashing
                        // see https://github.com/anarchicknight/react-native-communications/issues/39
                    } else {
                        console.warn('openURL error', err)
                    }
                });
            }
        }).catch(err => console.warn('An unexpected error happened', err));
    }

    async _findMe(){
      const current_coordinate = JSON.parse(await AsyncStorage.getItem('current_coordinate'));
      if(null !== current_coordinate) {
        const region = {
            latitude:current_coordinate.latitude,
            longitude:current_coordinate.longitude,
            latitudeDelta: 0.005,
            longitudeDelta: 0.001,
        };
        this.setState({region: region, isLoading: current_coordinate.latitude == this.init_region.latitude && current_coordinate.longitude == this.init_region.longitude}, function() {
console.log(current_coordinate);
        });
      }


        navigator.geolocation.watchPosition(
          ({coords}) => {
            // if(Platform.OS !== 'android') {
            //     navigator.geolocation.getCurrentPosition(function () {}, function () {}, {});
            //   }
            const {latitude, longitude} = coords;
            console.log(coords);
            const region = {
                latitude:latitude,
                longitude:longitude,
                latitudeDelta: 0.005,
                longitudeDelta: 0.001,
            };
            this.setState({
            //   position: {
            //     latitude,
            //     longitude,
            //   },
              region: region,

            }, async function() {

                // save current coordinate to async storage
                await AsyncStorage.setItem('current_coordinate', JSON.stringify(region));
                console.log('save current coordinate',this.state.region, this.state.isLoading);

                // go to current coordinate
                //if(this.state.isMapReady) {
                  //mapView._root.animateToRegion(region);
                //}


            })
          },
        //   (error) => alert(JSON.stringify(error)),
        //   (error) => alert('Could not detect your current location. Please recheck your phone setting.'),
          (error) => this.getCurrentPositionFromAsyncStorage(),
           //{
            // enableHighAccuracy: false, timeout: 20000
       //maximumAge: 0, timeOut: 10000
         //}
        )
      }

      clickFindMe() {
        if(this.state.isMapReady) {
          const coordinate = {latitude: this.state.region.latitude, longitude: this.state.region.longitude};
          mapView._root.animateToRegion(coordinate);
          console.log(coordinate, this.state.isMapReady);
        }

      }

    async getCurrentPositionFromAsyncStorage() {
      console.log('get coordinate from AsyncStorage');
        // get current from async storage
        const current_coordinate = JSON.parse(await AsyncStorage.getItem('current_coordinate'));
        if(null !== current_coordinate) {
            this.setState({
                  region: {
                    latitude:current_coordinate.latitude,
                    longitude:current_coordinate.longitude,
                    latitudeDelta: 0.005,
                    longitudeDelta: 0.001,
                  },

                }, function() {
                    this.setState({isLoading: this.state.region.latitude == this.init_region.latitude && this.state.region.longitude == this.init_region.longitude});
                })

        } else {
            alert('Could not detect your current location. Please recheck your phone setting.')
        }
      }

      animate(coordinate){
        console.log(coordinate);

        // if(undefined === mapView.props) {
        //     const newRegion = {
        //          latitude: mapView.state.currentRegion.latitude,
        //          longitude: mapView.state.currentRegion.longitude,
        //          latitudeDelta: mapView.state.currentRegion.latitudeDelta * 0.1,
        //          longitudeDelta: mapView.state.currentRegion.longitudeDelta * 0.1,
        //      };
        //      mapView.root.animateToRegion(newRegion, 1000);
        // } else {
        //     const newRegion = {
        //          latitude: mapView.props.region.latitude,
        //          longitude: mapView.props.region.longitude,
        //          latitudeDelta: mapView.props.region.latitudeDelta * 0.1 ,
        //          longitudeDelta: mapView.props.region.longitudeDelta * 0.1,
        //      };
        //      mapView.root.animateToRegion(newRegion, 1000);
        // }

        let newRegion = {
            latitude: coordinate.latitude,
            longitude: coordinate.longitude,
            latitudeDelta: mapView.state.region.latitudeDelta * 0.1,
            longitudeDelta: mapView.state.region.longitudeDelta * 0.1,
        };
        mapView._root.animateToRegion(newRegion, 1000);


     }

     closePopup() {
         // hide popup before change view
         console.log("close popup from map");
        this.setState({cuzheight: 0});
     }

     testClosePopup() {
        // hide popup before change view
        console.log("test close popup map view");
       this.setState({cuzheight: 0});
    }

    async updateFromLead() {
        console.log('update from lead');
        // update current lead
        const user_data = JSON.parse(await AsyncStorage.getItem('user_data'));

        let output_list_url_lead = Global.API_URL + "map_lead/";
        output_list_url_lead += user_data.uid + "/";
        output_list_url_lead += user_data.token + "/";
        output_list_url_lead += this.state.cuzid + "/";

        fetch(output_list_url_lead)
            .then((response) => response.json())
            .then((responseJson) => {

                if('1' == responseJson.status) {
                    // change marker
                    let markers = this.state.markers;
                    const idx = markers.findIndex(o => o.id === this.state.cuzid);

                    if(-1 !== idx) {
                        markers[idx]['lead_status'] = responseJson.data.lead_status;
                        markers[idx]['color'] = responseJson.data.color;
                        markers[idx]['name_lead'] = responseJson.data.name_lead;
                        markers[idx]['phone1'] = responseJson.data.phone1;
                        markers[idx]['phone2'] = responseJson.data.phone2;
                        this.setState({
                            markers: markers,
                            cuzstatus:responseJson.data.lead_status,
                            cuzname: responseJson.data.name_lead,
                            cuzphone1: responseJson.data.phone1,
                            cuzphone2: responseJson.data.phone2
                        }, function() {});
                    }

                }


            })
            .catch((error) => {
                console.error(error);
        });
    }

    render(): React$Element<*> {
         if (this.state.isLoading) {
            return (
                   <View style={{flex: 1, paddingTop: 60}}>
                     <ActivityIndicator />
                   </View>
                 );
         }
        const {navigation} = this.props;


        const varTop = height - 175;
   const hitSlop = {
     top: 15,
     bottom: 15,
     left: 15,
     right: 15,
   }
   bbStyle = function(vheight) {
     return {
       position: 'absolute',
       top: 70, // vheight
       //left: 10,
       right: 5,
       backgroundColor: 'transparent',
       alignItems: 'center',
       zIndex: 999
     }
   }


        return <BaseContainer title="Map View" {...{ navigation }} hide_footer={false}  close_popup={() => this.closePopup()}>
            <Container >
            <View style={Styles.listItem}>
                <View searchBar rounded style={{width:'100%'}}>
                    <Item>
                        <Input placeholder="Search" keyboardType="default"
                            autoCapitalize="none"
                            returnKeyType="next"
                            inverse
                            onChangeText={(value) => this.setState({keyword: value})}
                            onFocus={() => this.setState({cuzheight: 0})} // hide popup
                            value={this.state.keyword}/>
                        <Button style={styles.buttons}  onPress={() =>this.searchKey()} >
                            <Image source={Images.search}/>
                        </Button>
                    </Item>
                </View>
                {/* <Button onPress={() =>this.go("Filter")} style={styles.button}>
                    <Image source={Images.Filter}/>
                </Button> */}
             </View>

             {Platform.OS !== 'android' &&<View style={bbStyle(varTop)}>
         <TouchableOpacity
           hitSlop = {hitSlop}
           activeOpacity={0.7}
           style={styles.mapButton}
           onPress={ () => this.clickFindMe() }
         >
             <Text style={{fontWeight: 'bold', color: 'black',}}>
               Find Me
             </Text>
         </TouchableOpacity>
       </View>}
                    <MapView
                        provider={MapView.PROVIDER_GOOGLE}
                        style={{flex: this.state.flex,zIndex: -1, marginTop: this.state.map_margin_top}}
                        showsMyLocationButton={true}
                        showsUserLocation={true}
                        followUserLocation={true}
                        zoomEnabled={true}
                        // initialRegion={this.state.region}
                        region={this.state.region}
                        // clusterBorderWidth = {0}
                        //annotations={markers}

                        ref = {(ref)=>mapView=ref}
                        onClusterPress={(coordinate)=>{
                            this.animate(coordinate);
                        }}
                        onMapReady={() => this.onMapLayout()}
                        cacheEnabled={false}
                      >
                        {true && this.state.isMapReady && this.state.markers.map((marker, key) => (
                            <Marker
                                coordinate={marker.address}
                                // title={marker.name_lead}
                                image={this.changeColor(marker.color)}
                                onPress={() => this.cuzCallout(marker.name_lead,marker.lead_status,marker.id,marker.address.latitude,marker.address.longitude,marker.color,marker.phone1,marker.phone2, marker.address_original.latitude,marker.address_original.longitude, marker.address_map)}
                                >


                            </Marker>

                        ))}
                  </MapView>

                <View style={{height:this.state.cuzheight}} >

                    <View style={styles.bubble}>
                        <View style={{position: 'absolute',
                            right:     20,
                            top:      0,
                            bottom:30,
                            zIndex: 9999}}>
                            <TouchableHighlight
                            //   onPress={() => this.setState({cuzheight:0})}
                              onPress={() => this.testClosePopup()}
                              >
                            <Icon name="close" style={{fontSize: 25}}/>
                            </TouchableHighlight >
                        </View>
                        <Grid style={{paddingTop:30}}>
                            <Col style={{ width: "10%" }}>
                                <Image source={this.changeColor(this.state.cuzcolor)} />
                            </Col>
                            <Col>
                                <TouchableOpacity onPress={() => this.go1("ViewLead",{data:"None", output_key: this.state.cuzid, return_page: "MapsView", update_from_lead: () => this.updateFromLead()})}>
                                    <Text style={[styles.customView,{fontSize:20,fontWeight:"bold",color:"black"}]}>{this.state.cuzname}</Text>
                                </TouchableOpacity>
                                <Text style={styles.customView}>{this.state.cuzstatus}</Text>
                                <TouchableOpacity onPress={() => this.onPressCall(this.state.cuzphone1,"phone1")}>
                                    <Text style={[styles.customView,{fontWeight:"bold",color:"black"}]}>{this.state.cuzphone1}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.onPressCall(this.state.cuzphone2,"phone2")}>
                                    <Text style={[styles.customView,{fontWeight:"bold",color:"black"}]}>{this.state.cuzphone2}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.directionMap()}>
                                    <Text style={[styles.customView,{color:"#6AA6D6",fontSize:20}]}>GET DIRECTIONS</Text>
                                </TouchableOpacity>
                            </Col>
                        </Grid>
                    </View>
                </View>
            </Container>
        </BaseContainer>;
    }
}

const styles = StyleSheet.create({
    img: {
        ...WindowDimensions
    },
    customView: {

        // alignSelf:'center',
        marginLeft:5,
        marginBottom:20
      },
    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    button: {
        marginLeft: 5,
        backgroundColor: "#6AA6D6",
        alignSelf:'center',
        padding:0
        },
    text: {
        fontFamily: variables.titleFontfamily,
        color: "white"
    },

    latlng: {
        width: 200,
        alignItems: 'stretch',
        paddingTop:10,
    },
    bubble: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.7)',
        // paddingHorizontal: 18,
        // paddingVertical: 12,
        // borderRadius: 20,
      },
    map: {

        // top: 0,
        // left: 0,
        // right: 0,
        // bottom: 0,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginVertical: 20,
        backgroundColor: 'transparent',
      },
    buttons: {

        backgroundColor: "#6AA6D6",
        alignSelf:'center',
        // padding:0

    },
    mapButton: {
        width: 75,
        height: 75,
        borderRadius: 85/2,
        backgroundColor: 'rgba(252, 253, 253, 0.9)',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: 'black',
        shadowRadius: 8,
        shadowOpacity: 0.12,
        opacity: .6,
        zIndex: 10,
     },

});
