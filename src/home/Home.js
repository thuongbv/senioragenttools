// @flow
import autobind from "autobind-decorator";
import moment from "moment";
import React, {Component} from "react";
import {Text, Image, AsyncStorage, View,ActivityIndicator,Dimensions, StyleSheet,PermissionsAndroid, Alert, Platform} from "react-native";
import {H1,Container,Button} from "native-base";

import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Circle, Styles, Images, WindowDimensions,NavigationHelpers,Global } from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import Permissions from 'react-native-permissions';

const {height, width} = Dimensions.get("window");
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        };
     }

     async componentDidMount() {
        this.setState({
            isLoading: false
        });

        // get current coordinate
        // const geolocation_permision_allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
        // console.log(geolocation_permision_allowed);
        // if(geolocation_permision_allowed) {
        //     this._findMe();
        // } else {
        //     const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
        //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //         console.log("You can use user location");
        //         this._findMe();
        //       } else {
        //         console.log("Location permission denied")
        //       }
        // }

        Permissions.check('location').then(response => {
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            if('authorized' === response) {
                console.log("You can use user location");
                this._findMe();
            } else {
                // console.log(Platform.OS, response);
                Alert.alert(
                    'Can we access your location?',
                    'We need access location while using app',
                    [
                      {
                        text: 'No',
                        onPress: () => console.log('Permission denied'),
                        style: 'cancel',
                      },
                    //   response == 'undetermined'
                    //     ? { text: 'OK', onPress: this._requestPermission }
                    //     : { text: 'Open Settings', onPress: Permissions.openSettings },
                    { text: 'OK', onPress: this._requestPermission }
                    ]
                  )

                
            }
            
          })
      
    }

    _requestPermission = () => {
        Permissions.request('location').then(response => {
          // Returns once the user has chosen to 'allow' or to 'not allow' access
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          if ('authorized' === response) {
            console.log("You can use user location");
            this._findMe();
            } else {
            console.log("Location permission "+response)
            }
        })
      }

    _findMe(){
        navigator.geolocation.getCurrentPosition(
            async ({coords}) => {
              if(Platform.OS !== 'android') {
                navigator.geolocation.getCurrentPosition(function () {}, function () {}, {});
              }
              
            const {latitude, longitude} = coords;
            console.log(coords);
            const region = {
                latitude:latitude,
                longitude:longitude,
                latitudeDelta: 0.005,
                longitudeDelta: 0.001,
            };
            // save current coordinate to async storage
            await AsyncStorage.setItem('current_coordinate', JSON.stringify(region));
          },
          (error) => alert('Could not detect your current location. Please recheck your phone setting.')
          // {maximumAge: 0}
        )
      }
   
    go(key: string) {
        this.props.navigation.navigate(key);
    }
    
    render(): React$Element<*> {
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        const {navigation} = this.props;
        
        // const title = <TouchableOpacity onPress={() => this.go("Home")}><Text style={{color: "white"}}>Home</Text></TouchableOpacity>;
        const title = 'Home';
        return <BaseContainer title={title} {...{ navigation }}>
            <Container style={{backgroundColor: "#749ebe"}}>
               <View style={{
                       flex: 1,
                       flexDirection: 'column',
                       justifyContent: 'center',
                       width: width,
                       height: height
                     }}>
                    
                    <Image source={Images.navLogo}  style={style.sizeLogo}/>
                    <Text style={style.headline}>SIMPLY DESIGNED{'\n'}FOR FINAL EXPENSE AGENTS{'\n'}Version: 20190728.1</Text>
                    {/* <Button onPress={() => this.go("Settings")} style={style.button}>
                        <Text style={{color:"white"}}>Settings</Text>
                    </Button> */}
                </View>
            </Container>
        </BaseContainer>;
    }
}

const style = StyleSheet.create({
    button: {
        marginBottom: 20,
        backgroundColor: "#6AA6D6",
//        color: "white",
        alignSelf:'center'

    },
    text: {
        fontFamily: variables.titleFontfamily,
        color: "white"
    },
    headline: {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18,
        color: '#14574b'
      },
    sizeLogo:{
        // width : 238,
        // height: 50,
        width : width - 30,
        height: 256 * (width-30) /1210,
        alignSelf:'center',
        
        
    }
});