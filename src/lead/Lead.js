// @flow
import moment from "moment";
import React, {Component} from "react";
import {ListView,Text, Image, StyleSheet, View,FlatList,
    AsyncStorage,Dimensions,ActivityIndicator,TouchableWithoutFeedback,Keyboard} from "react-native";
import {H1,Container,Button,Item,Icon,Input} from "native-base";
import Toast from 'react-native-root-toast';
import RNFetchBlob from 'react-native-fetch-blob';
import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import { SearchBar } from 'react-native-elements';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class Lead extends Component {

    go(key: string) {
              this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    constructor(props) {
       super(props);
       
       this.state = {
            isLoading: true,
            page:1,
            output_key:'',
            search:'',
            keyword:'',
            uid:'',
            token:'',
            loadMore:true,
            dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
            data: [],
            is_focus_search: false,
            is_offline: false,
            isFilter: false
       };
       
       this.group = [];
       this.toast_configuration = {
           duration: Toast.durations.SHORT,
           position: Toast.positions.BOTTOM,
           shadow: true,
           animation: true,
           hideOnPress: true,
           delay: 0,
       };
       this.method = 'post';
    }
    
    async componentDidMount() {
        const {state} = this.props.navigation;
        
        if(state.params!=undefined){
            const is_focus_search = state.params.is_focus_search;
            console.log(is_focus_search);
            if(undefined !== is_focus_search && is_focus_search) {
                this.setState({is_focus_search: true,isLoading: false});
            } else {
                const data = state.params.data;
                const output_key = state.params.output_key;
                this.setState({
                    isLoading: false,
                    // dataSource:  this.state.dataSource.cloneWithRows(data),
                    data: data,
                    output_key: output_key
                });
            }
            
        }
    }

    async getLeadsOffline(pageNumber) {
        const json_file_path = await AsyncStorage.getItem('json_file_path');
        if(null !== json_file_path) {
            RNFetchBlob.fs.ls(json_file_path+'leads')
            // files will an array contains filenames
            .then(async (files) => {
                const count = 100;        
                const start = (pageNumber - 1) * count;
                const end = start + count - 1;
                if(start <= files.length) {
                    let leads_data = this.state.data;
                    // read files
                    for(let i = start; i <= end; i++) {
                        if(i <= files.length) {
                            RNFetchBlob.fs.readFile(json_file_path+'leads/'+files[i], 'utf8')
                            .then((file_content) => {
                            // handle the data ..
                                leads_data.push(JSON.parse(file_content));
                            })
                        }
                    }
                    
                    this.setState({data: leads_data, isLoading: false});
                }
            })
        }
    }

    async componentWillMount() {
        // get is_offline data from async storage
        const is_offline = JSON.parse(await AsyncStorage.getItem('is_offline'));
        this.setState({is_offline: null === is_offline ? this.state.is_offline : is_offline}, async function() {
            console.log('is offline: ', this.state.is_offline);
            if(this.state.is_offline) {
                // offline
                // get lead from files
                this.getLeadsOffline(1);
            } else {
                // online
                const {state} = this.props.navigation;
                if(state.params==undefined || (state.params!=undefined && undefined !== state.params.is_focus_search && state.params.is_focus_search)){
                        // console.log(1243432);
                    const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
                    let output_list_url = Global.API_URL + "leads/";

                    if('post' === this.method) {
                        let form_data = new FormData();
                        form_data.append("uid", user_dataa.uid);
                        form_data.append("token", user_dataa.token);
                        form_data.append("page_number", 1);
                        form_data.append("page_count", 100);

                        try {
                            let response = await fetch(output_list_url, {
                                method: 'POST',
                                headers: {
                                },
                                body: form_data
                            });
                            let responseJson = await response.json();
                            if(1 == responseJson.status) {
                                // saving user data
                                try {
                                    if('1' == responseJson.status&&responseJson.data.leads.length!=0) {
                                        mang =responseJson.data.leads;
                                        this.setState({
                                            isLoading: false,
                                            data: mang,
                                            uid:user_dataa.uid ,
                                            token:user_dataa.token,
        
                                    });
                                }
        
                                } catch (error) {
                                    console.error(error);
                                    Toast.show(error, this.toastConfiguration);
        
                                }
        
                            }else{
                                Toast.show(responseJson.msg, this.toastConfiguration);
                            }
        
                         } catch(error) {
        
                              Toast.show(error, this.toastConfiguration);
                         }
                    } else {
                        output_list_url += user_dataa.uid + "/";

                        output_list_url += user_dataa.token + "/";
                        output_list_url +=  1+ "/";
                        output_list_url += 100;
                        // console.log(output_list_url);
                        return fetch(output_list_url)
                            .then((response) => response.json())
                            .then((responseJson) => {
                                
                                if('1' == responseJson.status&&responseJson.data.leads.length!=0) {
                                    mang =responseJson.data.leads;
                                    // console.log(mang);
                                    this.setState({
                                        isLoading: false,
                                        // dataSource: this.state.dataSource.cloneWithRows(mang),
                                        data: mang,
                                        uid:user_dataa.uid ,
                                        token:user_dataa.token,
    
                                });
                            }
                            
                            })
                        .catch((error) => {
                            console.error(error);
                        });
                    }
                    
                }
            }
            
        });
        
    }
    async _onEndReached() {
        if(this.state.isFilter) {
            return;
        }
        console.log('on endreach');
        const {state} = this.props.navigation;
        if(state.params==undefined&&this.state.search != "searchKey"&&this.state.loadMore){
        let numpage=this.state.page+1;
        if(this.state.is_offline) {
            // offline
            // get lead from files
            await this.getLeadsOffline(numpage);
            this.setState({page: numpage});
        } else {
            // online
            const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
            let output_list_url = Global.API_URL + "leads/";

            if('post' === this.method) {
                let form_data = new FormData();
                form_data.append("uid", user_dataa.uid);
                form_data.append("token", user_dataa.token);
                form_data.append("page_number", numpage);
                form_data.append("page_count", 100);

                try {
                    let response = await fetch(output_list_url, {
                        method: 'POST',
                        headers: {
                        },
                        body: form_data
                    });
                    let responseJson = await response.json();
                    if(responseJson.data.leads.length==0){
                        this.setState({isLoading: false,loadMore:false})
                    }
                     if('1' == responseJson.status&&responseJson.data.length!=0) {
                        mang=mang.concat(responseJson.data.leads);
                        this.group = responseJson.data.leads;
                        this.setState({
                            // dataSource: this.state.dataSource.cloneWithRows(mang),
                            data: mang,
                            page: this.state.page+1
    
                        }); 
                    }

                 } catch(error) {

                      Toast.show(error, this.toastConfiguration);
                 }
            } else {
                output_list_url += user_dataa.uid + "/";
                output_list_url += user_dataa.token + "/";
                output_list_url +=  numpage+ "/";
                output_list_url += 100;
                    return fetch(output_list_url)
                        .then((response) => response.json())
                        .then((responseJson) => {
                        if(responseJson.data.leads.length==0){
                            this.setState({isLoading: false,loadMore:false})
                        }
                         if('1' == responseJson.status&&responseJson.data.length!=0) {
                            mang=mang.concat(responseJson.data.leads);
                            this.group = responseJson.data.leads;
                            this.setState({
                                // dataSource: this.state.dataSource.cloneWithRows(mang),
                                data: mang,
                                page: this.state.page+1
        
                            }); 
                        }
                        
                      })
                    .catch((error) => {
                    console.error(error);
                    });
            }
            
            }
        }
          
    }

    searchOffline = key => (lead) => {
        let arr = key.split(" ");
        let keyword = key;

        if(1 == arr.length && key.match(/^[0-9]*$/) && 10 == key.length) {
            // phone number: XXXXXXXXXX
            keyword = '('+key[0]+key[1]+key[2]+') '+key[3]+key[4]+key[5]+'-'+key[6]+key[7]+key[8]+key[9];
            arr= keyword.split(" ");
        }

        if (2 === arr.length){
            if(keyword.match(/[(]?\d{3}[-.)]?[ ]?\d{3}[-.]?\d{4}\b/)) {
                // phone number
                return (null !== lead.phone1 && 0 <= lead.phone1.indexOf(keyword)) || (null !== lead.phone2 && 0 <= lead.phone2.indexOf(keyword));
            } else {
                return null !== lead.first_name && 0 <= lead.first_name.indexOf(arr[0]) && null !== lead.last_name && 0 <= lead.last_name.indexOf(arr[1])
            }
        }else{
            let result = false;
            for(let i = 0; i < arr.length; i++){
                const keyword_1 = arr[i];
                if (keyword_1.trim() === ''){
                }else{
                    result = result || (null !== lead.first_name && 0 <= lead.first_name.indexOf(keyword_1)) || (null !== lead.last_name && 0 <= lead.last_name.indexOf(keyword_1)) || (null !== lead.phone1 && 0 <= lead.phone1.indexOf(keyword_1)) || (null !== lead.phone2 && 0 <= lead.phone2.indexOf(keyword_1));
                }

            }
            if(result) console.log(lead);
            return result;
        }
    }

    searchKey=async (order_name) =>{
        if(this.state.is_offline) {
            if('' === this.state.keyword) {
                return false;
            }
            // get all leads
            const json_file_path = await AsyncStorage.getItem('json_file_path');
            if(null !== json_file_path) {
                RNFetchBlob.fs.ls(json_file_path+'leads')
                // files will an array contains filenames
                .then(async (files) => {
                    this.setState({isLoading: true});
                    let leads_data = [];
                    const _this = this;
                    // read files
                    for(let i = 0; i < files.length; i++) {
                        RNFetchBlob.fs.readFile(json_file_path+'leads/'+files[i], 'utf8')
                        .then(async (file_content) => {
                        // handle the data ..
                            leads_data.push(JSON.parse(file_content));

                            if(i == files.length-1) {
                                // search keyword
                                const filter_leads = await leads_data.filter(_this.searchOffline(_this.state.keyword));
                                _this.setState({isLoading: false, data: filter_leads, isFilter: true}, function() {
                                    console.log(filter_leads);
                                });
                            }
                        });
                        
                    }
                    
            
                });
            }
        } else {
            const search = Global.API_URL + "search_leads/";
            const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
            let form_data_search = new FormData();
            
            form_data_search.append("uid", user_dataa.uid);
            form_data_search.append("token", user_dataa.token);
            form_data_search.append("keyword", this.state.keyword);
            form_data_search.append("lead_status","");
            form_data_search.append("record_day","");
            form_data_search.append("country", "");
            // form_data_search.append("city", this.state.city);
            form_data_search.append("type_view", "view_lead");
            form_data_search.append("page_number",1);
            form_data_search.append("page_count", 100);
            if('' !== order_name) {
                const order_direction_stored = await AsyncStorage.getItem(order_name+'_direction');
                const order_direction = null === order_direction_stored ? 'DESC' : ('ASC' === order_direction_stored ? 'DESC' : 'ASC');
                await AsyncStorage.setItem(order_name+'_direction', order_direction);
    
                form_data_search.append("order_name", order_name);
                form_data_search.append("order_direction", order_direction);
            }
            
            try {
                let response = await fetch(search, {
                    method: 'POST',
                    headers: {
                    },
                    body: form_data_search
                });
                let responseJson = await response.json();
    
                Toast.show(responseJson.msg, this.toastConfiguration);
        //                    NavigationHelpers.reset(this.props.navigation, "Home");
                if(1 == responseJson.status) {
                    //// login successfully
                    // saving user data
                    try {
                        let data=responseJson.data.leads
                        this.setState({
                            // dataSource:  this.state.dataSource.cloneWithRows(data),
                            data: data,
                            search: "searchKey"
                        },function(){
                            Keyboard.dismiss();
                            console.log(this.state.uid);
                            // this.componentWillMount(); 
                            // this.forceUpdate();
                        });                        
                    } catch (error) {
                    // Error saving data
                        console.error(error);
                        Toast.show(error, this.toastConfiguration);
                    }
                }else{
                    Toast.show(responseJson.msg, this.toastConfiguration);
                }
    
            } catch(error) {
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
        }
    }
    render(): React$Element<*> {
        const {navigation} = this.props;
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }

        return <BaseContainer title="List of Lead" {...{ navigation }}>
            <Container >
            <View style={Styles.listItem}>  
                <View searchBar rounded
                 style={{width:'100%'}}
                 >
                    <Item>
                        <Input placeholder="Search" keyboardType="default"
                            autoCapitalize="none"
                            returnKeyType="next"
                            inverse
                            onChangeText={(value) => this.setState({keyword: value})}
                            value={this.state.keyword}
                            autoFocus={this.state.is_focus_search}
                            />
                        <Button style={style.buttons}  onPress={() =>this.searchKey('')} >
                            <Image source={Images.search}/>
                        </Button>
                    </Item>
                </View>
                {/* <Button onPress={() =>this.go("Filter")} style={style.button}>
                    <Image source={Images.Filter}/>
                </Button>         */}
             </View>      
                
                <View style={[Styles.listItemView, style.group,{ marginLeft:5,marginBottom: 15,padding:10}]} last>
                    <View style={style.text}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                            <View style={{width: '25%'}} >
                                <TouchableWithoutFeedback onPress={() => this.searchKey('record_day')}>
                                    <Text>Lead Date</Text>
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={{width: '25%'}} >
                                <TouchableWithoutFeedback onPress={() => this.searchKey('lead_status')}>
                                    <Text>Lead Status</Text>
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={{width: '25%'}} >
                                <TouchableWithoutFeedback onPress={() => this.searchKey('first_name')}>
                                    <Text>First Name</Text>
                                </TouchableWithoutFeedback>
                            </View>
                            <View style={{width: '25%'}} >
                                <TouchableWithoutFeedback onPress={() => this.searchKey('last_name')}>
                                    <Text>Last Name</Text>
                                </TouchableWithoutFeedback>
                            </View>
                           
                            {/* <View style={{width: '30%'}} >
                                <Text>City</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>County</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>State</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>Zip</Text>
                            </View> */}
                        </View>
                    </View>
                </View>
                
               
                <FlatList
                    style={style.styleList}
                    onEndReached={this._onEndReached.bind(this)}
                    onEndReachedThreshold={100}
                    data={this.state.data}
                    keyExtractor={(item, index) => index}
                    renderItem={({item, index}) => 
                       
                        <TouchableWithoutFeedback onPress={() => this.go1("ViewLead",{data: item, output_key: item.id, leadIndex: index})}>
                            <View style={[Styles.listItemView, style.group]} last>
                                <View style={style.text}>
                                    <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                                            <View style={{width: '25%'}}>
                                                <Text>{item.record_day}</Text>
                                            </View>
                                            <View style={{width: '25%'}}>
                                                <Text>{item.lead_status}</Text>
                                            </View>
                                            <View style={{width: '25%'}} >
                                                <Text>{item.first_name}</Text>
                                            </View>
                                            <View style={{width: '25%'}} >
                                                <Text>{item.last_name}</Text>
                                            </View>
                                            {/* <View style={{width: '30%'}} >
                                                <Text>{item.city}</Text>
                                            </View> */}
                                            {/* <View style={{width: '30%'}} >
                                                <Text>{item.county}</Text>
                                            </View>
                                            <View style={{width: '30%'}} >
                                                <Text>{item.state}</Text>
                                            </View>
                                            <View style={{width: '30%'}} >
                                                <Text>{item.zip}</Text>
                                            </View> */}
                                            
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>

                    }
                    />
                   
                
            </Container>
        </BaseContainer>;
    }
}
const {width, height} = Dimensions.get("window");
const style = StyleSheet.create({
    styleList:{
        flex: 1,
        padding:0,
        margin:0
    },
    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    button: {
            marginLeft: 3,
            backgroundColor: "#6AA6D6",
            alignSelf:'center',
            padding:0
        },
    buttons: {
        
        backgroundColor: "#6AA6D6",
        alignSelf:'center', 
        // padding:0
        
    },
    group: {
            width:width,
            backgroundColor: variables.lightGray,
            justifyContent: "space-between",
            marginBottom: 1,
            
        },
    text: {
//        paddingLeft: variables.contentPadding * 2,
        paddingBottom:10
    },

});