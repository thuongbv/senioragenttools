// @flow
// import autobind from "autobind-decorator";
import React from "react";
import {View, Image, ImageBackground, StyleSheet, ScrollView, KeyboardAvoidingView, AsyncStorage, NetInfo} from "react-native";
import {H1, Container, Button, Text, Input, InputGroup} from "native-base";
import {NavigationActions} from "react-navigation";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";
import Toast from 'react-native-root-toast';

// import Mark from "./Mark";
import {Small, Styles, Images, Field, NavigationHelpers, WindowDimensions, Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";


export default class Login extends React.Component {

    props: {
        navigation: NavigationScreenProp<*, *>
    }

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        }
        this.toastConfiguration = {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
        }
    }

    async componentWillMount() {
        // const user_data = JSON.parse(await AsyncStorage.getItem('user_data'));
        // if(null !== user_data) {
        //     // already login => navigate to Main screen
        //     if("storage" === user_data.role) {
        //         NavigationHelpers.reset(this.props.navigation, "Home");
        //     } else if ("delivery" === responseJson.data.role || "delivery_support" === responseJson.data.role) {
        //         NavigationHelpers.reset(this.props.navigation, "DeliveryHome");
        //     } else {
        //         Toast.show("Under Construction", this.toastConfiguration);
        //     }
        // }
    }

    // @autobind
    signIn = () => {
        
        // check internet connection
        NetInfo.isConnected.fetch().then(async isConnected => {
        //     console.log(isConnected);
        
            if(isConnected) {
                ///// validate input
                if(this.state.username.trim()==='') {
                    Toast.show('Tên đăng nhập trống', this.toastConfiguration);
                return;
                }

                if(this.state.password.trim()==='') {
                    Toast.show('Mật khẩu trống', this.toastConfiguration);
                return;
                }

                // call api
                const login_url = Global.API_URL + "login/";
                let form_data_login = new FormData();
                form_data_login.append("username", this.state.username);
                form_data_login.append("password", this.state.password);
                
                
                return fetch(login_url,
                    {
                                method: 'POST',
                                headers: {},
                                body: form_data_login
                            })
                .then((response) => response.json())
                .then((responseJson) => {
                    Toast.show(responseJson.msg, this.toastConfiguration);
                    //   console.log(responseJson);
                    if('1' == responseJson.status) {
                        // get top list successfully
                        this.group = responseJson.data;
                    }
                 
                })
                .catch((error) => {
                    console.error(error);
                });
                
                // try {
                //     let response = await fetch(login_url, {
                //         method: 'POST',
                //         headers: {},
                //         body: form_data_login
                //     });

                //     let responseJson = await response.json();
                //     console.log(responseJson.data);
                //     Toast.show(responseJson.msg, this.toastConfiguration);
                //     if(1 == responseJson.status) {
                //         //// login successfully
                //         // saving user data
                //         try {
                //             // remove user data
                //             // console.log('remove');
                //             // await AsyncStorage.removeItem("user_data");

                //             console.log("huhuhu");
                            
                //             await AsyncStorage.setItem('user_data', JSON.stringify(responseJson.data));

                //             // AsyncStorage.setItem('user_data', JSON.stringify(responseJson.data), () => {
                //                 console.log("hihihihi");
                //             // });

                //             if("storage" === responseJson.data.role) {
                //                 NavigationHelpers.reset(this.props.navigation, "Home");
                //             } else if ("delivery" === responseJson.data.role || "delivery_support" === responseJson.data.role) {
                //                 NavigationHelpers.reset(this.props.navigation, "DeliveryHome");
                //             } else {
                //                 Toast.show("Under Construction", this.toastConfiguration);
                //             }
                            
                            
                //         } catch (error) {
                //         // Error saving data
                //             console.error(error);
                //             Toast.show(error, this.toastConfiguration);

                //         }
                        
                //     }

                // } catch(error) {
                //     console.error(error);
                //     Toast.show(error, this.toastConfiguration);
                // }
            } else {
                Toast.show('Không có kết nối mạng', this.toastConfiguration);
            }
        
        });

       
        
    }

    // @autobind
    signUp() {
        this.props.navigation.navigate("SignUp");
    }

    // @autobind
    forgotPassword() {
        this.props.navigation.navigate("ForgotPassword");
    }

    // @autobind
    signInFacebook() {
        Toast.show('Work is under construction', {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            onShow: () => {
                // calls on toast\`s appear animation start
            },
            onShown: () => {
                // calls on toast\`s appear animation end.
            },
            onHide: () => {
                // calls on toast\`s hide animation start.
            },
            onHidden: () => {
                // calls on toast\`s hide animation end.
            }
        });
    }

    render(): React$Element<*> {
        return <ImageBackground source={Images.login} style={style.img}>
            <Container style={StyleSheet.flatten(Styles.imgMask)}>
                <ScrollView contentContainerStyle={style.content}>
                    <KeyboardAvoidingView behavior="padding">
                        <View style={style.logo}>
                            <View>
                                {/* <Mark /> */}
                                <H1 style={StyleSheet.flatten(style.title)}>IManager</H1>
                            </View>
                        </View>
                        <View style={style.blur}>
                            <Field label="Tên đăng nhập"
                                    keyboardType="email-address"
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    inverse
                                    onChangeText={(value) => this.setState({username: value})}
                                    value={this.state.username}
                                />
                            
                            <Field label="Mật khẩu"
                                   secureTextEntry
                                   autoCapitalize="none"
                                   returnKeyType="go"
                                   onSubmitEditing={() => this.signIn()}
                                   last
                                   inverse
                                   onChangeText={(value) => this.setState({password: value})}
                                   value={this.state.password}
                            />
                            <View>
                                <View>
                                    <Button danger full onPress={() => this.signIn()} style={style.button}>
                                        <Text>Đăng nhập</Text>
                                    </Button>
                                </View>
                                {/* <View>
                                    <Button transparent full onPress={() => this.forgotPassword()}>
                                        <Small style={{color: "white"}}>Forgot password?</Small>
                                    </Button>
                                </View>
                                <View>
                                    <Button warning full onPress={() => this.signUp()} style={style.button}>
                                        <Text>Register</Text>
                                    </Button>
                                </View> */}
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </Container>
        </ImageBackground>;
    }
}

const style = StyleSheet.create({
    img: {
        // resizeMode: "cover",
        ...WindowDimensions
    },
    content: {
        flex: 1,
        justifyContent: "center"
    },
    logo: {
        alignSelf: "center",
        marginBottom: variables.contentPadding * 2
    },
    title: {
        marginTop: variables.contentPadding * 2,
        color: "white",
        textAlign: "center"
    },
    blur: {
        backgroundColor: "rgba(255, 255, 255, .2)"
    },
    button: {
        marginBottom: 20
    }
});
