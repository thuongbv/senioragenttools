// @flow
// import autobind from "autobind-decorator";
import React from "react";
//import Hr from "react-native-hr";
//import Hr from 'react-native-hr-plus';
import {View, Image, ImageBackground, StyleSheet, ScrollView, KeyboardAvoidingView, AsyncStorage, NetInfo} from "react-native";
import {H1, Container, Button, Text, Input, InputGroup,Item} from "native-base";
import {NavigationActions} from "react-navigation";
import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";
import Toast from 'react-native-root-toast';


// import Mark from "./Mark";
import {Small, Styles, Images, Field, NavigationHelpers, WindowDimensions, Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";


export default class Login extends React.Component {

    props: {
        navigation: NavigationScreenProp<*, *>
    }

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        }
        this.toastConfiguration = {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
        }
    }
     async componentWillMount() {
            const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
             if(null !== user_dataa) {
                NavigationHelpers.reset(this.props.navigation, "Home");

            }
    }
    // @autobind
    signIn = async () => {
        
        // check internet connection
        // NetInfo.isConnected.fetch().then(async isConnected => {
            // console.log(hihi);
            const isConnected=true

            // if(true) {
                ///// validate input
                if(this.state.username.trim()==='') {
                    Toast.show('Username must NOT blank', this.toastConfiguration);
                return;
                }

                if(this.state.password.trim()==='') {
                    Toast.show('Password must NOT blank', this.toastConfiguration);
                return;
                }

                // call api
                
                const login_url = Global.API_URL + "login/";
                
                let form_data_login = new FormData();
//                console.log(form_data_login);
                form_data_login.append("username", this.state.username);
                form_data_login.append("password", this.state.password);
               
               
                
                 try {
                    console.log(login_url);
                    let response = await fetch(login_url, {
                        method: 'POST',
                        headers: {
                        },
                        body: form_data_login
                    });
                    let responseJson = await response.json();
                    console.log(responseJson.status);

                    Toast.show(responseJson.msg, this.toastConfiguration);
//                    NavigationHelpers.reset(this.props.navigation, "Home");
                    if(1 == responseJson.status) {
                        //// login successfully
                        // saving user data
                        try {

                            await AsyncStorage.setItem('user_data', JSON.stringify(responseJson.data));

                            NavigationHelpers.reset(this.props.navigation, "Home");

                        } catch (error) {
                        // Error saving data
                            console.error(error);
                            Toast.show(error, this.toastConfiguration);

                        }

                    }else{
                        Toast.show(responseJson.msg, this.toastConfiguration);
                    }

                 } catch(error) {
//                     console.log(1234);
                      console.error(error);
                      Toast.show(error, this.toastConfiguration);
                 }
            // } else {
            //     Toast.show('Không có kết nối mạng', this.toastConfiguration);
            // }
        
        // });

       
        
    }


    // @autobind
    signUp() {
        this.props.navigation.navigate("SignUp");
    }

    // @autobind
    forgotPassword() {
        this.props.navigation.navigate("ForgotPassword");
    }

    // @autobind
    signInFacebook() {
        Toast.show('Work is under construction', {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            onShow: () => {
                // calls on toast\`s appear animation start
            },
            onShown: () => {
                // calls on toast\`s appear animation end.
            },
            onHide: () => {
                // calls on toast\`s hide animation start.
            },
            onHidden: () => {
                // calls on toast\`s hide animation end.
            }
        });
    }

    render(): React$Element<*> {
        return <ImageBackground source={Images.login} style={style.img}>
            <Container style={StyleSheet.flatten(Styles.bgContainer)}>
                <ScrollView contentContainerStyle={style.content}>
                    <KeyboardAvoidingView behavior="padding">
                        <View style={style.blur}>
                            <View>
                                {/* <Mark /> */}
                                <H1 style={[StyleSheet.flatten(style.title),style.tagHr]}>Navigator</H1>
                            </View>

                        </View>
                        <View style={style.blur}>
                            <Text style={{marginLeft:5,marginTop: 10}}>Username </Text>
                            <Item style={style.tagInput}>
                                <Input
                                    keyboardType="email-address"
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    inverse
                                    onChangeText={(value) => this.setState({username: value})}
                                    value={this.state.username}
                                />
                            </Item>
                            <Text style={{marginLeft:5,marginTop: 10}}>Password</Text>
                            <Item style={style.tagInput}>
                                <Input
                                       secureTextEntry
                                       autoCapitalize="none"
                                       returnKeyType="go"
                                       onSubmitEditing={() => this.signIn()}
                                       last
                                       inverse
                                       onChangeText={(value) => this.setState({password: value})}
                                       value={this.state.password}
                                />
                            </Item>
                            
                            <View >
                                <View>
                                    <Button full onPress={() => this.signIn()} style={style.button}>
                                        <Text>Sign in</Text>
                                    </Button>
                                </View>

                            </View>

                        </View>

                    </KeyboardAvoidingView>
                </ScrollView>
            </Container>
        </ImageBackground>;
    }
}

const style = StyleSheet.create({
    img: {
        // resizeMode: "cover",
        ...WindowDimensions
    },
    content: {
        flex: 1,
        justifyContent: "center",
        backgroundColor:"#ECF0F3",
        
    },
    logo: {
        alignSelf: "center",
        marginBottom: variables.contentPadding * 2
    },
    title: {
        fontWeight: 'bold',
        marginTop: variables.contentPadding * 4,
        color: "black",
        textAlign: "center",
        fontSize: 30,
        fontWeight: '100'
    },
    blur: {
        backgroundColor: "#ECF0F3",
        margin:20
    },
    button: {
        marginTop: 20,
        backgroundColor: "#6AA6D6",
//        color: "white",
        // alignSelf:'center'

    },
    tagInput:{
        borderColor:"gray",
        height:40
    },

});
