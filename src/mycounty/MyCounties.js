// @flow
import moment from "moment";
import React, {Component} from "react";
import {ListView,Text, Image, StyleSheet, View,ImageBackground,
    AsyncStorage,Dimensions,ActivityIndicator,TouchableWithoutFeedback,ScrollView} from "react-native";
import {H1,Container,Button} from "native-base";
import Toast from 'react-native-root-toast';


import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

export default class MyCounties extends Component {

    go(key: string) {
              this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    constructor(props) {
       super(props);
       this.state = {
        //  isLoading: true,
            isLoading: true,
         page:1,
         dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
       };
       this.group = [];
       this.toast_configuration = {
           duration: Toast.durations.SHORT,
           position: Toast.positions.BOTTOM,
           shadow: true,
           animation: true,
           hideOnPress: true,
           delay: 0,
       };
       
    }
    async componentWillMount() {
       
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let output_list_url = Global.API_URL + "counties/";
        output_list_url += user_dataa.uid + "/";
        output_list_url += user_dataa.token;
        console.log(output_list_url);
        return fetch(output_list_url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson.data);
                if('1' == responseJson.status) {
                    mang =responseJson.data;
                    console.log(mang);
                    this.setState({
                        isLoading: false,
                        dataSource: this.state.dataSource.cloneWithRows(mang)
                    

                });
                console.log(this.state.dataSource);
                
            }
            
            })
        .catch((error) => {
        console.error(error);
        });
        
    }
   
    render(): React$Element<*> {
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        const {navigation} = this.props;
        // console.log(this.state.dataSource);
        return <BaseContainer title="Lead View" {...{ navigation }}>
            <Container >
                    <Button onPress={() => this.go("MyCounties")} style={style.button}>
                        <Text style={{color:"white"}}>Add new County</Text>
                    </Button>
                <View style={[Styles.listItemView, style.group]} last>
                    <View style={style.text}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                            <View style={{width: '50%'}} >
                                <Text>County Name</Text>
                            </View>
                            <View style={{width: '50%'}} >
                                <Text>Action</Text>
                            </View>
                            
                        </View>
                    </View>
                </View>
                
               
                <ListView
                   
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => 
                    
                        <TouchableWithoutFeedback onPress={() => this.go("MyCounties")}>
                            <View style={[Styles.listItemView, style.group]} last>
                                <View style={style.text}>
                                    <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                                            <View style={{width: '50%'}} >
                                                <Text>{rowData.county_name}</Text>
                                            </View>
                                            <View style={{width: '50%'}} >
                                                <Text>Delete</Text>
                                            </View>
                                            
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    
                    }
                   
                    
                    />
              
                
            </Container>
        </BaseContainer>;
    }
    


}

const {width, height} = Dimensions.get("window");
const style = StyleSheet.create({

    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    button: {
        marginBottom: 10,
        marginTop: 10,
        marginLeft: 5,
        backgroundColor: "#6AA6D6",
//        color: "white",
        padding:0
        },
    group: {
            width:width,
            backgroundColor: variables.lightGray,
            justifyContent: "space-between",
            marginBottom: 10
        },
    text: {
//        paddingLeft: variables.contentPadding * 2,
        paddingBottom:10
    },

});