// @flow
import autobind from "autobind-decorator";
import moment from "moment";
import React, {Component} from "react";
import {Text, Image, StyleSheet, View,ImageBackground,Dimensions,AsyncStorage,ScrollView,KeyboardAvoidingView,ActivityIndicator} from "react-native";
import {H1,Container,Button,Item,Input,Picker} from "native-base";

import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Circle, Styles, Images, WindowDimensions,NavigationHelpers,Global} from "../components";
import variables from "../../native-base-theme/variables/commonColor";
import Toast from 'react-native-root-toast';
export default class MyProfile extends Component {
   
    go(key: string) {
        this.props.navigation.navigate(key);
    }
    constructor(props) {
        super(props);
        
        this.state = {
            isLoading: true,
            name: '',
            password: '',
            state_code:'',
            phone_number:'',
            city:'',
            zipcode:'',
            allow_lead_from_nav:0,
            uid:'',
            token:'',
            address:''
        }
        this.state_codes=[];
        this.allow_lead_from_navs=[{"key":0,"value":"No"},{"key":-1,"value":"Yes"}];
        this.toastConfiguration = {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
        }
    }
    async componentWillMount() {
       
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let output_list_url = Global.API_URL + "states/";
        output_list_url += user_dataa.uid + "/";
        
        output_list_url += user_dataa.token + "/";
        // console.log(output_list_url);
        return fetch(output_list_url)
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            if('1' == responseJson.status) {
                // get top list successfully
              this.state_codes = responseJson.data;
                
            }
          // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
          this.setState({
            isLoading: false,
            uid:user_dataa.uid,
            token:user_dataa.token,
            name:user_dataa.name,
            email:user_dataa.email,
            state_code:user_dataa.state_code,
            phone_number:user_dataa.phone_number,
            city:user_dataa.city,
            zipcode:user_dataa.zipcode,
            address:user_dataa.address,
            allow_lead_from_nav:user_dataa.allow_lead_from_nav
          //   dataSource: ds.cloneWithRows(responseJson.movies),
          }, function() {
            console.log(this.state.zipcode);
          });
        })
        .catch((error) => {
          console.error(error);
        });
        
        
    }
    EditProfile = async () => {
        const isConnected=true

            // call api

        const edit_user = Global.API_URL + "edit_user/";

        let form_data_edit_user = new FormData();
//                console.log(form_data_login);
        form_data_edit_user.append("uid", this.state.uid);
        form_data_edit_user.append("token", this.state.token);
        form_data_edit_user.append("name", this.state.name);
        form_data_edit_user.append("state_code", this.state.state_code);
        form_data_edit_user.append("phone_number", this.state.phone_number);
        form_data_edit_user.append("city", this.state.city);
        form_data_edit_user.append("zipcode", this.state.zipcode);
        form_data_edit_user.append("allow_lead_from_nav", this.state.allow_lead_from_nav);
        form_data_edit_user.append("address", this.state.address);               
                
        try {
            console.log(edit_user);
            let response = await fetch(edit_user, {
                method: 'POST',
                headers: {
                },
                body: form_data_edit_user
            });
            let responseJson = await response.json();
            console.log(responseJson.status);

            Toast.show(responseJson.msg, this.toastConfiguration);
//                    NavigationHelpers.reset(this.props.navigation, "Home");
            if(1 == responseJson.status) {
                //// login successfully
                // saving user data
                try {

                    await AsyncStorage.setItem('user_data', JSON.stringify(responseJson.data));

                    NavigationHelpers.reset(this.props.navigation, "Home");

                } catch (error) {
                // Error saving data
                    console.error(error);
                    Toast.show(error, this.toastConfiguration);

                }

            }else{
                Toast.show(responseJson.msg, this.toastConfiguration);
            }

            } catch(error) {
//                     console.log(1234);
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
            // } else {
            //     Toast.show('Không có kết nối mạng', this.toastConfiguration);
            // }
        
        // });

       
        
    }
    
    render(): React$Element<*> {
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        const {navigation} = this.props;
        return <BaseContainer title="Edit Profile" {...{ navigation }}>
            <Container style={StyleSheet.flatten(Styles.bgContainer)}>
                <ScrollView contentContainerStyle={style.content}>
                    <KeyboardAvoidingView behavior="padding">
                        <View style={style.blur}>
                            <Text style={{marginLeft:5,marginTop: 10}}>Email</Text>
                            <Item style={style.tagInput}>
                                <Input
                                    textContentType="none"
                                    keyboardType="email-address"
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    inverse
                                    onChangeText={(email) => this.setState({email})}
                                    value={this.state.email}
                                />
                            </Item>
                            <Text style={{marginLeft:5,marginTop: 10}}>Name*</Text>
                            <Item style={style.tagInput}>
                                <Input
                                       keyboardType="default"
                                       autoCapitalize="none"
                                       returnKeyType="next"
                                       inverse
                                       onChangeText={(name) => this.setState({name: name})}
                                       value={this.state.name}
                                />
                            </Item>
                            <Text style={{marginLeft:5,marginTop: 10}}>Address*</Text>
                            <Item style={style.tagInput}>
                                <Input
                                       keyboardType="default"
                                       autoCapitalize="none"
                                       returnKeyType="next"
                                       inverse
                                       onChangeText={(address) => this.setState({address: address})}
                                       value={this.state.address}
                                />
                            </Item>
                            
                            <Text style={{marginLeft:5,marginTop: 10}}>State*</Text>
                            <Picker
                                mode="dropdown"
                                selectedValue={this.state.state_code}
                                onValueChange={(state_code) => this.setState({state_code: state_code})}
                                >
                                    {this.state_codes.map((item, index) => (
                                        <Item label={item.state_name} value={item.state_code} />
                                    ))}
                            </Picker>
                            <Text style={{marginLeft:5,marginTop: 10}}>Phone number*</Text>
                            <Item style={style.tagInput}>
                                <Input
                                       keyboardType="default"
                                       autoCapitalize="none"
                                       returnKeyType="next"
                                       inverse
                                       onChangeText={(phone_number) => this.setState({phone_number: phone_number})}
                                       value={this.state.phone_number}
                                />
                            </Item>
                            <Text style={{marginLeft:5,marginTop: 10}}>City*</Text>
                            <Item style={style.tagInput}>
                                <Input
                                       keyboardType="default"
                                       autoCapitalize="none"
                                       returnKeyType="next"
                                       inverse
                                       onChangeText={(city) => this.setState({city: city})}
                                       value={this.state.city}
                                />
                            </Item>
                            <Text style={{marginLeft:5,marginTop: 10}}>Zipcode*</Text>
                            <Item style={style.tagInput}>
                                <Input
                                       keyboardType="default"
                                       autoCapitalize="none"
                                       returnKeyType="next"
                                       inverse
                                       onChangeText={(zipcode) => this.setState({zipcode: zipcode})}
                                       value={this.state.zipcode}
                                />
                            </Item>
                            <Text style={{marginLeft:5,marginTop: 10}}>Allow copy to Tracker*</Text>
                            <Picker
                                mode="dropdown"
                                selectedValue={this.state.allow_lead_from_nav}
                                onValueChange={(allow_lead_from_nav) => this.setState({allow_lead_from_nav: allow_lead_from_nav})}
                                >
                                    {this.allow_lead_from_navs.map((item, index) => (
                                        <Item label={item.value} value={item.key} />
                                    ))}
                            </Picker>
                            <View >
                                <View>
                                    <Button full onPress={() => this.EditProfile()} style={style.button}>
                                        <Text style={{color: "white"}}>Update</Text>
                                    </Button>
                                </View>

                            </View>

                        </View>

                    </KeyboardAvoidingView>
                </ScrollView>
            </Container>
        </BaseContainer>;
    }
}
const style = StyleSheet.create({
    img: {
        // resizeMode: "cover",
        ...WindowDimensions
    },
    content: {
        
        backgroundColor:"#ECF0F3",
        
    },
    logo: {
        alignSelf: "center",
        marginBottom: variables.contentPadding * 2
    },
    title: {
        fontWeight: 'bold',
        marginTop: variables.contentPadding * 4,
        color: "black",
        textAlign: "center",
        fontSize: 30,
        fontWeight: '100'
    },
    blur: {
        backgroundColor: "#ECF0F3",
        margin:20
    },
    button: {
        marginTop: 20,
        backgroundColor: "#6AA6D6",
       
        // alignSelf:'center'

    },
    tagInput:{
        borderColor:"gray",
        height:40
    },

});
