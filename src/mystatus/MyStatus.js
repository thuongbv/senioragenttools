// @flow
import moment from "moment";
import React, {Component} from "react";
import {ListView,Text, Image, StyleSheet, View,ImageBackground,
    AsyncStorage,Dimensions,ActivityIndicator,TouchableWithoutFeedback,ScrollView} from "react-native";
import {H1,Container,Button} from "native-base";
import Toast from 'react-native-root-toast';


import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

export default class MyCounties extends Component {

    go(key: string) {
              this.props.navigation.navigate(key);
    }
    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }
    constructor(props) {
       super(props);
       this.state = {
        //  isLoading: true,
            isLoading: true,
         page:1,
         dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
         dataSourceCuz: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
       };
       this.group = [];
       this.toast_configuration = {
           duration: Toast.durations.SHORT,
           position: Toast.positions.BOTTOM,
           shadow: true,
           animation: true,
           hideOnPress: true,
           delay: 0,
       };
       
    }
    changeColor(color) {
        switch (color) {
            case 'Blue':
                return  require('../components/img/Blue.png');
                break; 
            case 'Black':
                return  require('../components/img/Black.png');
                break;
            case 'BurlyWood':
                return  require('../components/img/BurlyWood.png');
                break;
            case 'DeepSkyBlue':
                return  require('../components/img/DeepSkyBlue.png');
                break; 
            case 'FireBrick':
                return require('../components/img/FireBrick.png');
                break; 
            case 'Fuschia':
                return  require('../components/img/Fuschia.png');
                break; 
            case 'Gold':
                return  require('../components/img/Gold.png');
                break;
            case 'Green':
                return require('../components/img/Green.png');
                break;
            case 'Grey':
                return require('../components/img/Grey.png');
                break;
            case 'Lime':
                return require('../components/img/Lime.png');
                break;
            case 'Purple':
                return require('../components/img/Purple.png');
                break;
            case 'Red':
                return  require('../components/img/Red.png');
                break;
            case 'Salmon':
                return  require('../components/img/Salmon.png');
                break;
            case 'Sienna':
                return  require('../components/img/Sienna.png');
                break;
            case 'SlateBlue':
                return require('../components/img/SlateBlue.png');
                break;
            case 'Teal':
                return require('../components/img/SlateBlue.png');
                break;
            default :
                return require('../components/img/SlateBlue.png');
                break;
        }
    }
    async componentWillMount() {
       
        const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
        let output_list_url = Global.API_URL + "statuses/";
        output_list_url += user_dataa.uid + "/";
        output_list_url += user_dataa.token;
        console.log(output_list_url);
        return fetch(output_list_url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson.data);
                if('1' == responseJson.status) {
                    mangCuz =responseJson.data.custom;
                    mang =responseJson.data.default;
                    console.log(mang);
                    this.setState({
                        isLoading: false,
                        dataSource: this.state.dataSource.cloneWithRows(mang),
                        dataSourceCuz:this.state.dataSource.cloneWithRows(mangCuz)

                });
                // console.log(this.state.dataSource);
                
            }
            
            })
        .catch((error) => {
        console.error(error);
        });
        
    }
   
    render(): React$Element<*> {
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        const {navigation} = this.props;
        // console.log(this.state.dataSource);
        return <BaseContainer title="My status" {...{ navigation }}>
            <Container >
                <View style={[Styles.listItemView, style.group]} last>
                    <View style={style.text}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                            <View style={{width: '30%'}} >
                                <Text>Pin</Text>
                            </View>
                            <View style={{width: '30%'}} >
                                <Text>Name</Text>
                            </View>
                            <View style={{width: '50%'}} >
                                <Text>Action</Text>
                            </View>
                            
                        </View>
                    </View>
                </View>
                
                <ScrollView >
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => 
                        <TouchableWithoutFeedback onPress={() => this.go("MyCounties")}>
                            <View style={[Styles.listItemView, style.group]} last>
                                <View style={style.text}>
                                    <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                                            <View style={{width: '30%'}} >
                                                <Image source={this.changeColor(rowData.color_custom)} />
                                            </View>
                                            <View style={{width: '30%'}} >
                                                <Text>{rowData.default_name}</Text>
                                            </View>
                                            <View style={{width: '50%'}} >
                                                <Text>Edit</Text>
                                            </View>
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    
                    }
                    />
                <ListView
                   dataSource={this.state.dataSourceCuz}
                   renderRow={(rowData) => 
                       <TouchableWithoutFeedback onPress={() => this.go("MyCounties")}>
                           <View style={[Styles.listItemView, style.group]} last>
                               <View style={style.text}>
                                   <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                                           <View style={{width: '30%'}} >
                                                <Image source={this.changeColor(rowData.custer_color)} />
                                           </View>
                                           <View style={{width: '30%'}} >
                                               <Text>{rowData.name}</Text>
                                           </View>
                                           <View style={{width: '50%', flexDirection: 'row'}} >
                                               <Text style={{width: '30%'}}>Edit</Text>
                                               <Text style={{width: '30%'}}>Delete</Text>
                                           </View>
                                           
                                   </View>
                               </View>
                           </View>
                       </TouchableWithoutFeedback>
                   
                   }
                  
                   
                   />
                </ScrollView >
            </Container>
        </BaseContainer>;
    }
    


}

const {width, height} = Dimensions.get("window");
const style = StyleSheet.create({

    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    button: {
            marginBottom: 20,
            marginRight: 5,
            backgroundColor: "#6AA6D6",
    //        color: "white",
            alignSelf:'center',
            padding:0
        },
    group: {
            width:width,
            backgroundColor: variables.lightGray,
            justifyContent: "space-between",
            marginBottom: 10
        },
    text: {
//        paddingLeft: variables.contentPadding * 2,
        paddingBottom:10
    },

});