// @flow
import autobind from "autobind-decorator";
import moment from "moment";
import React, {Component} from "react";
import {Text, Image, StyleSheet, View,ImageBackground,Dimensions,ScrollView,KeyboardAvoidingView,ActivityIndicator,AsyncStorage } from "react-native";
import {H3,Container,Button,Picker,Item,Input} from "native-base";
 import { Col, Row, Grid } from 'react-native-easy-grid';
import Toast from 'react-native-root-toast';
import RNFetchBlob from 'react-native-fetch-blob';
import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global,NavigationHelpers } from "../components";

import variables from "../../native-base-theme/variables/commonColor";
const {width, height} = Dimensions.get("window");

export default class Notes extends Component {
   
    go(data: string, detail_data: object) {
        this.props.navigation.navigate(data, detail_data);
    }
    
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: {},
            output_key: 0,
            edit_content_notes:'',
            uid:'',
            token:'',
            hide_footer: true, // always hide footer
            is_offline: false,
            leadIndex: null
        };
        this.notes_full = [];
    }
    async componentWillMount() {
        const {state} = this.props.navigation;
        const data = state.params.data;
        const output_key = state.params.output_key;
        const leadIndex = state.params.leadIndex;

        // get is_offline data from async storage
        const is_offline = JSON.parse(await AsyncStorage.getItem('is_offline'));
        this.setState({is_offline: null === is_offline ? this.state.is_offline : is_offline}, async function() {
            console.log('is offline: ', this.state.is_offline);
            if(this.state.is_offline) {
                // offine
                // get notes
                this.notes_full = await this.getDataOffline('notes');
                const note = this.notes_full.find(n => n.note_id == output_key);

                if(undefined !== note) {
                    this.setState({
                        isLoading: false,
                        edit_content_notes:note.content,
                        data: note,
                        output_key: output_key,
                        leadIndex: leadIndex
                    });
                }
            } else {
                // online
                const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
                this.setState({
                    isLoading: false,
                    edit_content_notes:data.content,
                    uid:user_dataa.uid,
                    token:user_dataa.token,
                    data: data,                    
                    output_key: output_key,
                    leadIndex: leadIndex
                });
            }
        });
    }

    async getDataOffline(data_name) {
        const json_file_path = await AsyncStorage.getItem('json_file_path');
        if(null !== json_file_path) {
            return RNFetchBlob.fs.readFile(json_file_path+data_name+'.json', 'utf8')
            .then((file_content) => {
            // handle the data ..
                return JSON.parse(file_content);
            })
        }
    }

    deleteNote=async () =>{
        if(this.state.is_offline) {
            // offline
            let offline_data_change = JSON.parse(await AsyncStorage.getItem('offline_data_change'));
            const note = {note_id: this.state.data.note_id, lead_id: this.state.data.lead_id, isDelete: true};
            // check if note is create offline
            const note_check_index = offline_data_change.notes.findIndex(n => n.note_id == this.state.data.note_id);
            if(-1 !== note_check_index) {
                // remove
                offline_data_change.notes.filter(n => n.note_id != this.state.data.note_id);
            } else {
                // update: add field is_delete
                offline_data_change.notes.push(note);
            }
            const _this = this;
            await AsyncStorage.setItem('offline_data_change', JSON.stringify(offline_data_change), async function() {
                // update notes in props
                const note_full_check_index = _this.notes_full.findIndex(n => n.note_id == _this.state.data.note_id);
                if(-1 !== note_full_check_index) {
                    _this.notes_full[note_full_check_index] = note;
                }

                // update file
                const json_file_path = await AsyncStorage.getItem('json_file_path');
                RNFetchBlob.fs.writeFile(json_file_path+'notes.json', JSON.stringify(_this.notes_full), 'utf8')
                .then(()=>{
                    try {
                        console.log(_this.state.leadIndex);
                        _this.go("ViewLead",{data:"None", output_key: _this.state.data.lead_id, leadIndex: _this.state.leadIndex});
                    } catch (error) {
                        Toast.show(error, _this.toastConfiguration);
                    }
                })
            });
        } else {
            // online
            // call api
            const delete_note = Global.API_URL + "note/";
            const form_data_delete_note="uid="+this.state.uid+"&token="+this.state.token+"&id="+this.state.data.note_id+"&lead_id="+ this.state.data.lead_id;

            try {
            
                let response = await fetch(delete_note, {
                    method: 'DELETE',
                    headers: {
                        
                    },
                    body: form_data_delete_note,
                });

                let responseJson = await response.json();
                Toast.show(responseJson.msg, this.toastConfiguration);
                    
                if(1 == responseJson.status) {
                    //// login successfully
                    // saving user data
                    try {
                        this.go("ViewLead",{data:"None", output_key: this.state.data.lead_id});
                        console.log(this.state.data.lead_id);
                    } catch (error) {
                    // Error saving data
                        Toast.show(error, this.toastConfiguration);

                    }

                }else{
                    Toast.show(responseJson.msg, this.toastConfiguration);
                }

            } catch(error) {
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
        }
        
    }
   updateNote=async () =>{
        if(this.state.edit_content_notes.trim()==='') {
            Toast.show('Content note not bank', this.toastConfiguration);
            return;
        }

        if(this.state.is_offline) {
            // offline
            let offline_data_change = JSON.parse(await AsyncStorage.getItem('offline_data_change'));
            const note = {note_id: this.state.data.note_id, lead_id: this.state.data.lead_id, content: this.state.edit_content_notes, date_time: this.state.data.date_time};
            // check if note is create/update offline => update
            const note_check_index = offline_data_change.notes.findIndex(n => n.note_id == this.state.data.note_id);
            // console.log(note_check_index);
            if(-1 !== note_check_index) {
                offline_data_change.notes[note_check_index] = note;
            } else {
                offline_data_change.notes.push(note);
            }
            const _this = this;
            await AsyncStorage.setItem('offline_data_change', JSON.stringify(offline_data_change), async function() {
                // update notes in props
                const note_full_check_index = _this.notes_full.findIndex(n => n.note_id == _this.state.data.note_id);
                if(-1 !== note_full_check_index) {
                    _this.notes_full[note_full_check_index] = note;
                }

                // update file
                const json_file_path = await AsyncStorage.getItem('json_file_path');
                RNFetchBlob.fs.writeFile(json_file_path+'notes.json', JSON.stringify(_this.notes_full), 'utf8')
                .then(()=>{
                    try {
                        console.log(_this.state.leadIndex);
                        _this.go("ViewLead",{data:"None", output_key: _this.state.data.lead_id, leadIndex: _this.state.leadIndex});
                    } catch (error) {
                        Toast.show(error, _this.toastConfiguration);
                    }
                })
            });
        } else {
            // online
            // call api
            const update_note = Global.API_URL + "note/";

            let form_data_update_note = new FormData();
            form_data_update_note.append("uid", this.state.uid);
            form_data_update_note.append("token", this.state.token);

            form_data_update_note.append("id", this.state.data.note_id);
            form_data_update_note.append("lead_id", this.state.data.lead_id);
            form_data_update_note.append("content", this.state.edit_content_notes);

            try {
            
                let response = await fetch(update_note, {
                    method: 'POST',
                    headers: {
                    },
                    body: form_data_update_note
                });
                
                let responseJson = await response.json();

                Toast.show(responseJson.msg, this.toastConfiguration);
                if(1 == responseJson.status) {
                    try {
                        this.go("ViewLead",{data:"None", output_key: this.state.data.lead_id});
                    } catch (error) {
                        console.error(error);
                        Toast.show(error, this.toastConfiguration);
                    }

                }else{
                    Toast.show(responseJson.msg, this.toastConfiguration);
                }

            } catch(error) {
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
        }
        
    }
    render(): React$Element<*> {
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
                </View>
            );
        }
        const {state} = this.props.navigation;
        const data = state.params.data;
        const output_key = state.params.output_key;
        const {navigation} = this.props;
        
        return <BaseContainer title="Edit Note" {...{ navigation }} hide_footer={this.state.hide_footer}>
            <Container style={{backgroundColor: "#ECF0F3"}}>
            <ScrollView contentContainerStyle={style.content}>
                <KeyboardAvoidingView behavior="padding">
                   
                    <Text style={{marginLeft:5,marginTop: 10}}>Note Content</Text>
                        <Item style={style.tagInput}>
                            <Input
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                inverse
                                onChangeText={(value) => this.setState({edit_content_notes: value})}
                                value={this.state.edit_content_notes}
                                multiline = {true}
                                style={{height:119}}
                            />
                        </Item>

                         <View style={Styles.listItem}>
                            <Button onPress={() => this.deleteNote()} style={style.buttons}>
                                <Text >Delete</Text>
                            </Button>
                            <Button onPress={() => this.go("ViewLead",{data:"None", output_key: data.lead_id})} style={style.buttons}>
                                <Text>Cancel</Text>
                            </Button>
                            <Button onPress={() => this.updateNote()} style={style.button}>
                                    <Text style={{color:"white"}}>OK</Text>
                            </Button>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </Container>
        </BaseContainer>;
    }
}

const style = StyleSheet.create({
    button: {
        marginBottom: 20,
        backgroundColor: "#6AA6D6",
//        color: "white",
        // alignSelf:'center'
        marginLeft:20,

    },
    buttons: {
        marginBottom: 20,
        backgroundColor: "rgb(216, 216, 216)",
        marginLeft:20,
        // alignSelf:'center'

    },
    text: {
        fontFamily: variables.titleFontfamily,
        color: "white"
    },
    headline: {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18
      },
      tagInput:{
        borderColor:"gray",
        marginLeft:10,
        marginRight:10
    },
    sizeLogo:{
        width : 357,
        // justifyContent: 'center',
        height: 70
        
    }
});