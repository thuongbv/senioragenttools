// @flow
import autobind from "autobind-decorator";
import moment from "moment";
import React, {Component} from "react";
import {Text, Image, StyleSheet, View,ImageBackground,Dimensions} from "react-native";
import {H1,Container,Button} from "native-base";

import type { NavigationScreenProp } from "react-navigation/src/TypeDefinition";

import {BaseContainer, Circle, Styles, Images, WindowDimensions,NavigationHelpers,Global } from "../components";
import variables from "../../native-base-theme/variables/commonColor";

export default class Setter extends Component {
   
    go(key: string) {
        this.props.navigation.navigate(key);
    }
    
    render(): React$Element<*> {
        const today = moment();
        const month = today.format("MMMM Y").toUpperCase();
        const dayOfMonth = today.format("D");
        const dayOfWeek = today.format("dddd").toUpperCase();
        const {navigation} = this.props;
        const {height, width} = Dimensions.get("window");
        console.log(width);
        return <BaseContainer title="Setter" {...{ navigation }}>
            <Container style={{backgroundColor: "#ECF0F3"}}>
            </Container>
        </BaseContainer>;
    }
}

const style = StyleSheet.create({
    button: {
        marginBottom: 20,
        backgroundColor: "#291BE0",
//        color: "white",
        alignSelf:'center'

    },
    text: {
        fontFamily: variables.titleFontfamily,
        color: "white"
    },
    headline: {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18
      },
    sizeLogo:{
        width : 357,
        // justifyContent: 'center',
        height: 70
        
    }
});