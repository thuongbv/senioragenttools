import React, {Component} from "react";
import {PermissionsAndroid,Text, Switch, StyleSheet, View, AsyncStorage,Dimensions,ActivityIndicator,Alert,ScrollView} from "react-native";
import {H1,Container,Button} from "native-base";
import Toast from 'react-native-root-toast';
import RNFetchBlob from 'react-native-fetch-blob';
import { zip, unzip, unzipAssets, subscribe } from 'react-native-zip-archive';
import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global} from "../components";

import variables from "../../native-base-theme/variables/commonColor";

export default class Settings extends Component {
    go(key: string) {
        this.props.navigation.navigate(key);
    }

    go1(key: string, detail_data: object) {
        this.props.navigation.navigate(key, detail_data);
    }

    constructor(props) {
       super(props);
       this.state = {
            isLoading: true,
            is_offline: false,
            json_file_content: {}
       };
       this.toast_configuration = {
           duration: Toast.durations.SHORT,
           position: Toast.positions.BOTTOM,
           shadow: true,
           animation: true,
           hideOnPress: true,
           delay: 0,
       };
       
    }
    async componentWillMount() {
        // get data from async storage
        const is_offline = JSON.parse(await AsyncStorage.getItem('is_offline'));
        
        this.setState({
            is_offline: null === is_offline ? this.state.is_offline : is_offline,
            isLoading: false
        }, async function() {
            const json_file_path = await AsyncStorage.getItem('json_file_path');
            console.log(json_file_path);
            if(null !== json_file_path) {
                // read file
                // RNFetchBlob.fs.readFile(json_file_path+'contact_results.json', 'utf8')
                // .then((data) => {
                // // handle the data ..
                // // const json_data = JSON.parse(data);
                // // console.log(json_data);
                // })
            }
        });
    }

    async downloadFile(fileName, url, is_offline){
        const write_file_permision_allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
        if(write_file_permision_allowed) {
            // continue
        } else {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use write file");
                // continue
              } else {
                console.log("Write external storage permission denied");
                return false;
              }
        }

        let date      = new Date();
        let ext       = this.extention(fileName);
        ext = "."+ext;
        const { config, fs } = RNFetchBlob;
        let dirs = RNFetchBlob.fs.dirs;
        let PictureDir = dirs.DownloadDir+"/"+fileName;
        const json_dir = dirs.DownloadDir+"/senioragenttools/" + Math.random().toString(36).substr(2, 5);
        console.log(PictureDir);

        let options = {
          fileCache: true,
          addAndroidDownloads : {
            useDownloadManager : true,
            notification : true,
            path:  PictureDir, //  + Math.floor(date.getTime() + date.getSeconds() / 2)+ext
            description : 'File downloaded by download manager.'
          }
        }
        console.log('downloading');
        config(options).fetch('GET', url).then((res) => {
            console.log('download successfully');
            const sourcePath = res.path();
            const targetPath = json_dir;
            // save to async storage
            const offline_data_change = {notes: [], leads: []};
            const _this = this;
            AsyncStorage.multiSet([['json_file_path', targetPath+'/'], ['offline_data_change', JSON.stringify(offline_data_change)]], function() {
                // unzip file
                unzip(sourcePath, targetPath)
                .then((path) => {
                    _this.setState({
                        isLoading: false,
                        is_offline: is_offline
                    }, async function() {
                        await AsyncStorage.setItem('is_offline', is_offline.toString());

                        // delete zip file
                        RNFetchBlob.fs.unlink(sourcePath);
                    });
                    console.log(`unzip completed at ${path}`);
                    
                })
                .catch((error) => {
                console.log(error)
                })
            });
        });
    }

    extention(filename){
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
    }

    async synchronize() {
        this.setState({isLoading: true}, async function() {
            const offline_data_change = await AsyncStorage.getItem('offline_data_change');
            if(null !== offline_data_change) {
                    const user_data = JSON.parse(await AsyncStorage.getItem('user_data'));
                    const api_url = Global.API_URL + "synchronize_leads/";
                    let synchronize_lead_data = new FormData();
                    synchronize_lead_data.append("uid", user_data.uid);
                    synchronize_lead_data.append("token", user_data.token);
                    synchronize_lead_data.append("offline_data", offline_data_change);
                    console.log(offline_data_change);
                    
                    try {
                        let response = await fetch(api_url, {
                            method: 'POST',
                            headers: {
                            },
                            body: synchronize_lead_data
                        });
                        let responseJson = await response.json();
                        console.log(responseJson);
                        Toast.show(responseJson.msg, this.toastConfiguration);
                        if(1 == responseJson.status) {
                            const is_offline = true;
                            await this.downloadFile(responseJson.data.file_name, responseJson.data.url, is_offline);
                        }

                    } catch(error) {
                        console.error(error);
                    }
            }
        });
       
    }
    
    async changeOnlineMode(is_offline) {
        if(is_offline) {
            this.setState({isLoading: true});
            // get lead data from server
            const user_data = JSON.parse(await AsyncStorage.getItem('user_data'));
            let output_list_url = Global.API_URL + "offline_leads/";
            output_list_url += user_data.uid + "/";
            output_list_url += user_data.token;
            
            return fetch(output_list_url)
            .then((response) => response.json())
            .then(async (responseJson) => {
                console.log(responseJson);
                Toast.show(responseJson.msg, this.toastConfiguration);
                if('1' == responseJson.status) {
                    // get json file content
                    // return fetch(responseJson.data.url).then((file_response) => file_response.json()).then(async (fileResponseJson) => {
                    //     // save to async storage
                    //     console.log(JSON.stringify(fileResponseJson));
                    //     await AsyncStorage.setItem('offline_data', JSON.stringify(fileResponseJson));
                    //     this.setState({
                    //         isLoading: false,
                    //         is_offline: is_offline
                    //     }, async function() {
                    //         await AsyncStorage.setItem('is_offline', is_offline.toString());
                    //     });
                    // });
                    await this.downloadFile(responseJson.data.file_name, responseJson.data.url, is_offline);
                    
                   
                }
            })
            .catch((error) => {
                console.error(error);
            });
        } else {
            this.setState({is_offline: is_offline}, async function() {
                // save to async storage
                await AsyncStorage.setItem('is_offline', is_offline.toString());
            });
        }
    }
   
    render(): React$Element<*> {
        if (this.state.isLoading) {
            return (
              <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
              </View>
            );
        }
        const {navigation} = this.props;
        
        return <BaseContainer title="Settings" {...{ navigation }}>
            <Container >
                <View style={style.blur}>
                    <Text style={{marginLeft:5,marginTop: 10}}>Offline </Text>
                    <Switch
                    value={this.state.is_offline}
                    onValueChange={(value) => this.changeOnlineMode(value)}
                    ></Switch>
                    <Button onPress={() => this.synchronize()} success disabled={!this.state.is_offline}>
                        <Text style={{color:"white"}}>Synchronize</Text>
                    </Button>
                </View>
                
                
                
            </Container>
        </BaseContainer>;
    }
    


}

const {width, height} = Dimensions.get("window");
const style = StyleSheet.create({

    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    button: {
            marginBottom: 10,
            marginTop: 10,
            marginLeft: 5,
            backgroundColor: "#291BE0",
    //        color: "white",
            padding:0
        },
    group: {
            width:width,
            backgroundColor: variables.lightGray,
            justifyContent: "space-between",
            marginBottom: 10
        },
    text: {
//        paddingLeft: variables.contentPadding * 2,
        paddingBottom:10
    },
    blur: {
        backgroundColor: "#ECF0F3",
        margin:20
    },
});