// @flow
import moment from "moment";
import React, {Component} from "react";
import {Text, Image, StyleSheet, View,PermissionsAndroid ,Platform,AsyncStorage,TouchableWithoutFeedback,
    ActivityIndicator,ListView,ScrollView,Dimensions,Alert,Modal,TouchableHighlight,Linking,TouchableOpacity,Keyboard, Picker as PickerNative} from "react-native";
import {H3,Container,Button,Picker,Item,Input,Icon} from "native-base";
 import { Col, Row, Grid } from 'react-native-easy-grid';
import Toast from 'react-native-root-toast';
import {BaseContainer, Circle, Styles, Images, WindowDimensions,Global,NavigationHelpers } from "../components";
import Communications from 'react-native-communications';
import variables from "../../native-base-theme/variables/commonColor";
import RNFetchBlob from 'react-native-fetch-blob';
const {width, height} = Dimensions.get("window");

export default class ViewLead extends Component {
      go(key: string, detail_data: object) {
              this.props.navigation.navigate(key, detail_data);
          }
          go1(key) {
            this.props.navigation.navigate(key);
        }
      constructor(props) {
          super(props);
          this.state = {
            isLoading: true,
            data: {},
            output_key: 0,
            contact:0,
            notes:'',
            status:'',
            check_copy:'',
            uid:'',
            token:'',
            modalVisible: false,
            // dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
            show_custom_note: false,
            show_contact_result: false,
            cusheight_viewlead: 0,
            hide_footer: true, // always hide footer
            return_page: '',
            show_picker_status: false,
            is_offline: false,
            leadIndex: null
        };

        // this.status='';
            this.addNote= this.addNote.bind(this);
          this.group = {};
          this.status_cuz=[];
          this.status_default =[];
          this.notes_full = [];
          this.notess =[];
          this.contact_results=[];
        //   this.contact='';
      }

    async getDataOffline(data_name) {
        const json_file_path = await AsyncStorage.getItem('json_file_path');
        if(null !== json_file_path) {
            return RNFetchBlob.fs.readFile(json_file_path+data_name+'.json', 'utf8')
            .then((file_content) => {
            // handle the data ..
                return JSON.parse(file_content);
            })
        }
    }

    async componentWillMount() {
        const {state} = this.props.navigation;
        const data = state.params.data;
        const output_key = state.params.output_key;
        const return_page = state.params.return_page;
        const leadIndex = state.params.leadIndex;
        if(undefined !== return_page) {
            this.setState({return_page: return_page});
        }

        // get is_offline data from async storage
        const is_offline = JSON.parse(await AsyncStorage.getItem('is_offline'));
        this.setState({is_offline: null === is_offline ? this.state.is_offline : is_offline}, async function() {
            console.log('is offline: ', this.state.is_offline, output_key);
            if(this.state.is_offline) {
                // offine
                // get status
                const statuses = await this.getDataOffline('statuses');
                this.status_cuz = statuses.custom;
                this.status_default = statuses.default;

                // get notes
                this.notes_full = await this.getDataOffline('notes');
                this.notess = this.notes_full.filter(n => n.lead_id == output_key);

                // get contact results
                this.contact_results = await this.getDataOffline('contact_results');

                if("None" === data) {
                    // get data from file by id
                    const json_file_path = await AsyncStorage.getItem('json_file_path');
                    if(null !== json_file_path) {
                        RNFetchBlob.fs.readFile(json_file_path+'leads/'+leadIndex+'-'+output_key+'.json', 'utf8')
                        .then((file_content) => {
                        // handle the data ..
                        console.log('init viewlead', leadIndex);
                            this.setState({
                                isLoading: false,
                                data: JSON.parse(file_content),
                                status:data.lead_status,
                                output_key: output_key,
                                leadIndex: leadIndex
                            });
                        })
                    }
                } else {
                    // get lead data
                    this.setState({
                        isLoading: false,
                        data: data,
                        status:data.lead_status,
                        output_key: output_key,
                        leadIndex: leadIndex
                    });
                }                
            } else {
                // online
                const user_dataa = JSON.parse(await AsyncStorage.getItem('user_data'));
                let output_list_url = Global.API_URL + "statuses/"+user_dataa.uid + "/"+user_dataa.token + "/";
                let output_list_url_contact = Global.API_URL + "contact_results/"+user_dataa.uid + "/"+user_dataa.token + "/";
                let output_list_url_note = Global.API_URL + "notes/"+user_dataa.uid + "/"+user_dataa.token + "/"+output_key + "/";
                fetch(output_list_url)
                    .then((response) => response.json())
                    .then((responseJson) => {

                        if('1' == responseJson.status) {
                            // get top list successfully
                            this.status_cuz = responseJson.data.custom;
                            this.status_default = responseJson.data.default;
                            fetch(output_list_url_note)
                                .then((response) => response.json())
                                .then((responseJson) => {
                                if('1' == responseJson.status) {
                                    // get top list successfully
                                    this.notess=responseJson.data;
                                    fetch(output_list_url_contact)
                                        .then((response) => response.json())
                                        .then((responseJson) => {

                                            if('1' == responseJson.status) {
                                                // get top list successfully
                                                this.contact_results = responseJson.data;
                                                // console.log(this.contact_results);
                                                this.setState({
                                                    // isLoading: false,
                                                    data: data,
                                                    status:data.lead_status,
                                                    output_key: output_key,
                                                    uid:user_dataa.uid,
                                                    token:user_dataa.token
                                                //   dataSource: ds.cloneWithRows(responseJson.movies),
                                                }, function() {
                                                    // if(data=="None"){ // always get lead data from db
                                                        let output_list_url_lead = Global.API_URL + "lead/";
                                                        output_list_url_lead += user_dataa.uid + "/";
                                                        output_list_url_lead += user_dataa.token + "/";
                                                        output_list_url_lead += output_key + "/";
                                                        console.log(output_list_url_lead);
                                                        fetch(output_list_url_lead)
                                                            .then((response) => response.json())
                                                            .then((responseJson) => {
                                                                console.log(responseJson.data);
                                                                if('1' == responseJson.status) {
                                                                    this.setState({
                                                                        isLoading: false,
                                                                        status:responseJson.data.lead_status,
                                                                        data: responseJson.data,
                                                                        output_key: output_key,
                                                                        uid:user_dataa.uid,
                                                                        token:user_dataa.token
                                                                    //   dataSource: ds.cloneWithRows(responseJson.movies),
                                                                    }, function() {

                                                                    });
                                                                }
                                                                // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

                                                            })
                                                            .catch((error) => {
                                                                console.error(error);
                                                        });
                                                    // }
                                                });
                                            }
                                        })
                                        .catch((error) => {
                                            console.error(error);
                                        });
                                        }
                                    })
                                .catch((error) => {
                                    console.error(error);
                                });

                            }
                        // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

                    })
                    .catch((error) => {
                        console.error(error);
                });
            }
        });
    }
    getChangeStatus(value:string){
        this.setState({status: value, show_picker_status: false});
        if(value=="Customer"){
            Alert.alert(
                'Confirmation',
                'Do you want to copy this lead to Tracker?',
                [
                  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                //   {text: 'OK', onPress: () => this.setState({check_copy: 'check_copy'})},
                  {text: 'OK', onPress: () => this.copyToTracker()},
                ],
                { cancelable: false }
              )
        }
    }

    async copyToTracker() {
        if(this.state.is_offline) {
            this.setState({check_copy: 'check_copy'});
        } else {
            const api_url = Global.API_URL + "copy_tracker/";

            let form_data = new FormData();
            form_data.append("uid", this.state.uid);
            form_data.append("token", this.state.token);
            form_data.append("id", this.state.output_key);

            try {

                let response = await fetch(api_url, {
                    method: 'POST',
                    headers: {
                    },
                    body: form_data
                });
                let responseJson = await response.json();
                Toast.show(responseJson.msg, this.toastConfiguration);

            } catch(error) {
    //                     console.log(1234);
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
        }
    }

    UpdateLead=async () =>{
        if(this.state.is_offline) {
            // offline
            let offline_data_change = JSON.parse(await AsyncStorage.getItem('offline_data_change'));
            const lead = {
                id: this.state.data.id,
                first_name: this.state.data.first_name,
                last_name: this.state.data.last_name,
                age: this.state.data.age,
                spouse_name: this.state.data.spouse_name,
                spouse_age: this.state.data.spouse_age,
                street_address: this.state.data.street_address,
                city: this.state.data.city,
                country: this.state.data.country,
                phone1: this.state.data.phone1,
                phone2: this.state.data.phone2,
                quick_note: this.state.data.quick_note,
                record_day: this.state.data.record_day,
                lead_status: this.state.data.lead_status,
                state: this.state.data.state_code,
                zip: this.state.data.zip,
                add_lead_type: '',
                decline_id: '',
                check_copy: this.state.data.check_copy,
                lead_type: this.state.data.lead_type,
                url: this.state.data.url
            };
            if(undefined === offline_data_change.leads) {
                offline_data_change.leads = [];
            }
            // check if lead is create/update offline => update
            const lead_check_index = offline_data_change.leads.findIndex(n => n.id == this.state.data.id);
            console.log(lead_check_index);
            if(-1 !== lead_check_index) {
                // get created date from old data
                lead.current_datetime = offline_data_change.leads[lead_check_index].current_datetime;
                offline_data_change.leads[lead_check_index] = lead;
            } else {
                offline_data_change.leads.push(lead);
            }
            console.log(offline_data_change);
            const _this = this;
            await AsyncStorage.setItem('offline_data_change', JSON.stringify(offline_data_change), async function() {
                // update file
                const json_file_path = await AsyncStorage.getItem('json_file_path');
                RNFetchBlob.fs.writeFile(json_file_path+'leads/'+_this.state.leadIndex+'-'+_this.state.data.id+'.json', JSON.stringify(lead), 'utf8')
                .then(()=>{
                    NavigationHelpers.reset(_this.props.navigation, "Lead");
                })
            });
        } else {
            // call api
            const edit_lead = Global.API_URL + "lead/";

            let form_data_edit_lead = new FormData();
            form_data_edit_lead.append("uid", this.state.uid);
            form_data_edit_lead.append("token", this.state.token);

            form_data_edit_lead.append("first_name", this.state.data.first_name);
            form_data_edit_lead.append("last_name", this.state.data.last_name);
            form_data_edit_lead.append("age", this.state.data.age);
            form_data_edit_lead.append("spouse_name", this.state.data.spouse_name);
            form_data_edit_lead.append("spouse_age", this.state.data.spouse_age);
            form_data_edit_lead.append("street_address", this.state.data.street_address);
            form_data_edit_lead.append("city", this.state.data.city);
            form_data_edit_lead.append("country", this.state.data.country);
            form_data_edit_lead.append("phone1", this.state.data.phone1);
            form_data_edit_lead.append("phone2", this.state.data.phone2);
            form_data_edit_lead.append("quick_note", this.state.data.quick_note);
            form_data_edit_lead.append("record_day", this.state.data.record_day);
            form_data_edit_lead.append("lead_status", this.state.status);
            form_data_edit_lead.append("state", this.state.data.state);
            form_data_edit_lead.append("zip", this.state.data.zip);
            form_data_edit_lead.append("email_address", this.state.data.email_address);
            form_data_edit_lead.append("add_lead_type", "");
            form_data_edit_lead.append("decline_id", "");
            form_data_edit_lead.append("id", this.state.data.id);
            form_data_edit_lead.append("check_copy", this.state.check_copy);
            form_data_edit_lead.append("file", "");
            form_data_edit_lead.append("url", this.state.data.url);
            try {
                let response = await fetch(edit_lead, {
                    method: 'POST',
                    headers: {
                    },
                    body: form_data_edit_lead
                });
                let responseJson = await response.json();
                Toast.show(responseJson.msg, this.toastConfiguration);
                if(1 == responseJson.status) {
                    try {
                        if('' === this.state.return_page) {
                            NavigationHelpers.reset(this.props.navigation, "Lead");
                        } else {
                            if('MapsView' === this.state.return_page) {
                                // update from lead
                                this.props.navigation.state.params.update_from_lead();
                            }
                            NavigationHelpers.moveBack(this.props.navigation);
                        }
                    } catch (error) {
                        console.error(error);
                        Toast.show(error, this.toastConfiguration);
                    }

                }else{
                    Toast.show(responseJson.msg, this.toastConfiguration);
                }
            } catch(error) {
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
        } 
    }

    getCurrentDatetime() {
        const currentdate = new Date();
        const current_hour = 12 > currentdate.getHours() ? currentdate.getHours() : currentdate.getHours()-12;
        return (10 > (currentdate.getMonth()+1) ? "0" : "") + (currentdate.getMonth()+1) + "/"
        + (10 > currentdate.getDate() ? "0" : "") + currentdate.getDate()  + "/" 
        + currentdate.getFullYear() + " "  
        + (10 > current_hour ? "0" : "") + current_hour + ":"  
        + (10 > currentdate.getMinutes() ? "0" : "")+ currentdate.getMinutes() + ":" 
        + (10 > currentdate.getSeconds() ? "0" : "")+ currentdate.getSeconds() + " "
        + (currentdate.getHours()>12 ? "PM" : "AM");
    }

    addNote=async () =>{
        if(this.state.notes.trim()==='') {
            Toast.show('Content note not bank', this.toastConfiguration);
            return;
        }

        if(this.state.is_offline) {
            // offline
            let offline_data_change = JSON.parse(await AsyncStorage.getItem('offline_data_change'));
            const note = {note_id: Math.random().toString(36).substr(2, 5), lead_id: this.state.data.id, content: this.state.notes, date_time: this.getCurrentDatetime()};
            offline_data_change.notes.push(note);
            const _this = this;
            await AsyncStorage.setItem('offline_data_change', JSON.stringify(offline_data_change), async function() {
                // update notes in props
                _this.notess.unshift(note);
                _this.notes_full.unshift(note);

                // update file
                const json_file_path = await AsyncStorage.getItem('json_file_path');
                RNFetchBlob.fs.writeFile(json_file_path+'notes.json', JSON.stringify(_this.notes_full), 'utf8')
                .then(()=>{
                    Toast.show('Add note successfully', _this.toastConfiguration);
                    _this.setState({notes: '',cusheight_viewlead:0
                        // ,hide_footer:false
                    });
                    Keyboard.dismiss();
                    console.log(offline_data_change);
                })
                // this.forceUpdate();
            });
        } else {
            // online
            // call api
            const add_note = Global.API_URL + "note/";

            let form_data_add_note = new FormData();
            form_data_add_note.append("uid", this.state.uid);
            form_data_add_note.append("token", this.state.token);

            form_data_add_note.append("id", '');
            form_data_add_note.append("lead_id", this.state.data.id);
            form_data_add_note.append("content", this.state.notes);
            try {

                let response = await fetch(add_note, {
                    method: 'POST',
                    headers: {
                    },
                    body: form_data_add_note
                });
                let responseJson = await response.json();
                Toast.show(responseJson.msg, this.toastConfiguration);
                if(1 == responseJson.status) {
                    try {
                        // update note
                        let output_list_url_note = Global.API_URL + "notes/"+this.state.uid + "/"+this.state.token + "/"+this.state.output_key + "/";
                        fetch(output_list_url_note)
                        .then((response) => response.json())
                        .then((responseJson) => {
                            if('1' == responseJson.status) {
                                // get top list successfully
                                this.notess=responseJson.data;
                                this.forceUpdate();
                            }
                        })
                        .catch((error) => {
                            console.error(error);
                        });

                        this.setState({notes: '',cusheight_viewlead:0
                        // ,hide_footer:false
                    });
                        Keyboard.dismiss();


                    } catch (error) {
                    // Error saving data
                        console.error(error);
                        Toast.show(error, this.toastConfiguration);
                    }
                }else{
                    Toast.show(responseJson.msg, this.toastConfiguration);
                }

            } catch(error) {
                console.error(error);
                Toast.show(error, this.toastConfiguration);
            }
        }
        
    }
    setStateContact(value:string){
        this.setState(
            {
                contact: value,
                notes:value,
                show_custom_note: false,
                show_contact_result: true,
                cusheight_viewlead: 600
            }

        );
    }
    onPressCall(phoneNumber,value) {
        let url;
        let prompt=true;
		if(Platform.OS !== 'android') {
			url = prompt ? 'telprompt:' : 'tel:';
		}
		else {
			url = 'tel:';
		}

        url += phoneNumber;

        Linking.canOpenURL(url).then(supported => {
            if(!supported) {
                console.log('Can\'t handle url: ' + url);
            } else {
                Linking.openURL(url)
                .catch(err => {
                    if(url.includes('telprompt')) {
                        // telprompt was cancelled and Linking openURL method sees this as an error
                        // it is not a true error so ignore it to prevent apps crashing
                        // see https://github.com/anarchicknight/react-native-communications/issues/39
                    } else {
                        console.warn('openURL error', err)
                    }
                });
            }
        }).catch(err => console.warn('An unexpected error happened', err));
    }
    showAdrress(address){
        Alert.alert(
            'Confirmation',
            'Do you want open map in ?',
            [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'Map', onPress: () => this.libkAddress(address,'Map')},
            {text: 'Browser', onPress: () => this.libkAddress(address,'Browser')},

            ],
            { cancelable: false }
        )
    }
    libkAddress(lat, lng,type, address){
        if(type=='Browser'){
            const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
            const latLng = `${lat},${lng}`;
            const label = 'Google Map';
            const url = Platform.select({
                // ios: `${scheme}${label}@${latLng}`,
                ios: "http://maps.google.com/maps?q="+address,
                android: `${scheme}${address}(${label})`
            });
            console.log(url);
            Linking.canOpenURL(url).then(supported => {
                if(!supported) {
                    console.log('Can\'t handle url: ' + url);
                    const browser_url="http://maps.google.com/maps?q="+lat+","+lng;
                    Linking.openURL(browser_url);
                } else {
                    Linking.openURL(url)
                    .catch(err => {
                        if(url.includes('telprompt')) {
                            // telprompt was cancelled and Linking openURL method sees this as an error
                            // it is not a true error so ignore it to prevent apps crashing
                            // see https://github.com/anarchicknight/react-native-communications/issues/39
                        } else {
                            console.warn('openURL error', err)
                        }
                    });
                }
            }).catch(err => console.warn('An unexpected error happened', err));
        }else{
            const dataMap=[{
                "color": "",
                "address": {
                    "latitude": this.state.data.lat,
                    "longitude": this.state.data.lng
                },
                "name_lead": this.state.data.first_name+' '+this.state.data.last_name,

            }];
            console.log(dataMap);
            this.props.navigation.navigate("MapsView",{data: dataMap,output_key:"Search"});
        }


    }
    showUrl(url){
        Alert.alert(
            'Confirmation',
            'Do you want to open this url?',
            [
            {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'Yes', onPress: () => this.linkUrl(url)},
            ],
            { cancelable: false }
        )
    }
    linkUrl(url){
        // console.log(url);
        // url="http://www.nettruyen.com/";
        Linking.canOpenURL(url).then(supported => {
            if(!supported) {
                console.log('Can\'t handle url: ' + url);
                Alert.alert(
                    'Confirmation',
                    'Can\'t handle url',
                    [
                    {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},

                    ],
                    { cancelable: false }
                )
            } else {
                Linking.openURL(url)
                .catch(err => {
                    if(url.includes('telprompt')) {
                        // telprompt was cancelled and Linking openURL method sees this as an error
                        // it is not a true error so ignore it to prevent apps crashing
                        // see https://github.com/anarchicknight/react-native-communications/issues/39
                    } else {
                        console.warn('openURL error', err)
                    }
                });
            }
        }).catch(err => console.warn('An unexpected error happened', err));
    }
    async downloadFile(fileName){
        const write_file_permision_allowed = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
        if(write_file_permision_allowed) {
            // continue
        } else {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use write file");
                // continue
              } else {
                console.log("Write external storage permission denied");
                return false;
              }
        }

        let date      = new Date();
        let url       = "https://senioragenttools.com/services/navigator_test/file/"+fileName;
        let ext       = this.extention(fileName);
        ext = "."+ext;
        // let reFileName = (fileName.length)
        const { config, fs } = RNFetchBlob;
        let dirs = RNFetchBlob.fs.dirs;

        // filePath = dirs.DownloadDir+"/"+"5b62cf817fe1c"+ext;
        let PictureDir = dirs.DownloadDir+"/"+fileName;

        console.log(PictureDir);

        let options = {
          fileCache: true,
          addAndroidDownloads : {
            useDownloadManager : true,
            notification : true,
            path:  PictureDir, //  + Math.floor(date.getTime() + date.getSeconds() / 2)+ext
            description : 'File downloaded by download manager.'
          }
        }
        Alert.alert("Downloading");
        config(options).fetch('GET', url).then((res) => {

          Alert.alert("Success Downloaded");
        });
      }
    extention(filename){
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
      }
      closeCustomNotePopup() {
        this.setState({cusheight_viewlead:0
            // ,hide_footer: false
        });
        Keyboard.dismiss();
        console.log('close note popup');
      }

    render(): React$Element<*> {

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, paddingTop: 60}}>
                <ActivityIndicator />
                </View>
            );
        }

            const {data, output_key,contact} = this.state;
            const {navigation} = this.props;

            return <BaseContainer title="Lead View" {...{ navigation }} hide_footer={this.state.hide_footer} >
            {/* <KeyboardAvoidingView behavior="padding"> */}
                <Container style={style.bgContainer}>
                    {0 < this.state.cusheight_viewlead && <View style={{height:this.state.cusheight_viewlead}} >
                        <View style={style.bubble}>
                            <View style={{position: 'absolute',
                                right:     20,
                                top:      0,
                                bottom:30,
                                zIndex: 9999}}>
                                <TouchableOpacity onPress={() => this.closeCustomNotePopup()}>
                                <Icon name="close" style={{fontSize: 25}}/>
                                </TouchableOpacity>
                            </View>
                            <Grid>
                                <Row style={this.state.show_contact_result ? style.show : style.hide} size={1}>
                                    <Col style={{height: 100}}>
                                    <Picker
                                        mode="dropdown"
                                        selectedValue={this.state.contact}
                                        onValueChange={(contact) => this.setStateContact(contact)}
                                        >
                                        <Item label="select contact result" value="" />
                                        {this.contact_results.map((item_contact, index) => (
                                            <Item label={item_contact.content} value={item_contact.content} />
                                        ))}
                                    </Picker>
                                    </Col>
                                </Row>
                                <Row size={9}>
                                <Col>
                                    <Item style={style.tagInput}>
                                    <Input
                                        autoCapitalize="none"
                                        returnKeyType="go"
                                        last
                                        inverse
                                        onChangeText={(notes) => this.setState({notes: notes})}
                                        multiline = {true}
                                        value={this.state.notes}
                                        placeholder='Custom Note'
                                        style={{height:119}}
                                        onFocus={() => this.setState({hide_footer: true})} // hide footer
                                    />
                                    </Item>
                                    <View style={style.listItem}>
                                        <Button onPress={() => this.addNote()} style={style.button}>
                                            <Text style={{color:"white"}}>Save</Text>
                                        </Button>
                                    </View>
                                </Col>
                                </Row>
                            </Grid>
                        </View>
                    </View>}

                    <View style={Styles.listItem}>
                        <Button onPress={() =>this.go("EditLead",{data:this.state.data, output_key: this.state.data.id, leadIndex: this.state.leadIndex})} style={style.button}>
                            <Image source={Images.Edit}/>
                        </Button>
                        <Button onPress={() => this.go("Calendars",{data:"None", output_key: this.state.data.id})} style={style.button}>
                            <Image source={Images.add_appointment} style={{width: 26, height: 26}}/>
                        </Button>
                        <Button onPress={() => this.UpdateLead()} style={style.button}>
                                <Text style={{color:"white"}}>Close Lead</Text>
                        </Button>
                    </View>



                     <ScrollView>
                        <H3 style={style.title}>{data.first_name+' '+data.last_name}</H3>
                        <Grid>
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Status</Text></Col>
                                {('android'===Platform.OS || this.state.show_picker_status) && <Col size={7}>
                                    <PickerNative
                                        mode="dropdown"
                                        selectedValue={this.state.status}
                                        onValueChange={(status) => this.getChangeStatus(status)}
                                        >
                                            {this.status_default.map((item, index) => (
                                                <Item label={item.default_name} value={item.default_name}/>
                                            ))}
                                            {this.status_cuz.map((item, index) => (
                                                <Item label={item.name} value={item.name}/>
                                            ))}
                                    </PickerNative>
                                </Col>}
                                {'ios'===Platform.OS && !this.state.show_picker_status && <Col size={7}>
                                <TouchableOpacity onPress={() => this.setState({show_picker_status: !this.state.show_picker_status})}
                                    >
                                        <Text style={[style.sizeTitile, {fontWeight: '700', color: 'blue'}]}>{this.state.status}</Text>
                                    </TouchableOpacity>
                                </Col>}
                            </Row>

                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Address</Text></Col>
                                <Col size={7}>
                                    <TouchableOpacity
                                    // onPress={() => this.showAdrress(data.lat+','+data.lng)}
                                    onPress={() => this.libkAddress(data.lat,data.lng,'Browser', data.street_address+' '+data.city+' '+data.state+' '+data.zip)}
                                    >
                                        <Text style={style.sizeTitile}>{data.street_address}</Text>
                                        <Text style={style.sizeTitile}>{data.city+' '+data.state+' '+data.zip}</Text>
                                    </TouchableOpacity>

                                </Col>
                            </Row>

                            {data.phone1!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Phone 1</Text></Col>
                                <Col size={7}>
                                    <View style={style.sizeTitile}>
                                        <TouchableOpacity onPress={() => this.onPressCall(data.phone1,"phone1")}>
                                            <View>
                                                <Text>{data.phone1}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>

                                </Col>
                            </Row>
                            :<View/>}

                            {data.phone2!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Phone 2</Text></Col>
                                <Col size={7}>
                                    <View style={style.sizeTitile}>
                                        <TouchableOpacity onPress={() => this.onPressCall(data.phone2, "phone2")}>
                                            <View>
                                                <Text>{data.phone2}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>

                                </Col>
                            </Row>
                            :<View/>}

                            {data.country!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>County</Text></Col>
                                <Col size={7}>
                                    <View style={style.sizeTitile}><Text>{data.country}</Text></View>
                                </Col>
                            </Row>
                            :<View/>}

                            {data.age!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Lead Age</Text></Col>
                                <Col size={7}><View style={style.sizeTitile}><Text>{data.age}</Text></View></Col>
                            </Row>
                            :<View/>}

                            {data.spouse_name!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Spouse</Text></Col>
                                <Col size={7}><View style={style.sizeTitile}><Text>{data.spouse_name}</Text></View></Col>
                            </Row>
                            :<View/>}

                            {data.spouse_age!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Spouse Age</Text></Col>
                                <Col size={7}><View style={style.sizeTitile}><Text>{data.spouse_age}</Text></View></Col>
                            </Row>
                            :<View/>}

                            {data.record_day!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Lead Date</Text></Col>
                                <Col size={7}><View style={style.sizeTitile}><Text>{data.record_day}</Text></View></Col>
                            </Row>
                            :<View/>}

                            {data.lead_type!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Lead Type</Text></Col>
                                <Col size={7}><View style={style.sizeTitile}><Text>{data.lead_type}</Text></View></Col>
                            </Row>
                            :<View/>}

                            {data.quick_note!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Quick Note</Text></Col>
                                <Col size={7}><View style={style.sizeTitile}><Text>{data.quick_note}</Text></View></Col>
                            </Row>
                            :<View/>}

                            {data.email_address!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Email</Text></Col>
                                <Col size={7}><View style={style.sizeTitile}><Text>{data.email_address}</Text></View></Col>
                            </Row>
                            :<View/>}

                            {data.file_attachment!=''
                            //  && Platform.OS === 'ios'
                             ?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Attachment</Text></Col>
                                <Col size={5}><View style={style.sizeTitile}>
                                    <TouchableOpacity onPress={() => this.downloadFile(data.file_attachment)}>
                                        <View>
                                            <Text>{data.file_attachment}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View></Col>
                                <Col size={2}>
                                    <TouchableOpacity onPress={() => this.linkUrl("https://senioragenttools.com/services/navigator_test/file/"+data.file_attachment)}>
                                        <View>
                                            <Text>Link</Text>
                                        </View>
                                    </TouchableOpacity></Col>
                            </Row>
                            :<View/>}

                            {data.url!=''?
                            <Row>
                                <Col size={3} style={style.label}><Text style={style.items}>Url</Text></Col>
                                <Col size={7}><View style={style.sizeTitile}>
                                    <TouchableOpacity onPress={() => this.showUrl(data.url)}>
                                        <View>
                                            <Text>{data.url}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View></Col>
                            </Row>
                            :<View/>}

                            <Row>
                                <Col>
                                    <Button
                                     onPress={() => this.setState({show_custom_note: false, show_contact_result: !this.state.show_contact_result, cusheight_viewlead: 600})}
                                    // onPress={() => this.go('CustomNote', {lead_id: this.state.output_key, show_contact_result: true})}
                                     style={style.button} full>
                                        <Text style={style.button_text}>Select{'\n'}Contact Result</Text>
                                    </Button>
                                </Col>
                                <Col>
                                    <Button
                                    onPress={() => this.setState({ show_contact_result: false, cusheight_viewlead: 600})}
                                    // onPress={() => this.go('CustomNote', {lead_id: this.state.output_key, show_contact_result: false})}
                                    style={style.button} full>
                                        <Text style={style.button_text}>Enter{'\n'}Custom Note</Text>
                                    </Button>
                                </Col>
                            </Row>

                            {/* <Row style={this.state.show_contact_result ? style.show : style.hide}>
                                <Col size={3} style={style.label}><Text style={style.items}>Contact Results</Text></Col>
                                <Col size={7}>
                                    <Picker
                                        mode="dropdown"
                                        selectedValue={this.state.contact}
                                        onValueChange={(contact) => this.setStateContact(contact)}
                                        >
                                        <Item label="select contact result" value="" />
                                        {this.contact_results.map((item_contact, index) => (
                                            <Item label={item_contact.content} value={item_contact.content} />
                                        ))}
                                    </Picker>
                                </Col>
                            </Row> */}

                            <Row style={this.state.show_custom_note ? style.show : style.hide}>
                                {/* <Col size={3} style={style.label}><Text style={style.items}>Custom Note</Text></Col> */}
                                <Col>
                                    <Item style={style.tagInput}>
                                    <Input
                                        // maxLength = {40}
                                        autoCapitalize="none"
                                        returnKeyType="go"
                                        last
                                        inverse
                                        onChangeText={(notes) => this.setState({notes: notes})}
                                        multiline = {true}
                                        // numberOfLines = {4}
                                        value={this.state.notes}
                                        placeholder='Custom Note'
                                        style={{height:69}}
                                    />
                                    </Item>
                                    <View style={style.listItem}>
                                        <Button onPress={() => this.addNote()} style={style.button}>
                                            <Text style={{color:"white"}}>Save</Text>
                                        </Button>
                                    </View>
                                </Col>
                            </Row>

                        </Grid>

                            {/* {data.file_attachment!=''?<View style={style.sizeTitile}>
                                <Text style={style.items}>Attachment</Text>
                                    <TouchableOpacity onPress={() => this.downloadFile(data.file_attachment)}>
                                <View>
                                    <Text>{data.file_attachment}</Text>
                                </View>
                                    </TouchableOpacity>
                                </View>
                                :<View/>} */}







                        <View style={[Styles.listItemView, style.group]} last>
                                <View style={style.text}>
                                    <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                                        <View style={{width: '30%'}} >
                                            <Text>Date</Text>
                                        </View>
                                        <View style={{width: '70%'}} >
                                            <Text>Data</Text>
                                        </View>
                                    </View>
                                </View>
                        </View>
                        {/* <View style={{height:250}}> */}
                            {this.notess.map((item, index) => (
                                // <Group style={{height:250}} info={item} onPress={() => this.go("ViewLead",{data: item, output_key: item.note_id})}/>
                                <Group style={{height:250}} info={item} onPress={() => this.go("Notes",{data: item, output_key: item.note_id, leadIndex: this.state.leadIndex})}/>
                            ))}
                        {/* </View> */}
                        </ScrollView>
                     </Container>
                     {/* </KeyboardAvoidingView> */}
            </BaseContainer>;
        }
}

class Group extends Component {
    props: {
        info: array,
        onPress: () => void
    }
    render(): React$Element<*> {
        const {info, onPress} = this.props;
        return <TouchableWithoutFeedback onPress={onPress}>
            <View style={[Styles.listItemView, style.group]} last>
                <View style={style.text}>
                <View style={{flex: 1, flexDirection: 'row', justifyContent : 'space-between'}}>
                    <View style={{width: '30%'}} >
                        <Text>{info.date_time}</Text>
                    </View>

                    <View style={{width: '70%'}} >
                        <Text>{info.content}</Text>
                    </View>

                </View>
                </View>
            </View>
            </TouchableWithoutFeedback>;
    }
}
const style = StyleSheet.create({
    img: {
        ...WindowDimensions
    },
    tagInput:{
        borderColor:"gray",
        marginLeft:10,
        marginRight:10,
    },
    title:{
        color:"#0099FF",
        marginLeft:10,
        fontWeight:"bold"
    },
    circle: {
        marginVertical: variables.contentPadding * 4
    },
    badge: {
        position: "absolute",
        right: 10,
        top: 10
    },
    sizeTitile:{
        flexDirection: 'row',
        marginLeft:10,
    },
    items:{
        marginRight:10,
        fontWeight:"bold",
        color:"black"
    },
    button: {
        // padding:0,
        marginTop:20,
        marginRight: 10,
        marginBottom: 10,
        backgroundColor: "#6AA6D6",
        marginLeft:10,


    },
        group: {
            width:width,

            justifyContent: "space-between",
            marginBottom: 10,

        },
    text: {
        // fontFamily: variables.titleFontfamily,
        color: "white"
    },
    label: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 10
    },
    button_text: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    hide: {height: 0, opacity: 0,width:0},
    bubble: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.7)',
        // paddingHorizontal: 18,
        // paddingVertical: 12,
        // borderRadius: 20,
        position: 'relative',
        zIndex: -1
      },
      customView: {

        // alignSelf:'center',
        marginLeft:5,
        marginBottom:20
      },
      bgContainer: {backgroundColor: 'rgba(255,255,255,0.7)'}

});
